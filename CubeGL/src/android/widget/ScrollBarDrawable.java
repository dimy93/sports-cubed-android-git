package android.widget;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
//Dummy Class For Linking to ScrollBarDrawable
public class ScrollBarDrawable extends Drawable {

    public ScrollBarDrawable() {
    }

    /**
     * Indicate whether the horizontal scrollbar track should always be drawn regardless of the
     * extent. Defaults to false.
     *
     * @param alwaysDrawTrack Set to true if the track should always be drawn
     */
    public void setAlwaysDrawHorizontalTrack(boolean alwaysDrawTrack) {
    }

    /**
     * Indicate whether the vertical scrollbar track should always be drawn regardless of the
     * extent. Defaults to false.
     *
     * @param alwaysDrawTrack Set to true if the track should always be drawn
     */
    public void setAlwaysDrawVerticalTrack(boolean alwaysDrawTrack) {
    }

    /**
     * Indicates whether the vertical scrollbar track should always be drawn regardless of the
     * extent.
     */
    public boolean getAlwaysDrawVerticalTrack() {
        return false;
    }

    /**
     * Indicates whether the horizontal scrollbar track should always be drawn regardless of the
     * extent.
     */
    public boolean getAlwaysDrawHorizontalTrack() {
        return false;
    }

    public void setParameters(int range, int offset, int extent, boolean vertical) {
    }

    @Override
    public void draw(Canvas canvas) {
    }

    @Override
    protected void onBoundsChange(Rect bounds) {
    }

    protected void drawTrack(Canvas canvas, Rect bounds, boolean vertical) {
    }

    protected void drawThumb(Canvas canvas, Rect bounds, int offset, int length, boolean vertical) {
    }

    public void setVerticalThumbDrawable(Drawable thumb) {
    }

    public void setVerticalTrackDrawable(Drawable track) {
    }

    public void setHorizontalThumbDrawable(Drawable thumb) {
    }

    public void setHorizontalTrackDrawable(Drawable track) {
    }

    public int getSize(boolean vertical) {
        return 0;
    }

    @Override
    public void setAlpha(int alpha) {
    }

    @Override
    public int getAlpha() {
        return 0;
    }

    @Override
    public void setColorFilter(ColorFilter cf) {
    }

    @Override
    public int getOpacity() {
        return 0;
    }

    @Override
    public String toString() {
        return "";
    }
}