
package com.cube.GLCube.GL;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import android.app.Activity;
import android.graphics.Bitmap;

import com.cube.GLCube.UIConnect.ConnectToUI.FutureLayout;
import com.cube.GLCube.UIConnect.CubeSide;
import com.cube.GLCube.UIConnect.CubeSide.CubeSideListener;
import com.cube.GLCube.UIConnect.XMLLoad;
import com.cube.UI.TouchEventListener;
import com.threed.jpct.Matrix;
import com.threed.jpct.Object3D;
import com.threed.jpct.SimpleVector;
import com.threed.jpct.Texture;
import com.threed.jpct.TextureManager;
import com.threed.jpct.World;

public class Cube extends Object3D implements CubeSideListener
{
	Activity activity;
	final HashMap < String, Object3D > objects;
	
	private static final long serialVersionUID = 1L;
	private final Map < String, CubeSide > layouts;
	private Folding folding;
	private Fixing fix;
	private Zooming zoom;
	private RotateAnimation rotate;
	private CountDownLatch drawn;
	
	private final List < Float > cubeVerteces;
	private final List < Integer > cubeIndeces;
	private final GLRenderer render;
	
	public Cube ( GLRenderer render, Map < String, CubeSide > layouts )
	{
		super ( 0 );
		
		this.render = render;
		World world = render.getWorld ( );
		activity = (Activity) render.parent.getContext ( );
		
		this.layouts = layouts;
		List < String > strs = XMLLoad.getSides ( activity );
		
		cubeIndeces = XMLLoad.getcubeIndeces ( activity );
		cubeVerteces = XMLLoad.getCubeVerteces ( activity );
		
		objects = new HashMap < String, Object3D > ( );
		int cubeIndex = 0;
		
		for ( String side : strs )
		{
			CubeSide cs = layouts.get ( side );
			cs.setOwner ( this );
			cs.addListener(this);
			
			
			// Create the plane
			Object3D obj = new Object3D ( 2 );
			obj.setCulling ( false );
			
			// Create the plane's mesh
			obj.addTriangle ( getVertex ( cubeIndex++ ), 0, 0, getVertex ( cubeIndex++ ), 0, 1,
					getVertex ( cubeIndex++ ), 1, 0 );
			obj.addTriangle ( getVertex ( cubeIndex++ ), 1, 0, getVertex ( cubeIndex++ ), 0, 1,
					getVertex ( cubeIndex++ ), 1, 1 );
			
			// Add the plane to various places in order to work
			objects.put ( side, obj );
			this.addChild ( obj );
			obj.build ( );
			world.addObject ( obj );
		}
		
		// Cube's setup
		// setSide ( 2f );
		this.setCulling ( false );
		world.addObject ( this );
		
		// Setup animations
		fix = new Fixing ( this );
		fix.alikeSkip ( );
		folding = new Folding ( this );
		zoom = new Zooming ( this, world.getCamera ( ) );
		RotateAnimation.setCamera ( world.getCamera ( ) );
	}
	
	public void draw ( )
	{
		if ( drawn != null )
		{
			drawn.countDown ( );
			if ( drawn.getCount ( ) == 0 )
				drawn = null;
		}
		boolean folded = folding.draw ( );
		if ( folded )
		{
			fix.restart ( );
		}
		if ( rotate != null )
		{
			if ( !rotate.draw ( ) )
				rotate = null;
		}
		
		if (TouchEventListener.isLocked())
		{
			fix.restart();
		}
		
		float fixed = fix.draw ( );
		if ( fixed == 1f )
		{
			zoom.start ( 0 );
		}
		zoom.draw ( );
	}
	
	public void fold ( int time )
	{
		folding.start ( time );
	}
	
	public void animateRotation ( float velocityX, float velocityY, float friction )
	{
		rotate = new RotateAnimation ( this, velocityX, velocityY, friction );
	}
	
	public CountDownLatch setTextureAndDraw ( Bitmap bitmap, String side )
	{
		setTexture ( bitmap, side );
		drawn = new CountDownLatch ( 2 );
		return drawn;
	}
	
	public void setTexture ( Bitmap bitmap, String side )
	{
		Texture texture = new Texture ( bitmap, false );
		TextureManager tm = TextureManager.getInstance ( );
		
		if ( tm.containsTexture ( side ) )
		{
			tm.replaceTexture ( side, texture );
		}
		else
		{
			tm.addTexture ( side, texture );
		}
		
		objects.get ( side ).setTexture ( side );
		bitmap.recycle ( );
	}
	
	public void setTextures ( List < FutureLayout < Bitmap >> textureInit )
	{
		for ( FutureLayout < Bitmap > futureLayout : textureInit )
		{
			String side = futureLayout.getLayoutName ( );
			Bitmap bitmap = futureLayout.getLastResult ( );
			setTexture ( bitmap, side );
		}
	}
	
	public Map < String, CubeSide > getLayouts ( )
	{
		return layouts;
	}
	
	public void rotateAxis ( SimpleVector axis, float angle )
	{
		if ( zoom.inZoom ( ) )
			return;
		super.rotateAxis ( axis, angle );
		fix.restart ( );
	}
	
	@Override
	public void setRotationMatrix(Matrix m)
	{
		if (  zoom.inZoom ( ) )
			return;
		super.setRotationMatrix(m);
	}
	
	public void rotateX ( float angle )
	{
		if ( zoom.inZoom ( ) )
			return;
		super.rotateX ( angle );
		fix.restart ( );
	}
	
	public void rotateY ( float angle )
	{
		if ( zoom.inZoom ( ) )
			return;
		super.rotateY ( angle );
		fix.restart ( );
	}
	
	public void rotateZ ( float angle )
	{
		if ( zoom.inZoom ( ) )
			return;
		super.rotateZ ( angle );
		fix.restart ( );
	}
	
	public GLRenderer getRenderer ( )
	{
		return render;
	}
	
	public String getLastSide ( )
	{
		List < String > strs = XMLLoad.getSides ( activity );
		for ( String side : strs )
		{
			CubeSide cs = layouts.get ( side );
			if ( cs == TouchEventListener.onSide )
				return side;
		}
		return fix.getSide ( );
	}
	
	public void resetZoom ( )
	{
		resetZoom ( 0 );
	}
	
	public void resetZoom ( int waitMilis )
	{
		zoom.reset ( waitMilis );
	}
	
	public String getInZoomSide ( )
	{
		if ( !zoom.inZoom ( ) )
			return null;
		return getLastSide ( );
	}
	
	public CubeSide getCubeSide ( String cs )
	{
		return layouts.get ( cs );
	}

	public SimpleVector getRotationFromMatrix ( )
	{
		float [ ] matrix = getRotationMatrix ( ).getDump ( );
		float beta = (float) Math.asin ( -matrix [ 2 ] );
		float cosB = (float) Math.sqrt ( 1 - matrix [ 2 ] * matrix [ 2 ] );
		
		if ( cosB == 0f )
		{
			float gama = (float) Math.acos ( matrix [ 5 ] );
			if ( matrix [ 4 ] < 0 )
				gama = -gama;
			
			return new SimpleVector ( 0f, beta, gama );
		}
		
		float gama = (float) Math.acos ( matrix [ 0 ] / cosB );
		if ( matrix [ 1 ] * cosB > 0 )
			gama = -gama;
		
		float alfa = (float) Math.acos ( matrix [ 10 ] / cosB );
		if ( matrix [ 6 ] * cosB > 0 )
			alfa = -alfa;
		
		return new SimpleVector ( alfa, beta, gama );
	}
	
	private SimpleVector getVertex ( int cubeIndex )
	{
		cubeIndex = cubeIndeces.get ( cubeIndex );
		cubeIndex *= 3;
		float x = cubeVerteces.get ( cubeIndex++ );
		float y = cubeVerteces.get ( cubeIndex++ );
		float z = cubeVerteces.get ( cubeIndex++ );
		return new SimpleVector ( x, y, z );
	}

	@Override
	public void onZoomInStart(CubeSide caller) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onZoomOutStart(CubeSide caller) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onZoomInEnd(CubeSide caller) {
		if (zoom != null)
			zoom.onZoomInEnd ( );
	}

	@Override
	public void onZoomOutEnd(CubeSide caller) {
		zoom.onZoomOutEnd ( );
		fix.restart ( );
		fix.alikeSkip ( );
	}

	@Override
	public void onZoomOutStarted(CubeSide caller) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onZoomInStarted(CubeSide caller) {
		// TODO Auto-generated method stub
		
	}
	
}
