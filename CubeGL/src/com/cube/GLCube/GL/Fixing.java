package com.cube.GLCube.GL;


import java.util.List;


import com.cube.GLCube.UIConnect.XMLLoad;
import com.threed.jpct.Matrix;
import com.threed.jpct.SimpleVector;

/**
 * The Fixing class is responsible for the animated transition between the state a person leaves the
 * cube into and the state at which the person sees only the closest to this position side rotated
 * so that the text on it is aligned correctly
 * 
 * @author Dimitar Dimitrov
 */
class Fixing
{
	
	boolean skipAlike;
	/**
	 * The amount of time between the last cube activity and the beginning of the animation
	 */
	private long wait;
	/**
	 * The moment at which the cube was last rotated in milliseconds
	 */
	private long lastRotate;
	/**
	 * The amount of time the animation lasts in milliseconds
	 */
	private long animTime;
	/**
	 * The moment at which the animation started in milliseconds
	 */
	private long animStart;
	/**
	 * The possible values of side
	 */
	private List < String > sides;
	/**
	 * The last cube side the cube was rotated to
	 */
	private String side;
	/**
	 * The cube the animation is applied to
	 */
	private Cube cube;
	/**
	 * This vector represents the Euclidean rotational coordinates of the cube when the animation
	 * started
	 */
	private SimpleVector rotationTo;
	/**
	 * This vector represents the Euclidean rotational coordinates of the cube when the animation
	 * ends
	 */
	private SimpleVector rotationFrom;
	/**
	 * Unchangeable list of immutable Integers that contains each side's correct Euclidean
	 * rotational coordinates devided by pi/2. Those coordinates are represented as consecutive
	 * triples. The order in which the sides are stored is Front, Back, Up, Down, Left and Right.
	 */
	private final List < Integer > correctRotation;
	
	/**
	 * Class constructor.
	 * 
	 * @param cube
	 *            the cube associated with the fixing
	 * @see Cube
	 */
	public Fixing ( Cube cube )
	{
		this ( cube, 1000 );
	}
	
	/**
	 * Class constructor.
	 * 
	 * @param cube
	 *            the cube associated with the fixing
	 * @param wait
	 *            the time the animation waits before start fixing
	 * @see Cube
	 */
	public Fixing ( Cube cube, long wait )
	{
		this ( cube, wait, 300 );
	}
	
	/**
	 * Class constructor.
	 * 
	 * @param cube
	 *            the cube associated with the fixing
	 * @param wait
	 *            the time the animation waits before start fixing
	 * @param animTime
	 *            the time it takes for the cube to reposition
	 * @see Cube
	 */
	public Fixing ( Cube cube, long wait, long animTime )
	{
		this.cube = cube;
		this.wait = wait;
		this.animStart = -1;
		this.animTime = animTime;

		correctRotation = XMLLoad.getFixingEuclideanAngles ( cube.activity );
		sides = XMLLoad.getSides ( cube.activity );
		
		restart ( );
	}
	
	/**
	 * Sets the time elapsed since last inactivity to 0.
	 */
	public void restart ( )
	{
		lastRotate = System.currentTimeMillis ( );
	}
	
	/**
	 * Skips the alike check.
	 */
	public void alikeSkip ( )
	{
		skipAlike = true;
	}
	
	/**
	 * Sets the critical inactivity time after which the animation starts to execute to wait.
	 * 
	 * @param wait
	 *            the new time after which the animation starts to execute
	 */
	public void setWait ( long wait )
	{
		this.wait = wait;
	}
	
	public String getSide ( )
	{
		return side;
	}
	
	private void setSide ( String side )
	{
		this.side = side;
	}
	
	/**
	 * Draws the animation and checks the time elapsed since last inactivity. Should be called every
	 * frame.
	 * 
	 * @return returns true if the cube's mesh and/or matrix has been changed and false otherwise
	 */
	public float draw ( )
	{
		long currTime = System.currentTimeMillis ( );
		if ( animStart > 0 && rotationTo != null && rotationFrom != null )//In anim
		{
			cube.setRotationMatrix ( new Matrix ( ) );
			float interpolator = ( currTime - animStart ) / (float) animTime;
			
			if ( interpolator >= 1f )
			{
				animStart = -1;
				interpolator = 1f;
				lastRotate = currTime;
			}
			
			cube.rotateX ( interpolate ( interpolator, rotationFrom.x, rotationTo.x ) );
			cube.rotateY ( interpolate ( interpolator, rotationFrom.y, rotationTo.y ) );
			cube.rotateZ ( interpolate ( interpolator, rotationFrom.z, rotationTo.z ) );
			
			return interpolator;
		}
		
		if ( currTime - lastRotate > wait )
		{
			SimpleVector rot = cube.getRotationFromMatrix ( );
			
			short wholeX = round ( rot.x );
			short wholeY = round ( rot.y );
			short wholeZ = round ( rot.z );
			
			rotationFrom = rot;
			rotationTo = findCorrect ( wholeX, wholeY, wholeZ );
			rotationTo.scalarMul ( (float) ( Math.PI / 2 ) );
			if ( !skipAlike && alike ( rotationFrom, rotationTo ) )
			{
				restart ( );
				animStart = -1;
				rotationFrom = rotationTo = null;
			}
			else
			{
				animStart = currTime;
				skipAlike = false;
			}
			return -1f;
		}
		
		return -1f;
	}
	
	/**
	 * Finds the closest angle from type k*pi/2 from the given angle.
	 * 
	 * @param angle
	 *            the given angle
	 * @return returns the resulting angle divided pi/2
	 */
	private short round ( float angle )
	{
		double quotient = angle / Math.PI * 4d;
		long wholepart = (long) Math.floor ( quotient );
		
		if ( wholepart % 2 == 1 )
			wholepart++ ;
		
		wholepart = wholepart % 8;
		wholepart /= 2;
		
		return (short) wholepart;
		
	}
	
	/**
	 * Calculates the current angle of the animation.
	 * 
	 * @param interpolator
	 *            the current interpolation index where 1 is the end position and 0 is the start
	 *            position
	 * @param from
	 *            the angle at which the animation starts
	 * @param to
	 *            the angle at which the animation ends
	 * @return returns the interpolated angle
	 */
	private float interpolate ( float interpolator, float from, float to )
	{
		float d = to - from;
		return interpolator * d + from;
	}
	
	/**
	 * Searches through correctRotation in order to find the current side the cube needs to be
	 * rotated to and fixes the z coordinate so the side is aligned.
	 * 
	 * @param inputX
	 *            cube's desired alpha rotation before the fix divided by pi/2
	 * @param inputY
	 *            cube's desired beta rotation before the fix divided by pi/2
	 * @param inputZ
	 *            cube's desired gamma rotation before the fix divided by pi/2
	 * @return returns the fixed Euclidean rotation coordinates divided by pi/2 as a SimpleVector
	 *         ( returns null if no match has been found )
	 */
	private SimpleVector findCorrect ( short inputX, short inputY, short inputZ )
	{
		for ( int i = 0; i < correctRotation.size ( ); i += 3 )
		{
			setSide( sides.get ( i / 3 ) );
			final int x = correctRotation.get ( i ), y = correctRotation.get ( i + 1 ), z = correctRotation
					.get ( i + 2 );
			if ( ( x - inputX ) % 4 == 0 && ( y - inputY ) % 4 == 0 )
			{
				SimpleVector res = SimpleVector
						.create ( inputX, inputY, fixZtoClosest ( z, inputZ ) );
				return res;
			}
			if ( ( x - 2 - inputX ) % 4 == 0 && ( y - 2 + inputY ) % 4 == 0 )
			{
				SimpleVector res = SimpleVector.create ( inputX, inputY,
						fixZtoClosest ( 2 + z, inputZ ) );
				return res;
			}
			if ( ( y - 1 ) % 4 == 0 && ( inputY - 1 ) % 4 == 0 )
			{
				SimpleVector res = SimpleVector.create ( inputX, inputY,
						fixZtoClosest ( z - x + inputX, inputZ ) );
				return res;
			}
			if ( ( y + 1 ) % 4 == 0 && ( inputY + 1 ) % 4 == 0 )
			{
				SimpleVector res = SimpleVector.create ( inputX, inputY,
						fixZtoClosest ( z + x - inputX, inputZ ) );
				return res;
			}
		}
		return null;
	}
	
	/**
	 * Fixes a given z rotation coordinate so that the rotation of the cube is minimal
	 * 
	 * @param z
	 *            the desired z rotation coordinate divided by pi/2 before the change
	 * @param target
	 *            the z rotation coordinate divided by pi/2 before the alignment
	 * @return returns the fixed z rotation
	 */
	private float fixZtoClosest ( int z, int target )
	{
		z %= 4;
		if ( z < 0 )
		{
			if ( Math.abs ( z + 4 - target ) < Math.abs ( z - target ) )
				return z + 4;
		}
		else
		{
			if ( Math.abs ( z - 4 - target ) < Math.abs ( z - target ) )
				return z - 4;
		}
		return z;
	}
	
	private boolean alike ( SimpleVector rotationFrom, SimpleVector rotationTo )
	{
		float xDiff = Math.abs ( rotationTo.x - rotationFrom.x );
		float yDiff = Math.abs ( rotationTo.y - rotationFrom.y );
		float zDiff = Math.abs ( rotationTo.z - rotationFrom.z );
		if ( xDiff > 0.001f || yDiff > 0.001f || zDiff > 0.001f )
		{
			return false;
		}
		return true;
	}
}
