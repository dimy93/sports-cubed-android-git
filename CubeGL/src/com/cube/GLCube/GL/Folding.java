
package com.cube.GLCube.GL;

import com.cube.GLCube.UIConnect.XMLLoad;

import java.util.List;

import com.threed.jpct.Matrix;
import com.threed.jpct.Object3D;
import com.threed.jpct.SimpleVector;

class Folding
{
	/**
	 * The cube to apply the animation to.
	 */
	private Cube cube;
	/**
	 * True if the second part of the animation is running and false otherwise.
	 */
	private boolean backFold;
	/**
	 * The amount of time in milliseconds it takes for the animation to finish.
	 */
	private int animTime;
	/**
	 * It stores the starting time of the animation in nanoseconds.
	 */
	private long startAnimTime;
	/**
	 * Final unchangeable list storing the pivot points of the layouts.
	 */
	private final List < Integer > pivots;
	/**
	 * Final unchangeable list storing the ordering of the pivot points of the layouts.
	 */
	private final List < String > ordering;
	private boolean halfFold;
	
	public Folding ( Cube cube )
	{
		this.cube = cube;
		this.halfFold = false;
		animTime = 0;
		startAnimTime = -1;
		
		pivots = XMLLoad.getFoldingPivots ( cube.activity );
		ordering = XMLLoad.getFoldingSides ( cube.activity );
	}
	
	public boolean draw ( )
	{
		if ( animTime == 0 || startAnimTime == -1 )
			return false;
		long currTime = System.nanoTime ( );
		long elapsed = ( currTime - startAnimTime ) / 1000000;
		float interpolator = (float) Math.PI / 2f * ( 1 - (float) elapsed / animTime );
		if ( interpolator < -Math.PI / 2 && halfFold  )
		{
			halfFold = false;
			interpolator = (float) -Math.PI / 2;
			animTime = 0;
			startAnimTime = -1;
			backFold = false;
			rotateY ( "back", interpolator );
			return true;
		}
		if ( interpolator < 0 )
		{
			halfFold = true;
			if ( !backFold )
			{
				cube.objects.get ( "back" ).rotateMesh ( );
				cube.objects.get ( "back" ).setRotationPivot ( getPivot ( "backEnd" ) );
				backFold = true;
				rotateY ( "back", interpolator );
				interpolator = 0f;
			}
			else
			{
				rotateY ( "back", interpolator );
				return true;
			}
		}
		rotateY ( "left", -interpolator );
		rotateY ( "right", interpolator );
		rotateX ( "down", interpolator );
		rotateX ( "up", -interpolator );
		rotateY ( "back", interpolator );
		return true;
	}
	
	public void start ( int time )
	{
		backFold = false;
		animTime = time;
		startAnimTime = System.nanoTime ( );
		
		// Set the position of the back
		Object3D back = cube.objects.get ( "back" );
		back.setRotationPivot ( getPivot ( "backEnd" ) );
		rotateY ( "back", (float) ( Math.PI / 2 ) );
		back.rotateMesh ( );
		back.setRotationMatrix ( new Matrix ( ) );
		
		// Set pivot points
		for ( String side : cube.objects.keySet ( ) )
		{
			SimpleVector sv = getPivot ( side );
			if ( sv != null )
				cube.objects.get ( side ).setRotationPivot ( sv );
		}
	}
	
	
	private void rotateX ( String side, float angle )
	{
		cube.objects.get ( side ).setRotationMatrix ( new Matrix ( ) );
		cube.objects.get ( side ).rotateX ( angle );
	}
	
	private void rotateY ( String side, float angle )
	{
		cube.objects.get ( side ).setRotationMatrix ( new Matrix ( ) );
		cube.objects.get ( side ).rotateY ( angle );
	}
	
	private SimpleVector getPivot ( String s )
	{
		int index = ordering.indexOf ( s );
		if ( index == -1 )
			return null;
		index *= 3;
		return new SimpleVector ( pivots.get ( index++ ), pivots.get ( index++ ),
				pivots.get ( index++ ) );
	}
}
