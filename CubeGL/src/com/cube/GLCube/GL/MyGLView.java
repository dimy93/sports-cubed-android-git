package com.cube.GLCube.GL;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLDisplay;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;

import com.cube.UI.MainActivity;

public class MyGLView extends GLSurfaceView {
	
	protected GLRenderer renderer;

	public MyGLView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initConfig();
	}

	public MyGLView(Context context) {
		super(context);
		initConfig();
	}
	
	private void initConfig(){
		setEGLConfigChooser ( new GLSurfaceView.EGLConfigChooser ( ) {
			public EGLConfig chooseConfig ( EGL10 egl, EGLDisplay display )
			{
				// Ensure that we get a 16bit framebuffer. Otherwise, we'll fall
				// back to Pixelflinger on some device (read: Samsung I7500)
				int [ ] attributes = new int [ ] { EGL10.EGL_DEPTH_SIZE, 16, EGL10.EGL_NONE };
				EGLConfig [ ] configs = new EGLConfig [ 1 ];
				int [ ] result = new int [ 1 ];
				egl.eglChooseConfig ( display, attributes, configs, 1, result );
				return configs [ 0 ];
			}
		} );
		
		renderer = new GLRenderer ( (MainActivity)getContext(), this );
		setRenderer ( renderer );
	}

	@Override
	public Parcelable onSaveInstanceState() {

		Bundle bundle = new Bundle();
		bundle.putParcelable("instanceState", super.onSaveInstanceState());
		bundle.putParcelable("renderState", renderer.onSaveInstanceState());
		return bundle;
	}

	@Override
	public void onRestoreInstanceState(Parcelable state) {

		if (state instanceof Bundle) {
			Bundle bundle = (Bundle) state;
			renderer.onRestoreInstanceState(bundle.getParcelable("renderState"));
			state = bundle.getParcelable("instanceState");
		}
		super.onRestoreInstanceState(state);
	}
	
	@Override
	public void onResume(){
		renderer.onResume();
		super.onResume();
	}
	@Override
	public void onPause(){
		renderer.onPause();
		super.onPause();
	}

}
