
package com.cube.GLCube.GL;

import java.util.LinkedList;

import javax.microedition.khronos.opengles.GL10;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.opengl.GLSurfaceView;
import android.os.Build;

import com.cube.GLCube.UIConnect.ConnectToUI.FutureLayout;
import com.cube.GLCube.UIConnect.Run;


@TargetApi(Build.VERSION_CODES.HONEYCOMB)
@SuppressWarnings ( "rawtypes" )
public abstract class Render implements GLSurfaceView.Renderer, Run.RunnableListener
{
	private boolean initializing = true, initialized = true, wait = false, skip = false,
			running = false, initforce = false;
	private final LinkedList < Run < ? > > runnables;
	protected Activity activity;
	protected GLSurfaceView parent;
	
	public Render ( Activity activity, GLSurfaceView parent )
	{
		//Log.d("CubeGL","OnCreate");
		this.activity = activity;
		this.parent = parent;
		runnables = new LinkedList < Run < ? > > ( );
	}
	
	public abstract boolean onInitializating ( GL10 gl );
	
	public abstract boolean onInitialized ( GL10 gl );
	
	public abstract void onDrawing ( GL10 gl );
	
	public abstract void onSkipFrame ( GL10 gl );
	
	public void runOnGLThread ( Runnable r )
	{
		parent.queueEvent ( r );
	}
	
	public void skipFrame ( )
	{
		skip = true;
	}
	
	@SuppressWarnings ( "unchecked" )
	public void wait ( Run < ? > runnable )
	{
		if ( runnable == null )
			return;
		wait = true;
		runnables.addLast ( runnable );
		runnable.addListener ( this );
	}
	
	@Override
	public void onDrawFrame ( GL10 gl )
	{
		//Log.d("CubeGL","onDraw:");
		if ( initforce ){
			initializing = false; 
			initialized = true;
			wait = false; 
			skip = false;
			running = false;
			runnables.clear();
			initforce = false;
		}
		if ( wait )
		{
			//Log.d("CubeGL","Wait");
			wait = runnables.size ( ) != 0;
			if ( running == false && wait )
			{
				try
				{
					FutureLayout futureLayout = (FutureLayout) runnables.getFirst ( );
					// Delayed in order to fix the layout rotation
					//Log.d("CubeGL","futureLayout:"+futureLayout + " runnables.getFirst ( ):" +runnables.getFirst ( )+" layout:"+futureLayout.getLayout ( ));
					futureLayout.getLayout ( ).postDelayed ( runnables.getFirst ( ), 100 );
				}
				catch ( ClassCastException e )
				{
					activity.runOnUiThread ( runnables.getFirst ( ) );
				}
				running = true;
			}
			onSkipFrame(gl);
			return;
		}
		if ( skip )
		{
			//Log.d("CubeGL","Skip");
			skip = false;
			onSkipFrame(gl);
			return;
		}
		if ( initializing )
		{
			//Log.d("CubeGL","InitIIIIING");
			initializing = this.onInitializating ( gl );
			onSkipFrame(gl);
			return;
		}
		if ( initialized )
		{
			//Log.d("CubeGL","InitEEEED");
			initialized = this.onInitialized ( gl );
			onSkipFrame(gl);
			return;
		}
		//Log.d("CubeGL","Drawinnnnng");
		this.onDrawing ( gl );
	}
	
	@SuppressWarnings ( "unchecked" )
	@Override
	public void onResultComputed ( final Run runnable, Object result )
	{
		runOnGLThread ( new Runnable ( ) {
			
			@Override
			public void run ( )
			{
				running = false;
				runnable.removeListener ( Render.this );
				runnables.poll ( );
			}
		} );
		
	}

	public void onResume() {
		//Log.d("CubeGL","onRestore:"+(wait || initializing || initialized || skip));
		if ( wait || initializing || initialized || skip )
		{
			//activity.recreate();
			Intent starterIntent = activity.getIntent();
			starterIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			activity.startActivity(starterIntent);
			activity.finish();
		}
		//Log.d("CubeGL","Initializing:"+initializing);
		//Log.d("CubeGL","Wait:"+wait);
		
	}
	
	public void onPause() {
		// TODO Auto-generated method stub
		
	}
}
