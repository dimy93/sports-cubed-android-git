
package com.cube.GLCube.GL;


import com.threed.jpct.Camera;
import com.threed.jpct.FrameBuffer;
import com.threed.jpct.Interact2D;
import com.threed.jpct.SimpleVector;

public class RotateAnimation
{
	private float velocityX, velocityY, frictionX, frictionY;
	private Cube owner;
	private long lastDraw;
	private float cubeRadiusPx;
	private static Camera camera;
	
	public RotateAnimation ( Cube owner, float velocityX, float velocityY, float friction )
	{
		this.owner = owner;
		this.velocityX = velocityX;
		this.velocityY = velocityY;
		float absMax = Math.max ( Math.abs ( velocityX ), Math.abs ( velocityY ) );
		this.frictionX = -velocityX / absMax * friction;
		this.frictionY = -velocityY / absMax * friction;
		toAngularVelocity ( );
		lastDraw = System.currentTimeMillis ( );
	}
	
	public static void setCamera ( Camera cam )
	{
		camera = cam;
	}
	
	public void toAngularVelocity ( )
	{
		FrameBuffer fb = owner.getRenderer ( ).getFrameBuffer ( );
		SimpleVector LT = Interact2D.project3D2D ( camera, fb, new SimpleVector ( 1f, 1f, -1f ) );
		SimpleVector RB = Interact2D.project3D2D ( camera, fb, new SimpleVector ( -1f, -1f, -1f ) );
		cubeRadiusPx = (float) ( ( LT.x - RB.x ) / Math.sqrt ( 2 ) );
	}
	
	public boolean draw ( )
	{
		long currentDraw = System.currentTimeMillis ( );
		long elapsed = currentDraw - lastDraw;
		if ( elapsed < 3 )
			return true;
		lastDraw = currentDraw;
		float oldVelocityX = velocityX;
		float oldVelocityY = velocityY;
		velocityX += frictionX;
		velocityY += frictionY;
		float angleX = ( velocityX * elapsed / 1000f ) / -cubeRadiusPx;
		float angleY = ( velocityY * elapsed / 1000f ) / -cubeRadiusPx;
		owner.rotateX ( angleX );
		owner.rotateY ( angleY );
		return oldVelocityX * velocityX > 0 && oldVelocityY * velocityY > 0;
	}
}
