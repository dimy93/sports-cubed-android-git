
package com.cube.GLCube.GL;
import android.view.View;

import com.cube.GLCube.UIConnect.ConnectToUI;
import com.cube.GLCube.UIConnect.CubeSide;
import com.cube.UI.TouchEventListener;
import com.threed.jpct.Camera;
import com.threed.jpct.FrameBuffer;
import com.threed.jpct.Interact2D;
import com.threed.jpct.SimpleVector;

class Zooming
{
	private Cube cube;
	private Camera camera;
	private long animStart;
	private final SimpleVector cubeRB = new SimpleVector ( 1f, 1f, -1f );
	private final SimpleVector cubeLT = new SimpleVector ( -1f, -1f, -1f );
	private boolean inZoomOut = false, inZoomIn = false;
	private long waitForCube = 500;
	private long zoomOutEnd;
	
	public Zooming ( Cube cube, Camera camera )
	{
		this.camera = camera;
		this.cube = cube;
		this.animStart = -1;
		this.zoomOutEnd = -1;
		this.waitForCube = 20;
	}
	
	public void start ( long wait )
	{
		this.animStart = System.currentTimeMillis ( ) + wait;
	}
	
	public boolean draw ( )
	{
		if ( zoomOutEnd > 0 && zoomOutEnd < System.currentTimeMillis ( ) )
		{
			zoomOutEnd = -1;
			inZoomOut = false;
		}
		if ( animStart < 0 || System.currentTimeMillis ( ) < animStart )
			return false;
		
		if ( inZoomIn )
			return false;
		
		final String side = cube.getLastSide ( );
		final CubeSide cs = cube.getLayouts ( ).get ( side );
		if ( cs == null || cs.getVisibility ( ) == View.VISIBLE || TouchEventListener.onSide != null )
			return false;
		inZoomIn = true;
		ConnectToUI.UIAnimationIn ( cs, side, getMeasuredSide ( ), cube.activity );
		
		animStart = -1;
		
		return true;
	}
	
	public void reset ( int waitMilis )
	{
		if ( inZoomOut || inZoomIn )
			return;
		final String side = cube.getLastSide ( );
		final CubeSide cs = cube.getLayouts ( ).get ( side );
		if ( cs == null || cs.getVisibility ( ) == View.INVISIBLE )
			return;
		
		inZoomOut = true;
		ConnectToUI.UIAnimationOut ( cs, side, getMeasuredSide ( ), cube.activity, waitMilis );
		
		animStart = -1;
	}
	
	public float getMeasuredSide ( )
	{
		FrameBuffer fb = cube.getRenderer ( ).getFrameBuffer ( );
		SimpleVector LT = Interact2D.project3D2D ( camera, fb, cubeLT );
		SimpleVector RB = Interact2D.project3D2D ( camera, fb, cubeRB );
		return RB.x - LT.x;
	}
	
	public void onZoomOutEnd ( )
	{
		zoomOutEnd = System.currentTimeMillis ( ) + waitForCube;
	}
	
	public void onZoomInEnd ( )
	{
		inZoomIn = false;
	}
	
	public boolean inZoom ( )
	{
		return inZoomOut || inZoomIn || TouchEventListener.onSide != null;
	}
	
}
