
package com.cube.GLCube.UIConnect;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import android.app.Activity;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

import com.cube.AndroidUI.R.id;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.Animator.AnimatorListener;
import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.animation.ObjectAnimator;

public class ConnectToUI
{
	public static void UIAnimationOut ( final CubeSide layout, final String sideName,
			final float side, final Activity activity, final int waitMilis )
	{
		Runnable r = new Runnable ( ) {
			
			@Override
			public void run ( )
			{
				layout.dispatchOnZoomOutStart ( );
				AnimatorListener listener = new CustomAnimListenerOut ( layout, sideName );
				float scaling = side / layout.getMeasuredHeight ( );
				UIAnim ( layout, sideName, 1f, scaling, listener, waitMilis );
			}
			
		};
		activity.runOnUiThread ( r );
	}
	
	public static void UIAnimationIn ( final CubeSide layout, final String sideName,
			final float side, final Activity activity )
	{
		Runnable r = new Runnable ( ) {
			
			@Override
			public void run ( )
			{
				layout.dispatchOnZoomInStart ( );
				AnimatorListener listener = new CustomAnimListenerIn ( layout );
				layout.setVisibility ( View.VISIBLE );
				layout.bringToFront ( );
				float scaling = side / layout.getMeasuredHeight ( );
				UIAnim ( layout, sideName, scaling, 1f, listener, 0 );
			}
			
		};
		activity.runOnUiThread ( r );
	}
	
	public static List < FutureLayout < Bitmap >> loadTexture ( final List < String > sides )
	{
		List < FutureLayout < Bitmap >> result = new ArrayList < FutureLayout < Bitmap >> ( );
		for ( String side : sides )
		{
			result.add ( new FutureLayout < Bitmap > ( side ) {
				
				@Override
				public void run ( )
				{
					CubeSide layout = getLayout ( );
					setResult ( layout.getScreenViewBitmap ( ) );
				}
			} );
		}
		return result;
	}
	
	public static Run < HashMap < String, CubeSide >> createLayouts ( final List < String > sides,
			final int sideSize, final Activity activity )
	{
		Run < HashMap < String, CubeSide >> r = new Run < HashMap < String, CubeSide >> ( ) {
			@Override
			public void run ( )
			{
				setResult ( layouts ( sides, sideSize, activity ) );
			}
		};
		
		return r;
	}
	
	public static abstract class FutureLayout < T > extends Run < T >
	{
		private CubeSide layout;
		private String side;
		
		public FutureLayout ( String side )
		{
			this.side = side;
		}
		
		public void setLayout ( CubeSide layout )
		{
			Log.d("CubeGL","setLayout:"+layout+" in " + this);
			this.layout = layout;
		}
		
		public CubeSide getLayout ( )
		{
			return layout;
		}
		
		public String getLayoutName ( )
		{
			return side;
		}
	}
	
	private static void UIAnim ( CubeSide layout, String side, float scalingSt, float scalingEn,
			AnimatorListener listener, int waitMilis )
	{
		AnimatorSet set = new AnimatorSet ( );
		set.addListener ( listener );
		ObjectAnimator oax = ObjectAnimator.ofFloat ( layout, "scaleX", scalingSt, scalingEn );
		ObjectAnimator oay = ObjectAnimator.ofFloat ( layout, "scaleY", scalingSt, scalingEn );
		set.playTogether ( oax, oay );
		set.setDuration ( 800 );
		set.setStartDelay ( waitMilis );
		set.start ( );
	}
	
	private static HashMap < String, CubeSide > layouts ( final List < String > sides,
			final int sideSize, final Activity activity )
	{
		final HashMap < String, CubeSide > layouts = new HashMap < String, CubeSide > ( );
		final HashMap < String, Integer > ids = new HashMap < String, Integer > ( );
		ids.put ( "left", id.cube_left );
		ids.put ( "right", id.cube_right );
		ids.put ( "front", id.cube_front );
		ids.put ( "back", id.cube_back );
		ids.put ( "up", id.cube_top );
		ids.put ( "down", id.cube_bottom );
		
		// Fetch RelativeLayouts and fix their properties
		for ( String name : sides )
		{
			CubeSide side = (CubeSide) activity.findViewById ( ids.get ( name ) );
			layouts.put ( name, side );
			side.setVisibility ( RelativeLayout.INVISIBLE );
			LayoutParams rp = (LayoutParams) side.getLayoutParams ( );
			rp.width = rp.height = Math.min ( sideSize, sideSize );
			side.invalidate ( );
			side.requestLayout ( );
		}
		return layouts;
	}
	
	private static class CustomAnimListenerOut implements AnimatorListener
	{
		private CubeSide layout;
		private String side;
		private boolean stop = false;
		
		public CustomAnimListenerOut ( CubeSide layout, String side )
		{
			this.layout = layout;
			this.side = side;
		}

		public void onAnimationEnd ( Animator animator )
		{
			final Bitmap b = layout.getScreenViewBitmap ( );
			CountDownLatch wait = layout.setTextureAndDraw ( b, side );
			try
			{
				if(!stop)
				{
					wait.await ( );
				}
			}
			catch ( InterruptedException e )
			{
			}
			layout.setVisibility ( View.INVISIBLE );
			layout.dispatchOnZoomOutEnd ( );
		}
		
		public void onAnimationStart ( Animator arg0 )
		{
			layout.dispatchOnZoomOutStarted ( arg0 );
		}
		
		public void onAnimationRepeat ( Animator arg0 )
		{
		}
		
		public void onAnimationCancel ( Animator arg0 )
		{
			stop = true;
			arg0.end();
		}
	};
	
	private static class CustomAnimListenerIn implements AnimatorListener
	{
		private CubeSide layout;
		
		public CustomAnimListenerIn ( CubeSide layout )
		{
			this.layout = layout;
		}
		
		public void onAnimationEnd ( Animator animator )
		{
			//Note: called on either End or Cancel
			layout.dispatchOnZoomInEnd ( );
		}
		
		public void onAnimationStart ( Animator arg0 )
		{
			layout.dispatchOnZoomInStarted ( arg0 );
		}
		
		public void onAnimationRepeat ( Animator arg0 )
		{
		}
		
		public void onAnimationCancel ( Animator arg0 )
		{
					arg0.end();
		}
	};
}
