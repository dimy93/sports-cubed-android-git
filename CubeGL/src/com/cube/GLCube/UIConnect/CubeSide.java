
package com.cube.GLCube.UIConnect;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.OnHierarchyChangeListener;
import android.widget.RelativeLayout;

import com.cube.GLCube.GL.Cube;
import com.cube.UI.TouchEventListener;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.view.ViewHelper;
import com.threed.jpct.FrameBuffer;

public class CubeSide extends RelativeLayout implements OnHierarchyChangeListener
{
	protected Cube owner;
	protected int cubeTextureSize;
	protected int cubeTextureQuality;
	private boolean reflection;
	private ArrayList<CubeSideListener> listeners = new ArrayList<CubeSideListener>();
	private Animator currentAnimation = null;
	private boolean paused=false;
	protected boolean forceZoomIn = false;
	protected boolean bitmapDrawing;
	private int initWidth;
	private int initHeight;
	private boolean initZoom = false;
	
	public CubeSide ( Context context, AttributeSet attrs, int defStyle )
	{
		super ( context, attrs, defStyle );
		setScreenshotQuality ( );
		setOnHierarchyChangeListener ( this );
		setReflectionUse ( );
		
	}
	
	public CubeSide ( Context context, AttributeSet attrs )
	{
		super ( context, attrs );
		setScreenshotQuality ( );
		setOnHierarchyChangeListener ( this );
		setReflectionUse ( );
	}
	
	public CubeSide ( Context context )
	{
		super ( context );
		setBackgroundColor ( 0xFFFFFFFF );
		setScreenshotQuality ( );
		setOnHierarchyChangeListener ( this );
		setReflectionUse ( );
	}
	
	public void setOwner ( Cube owner )
	{
		this.owner = owner;
		Activity a = (Activity) this.getContext ( );
		a.runOnUiThread ( new Runnable ( ) {
			
			@Override
			public void run ( )
			{
				setCubeSideDimens ( );
			}
			
		} );
		if ( initZoom )
		{
			this.post(new Runnable() {
				
				@Override
				public void run() {
					onFinishScreenShot();
				}
			});
		}
	}
	
	public void onChildViewAdded ( View child, int id )
	{
		
	}
	
	public void setReflectionUse ( )
	{
		try
		{
			Class < ? > clazz;
			clazz = Class.forName ( "android.view.View" );
			Method m = clazz.getDeclaredMethod ( "createSnapshot", Bitmap.Config.class, int.class,
					boolean.class );
			m.setAccessible ( true );
			m.invoke ( this, Bitmap.Config.RGB_565, 0xFFFFFFFF, false );
			reflection = true;
		}
		catch ( Exception e )
		{
			reflection = false;
		}
	}
	
	@Override
	public void onChildViewAdded ( View parent, View child )
	{
		onChildViewAdded ( child, child.getId ( ) );
		try
		{
			ViewGroup vg = (ViewGroup) child;
			vg.setOnHierarchyChangeListener ( this );
			for ( int i = 0; i < vg.getChildCount ( ); ++i )
			{
				View nextChild = vg.getChildAt ( i );
				onChildViewAdded ( vg, nextChild );
			}
		}
		catch ( ClassCastException e )
		{
			
		}
	}
	
	public void onChildViewRemoved ( View child, int id )
	{
		
	}
	
	@Override
	public void onChildViewRemoved ( View parent, View child )
	{
		onChildViewRemoved ( child, child.getId ( ) );
	}
	
	public void setBackgroundColor ( int color )
	{
		int filter = 0xFF000000;
		if ( ( filter & color ) != filter )
			throw new IlligalSolidColor ( );
		super.setBackgroundColor ( color );
	}
	
	public Bitmap getScreenViewBitmap ( )
	{
		bitmapDrawing = true;
		if ( reflection )
		{
			Bitmap temp = null, pic = null;
			try
			{
				Class < ? > clazz = Class.forName ( "android.view.View" );
				Method m = clazz.getDeclaredMethod ( "createSnapshot", Bitmap.Config.class,
						int.class, boolean.class );
				m.setAccessible ( true );
				
				temp = (Bitmap) m.invoke ( this, Bitmap.Config.RGB_565, 0xFFFFFFFF, false );
				pic = Bitmap.createScaledBitmap ( temp, cubeTextureSize, cubeTextureSize, false );
				temp.recycle ( );
				return pic;
			}
			catch ( Exception e )
			{
				if ( temp != null )
				{
					temp.recycle ( );
				}
				if ( pic != null )
				{
					pic.recycle ( );
				}
				reflection = false;
			}
		}
		View table = this;
		table.setDrawingCacheQuality ( cubeTextureQuality );
		table.setDrawingCacheEnabled ( true );
		table.buildDrawingCache ( true );
		Bitmap pic = Bitmap.createScaledBitmap ( table.getDrawingCache ( ), cubeTextureSize,
				cubeTextureSize, false );
		table.setDrawingCacheEnabled ( false ); // clear drawing cache
		bitmapDrawing = false;
		return pic;
	}
	
	@Override
	protected void dispatchDraw(Canvas c){
		super.dispatchDraw(c);
		if ( bitmapDrawing )
		{
			onFinishScreenShot();
		}
	}
	
	protected void onFinishScreenShot(){
		if (forceZoomIn && owner != null) {
			setInitSize( getInitWidth(), getInitHeight() );
			onInitialSizeSet( getInitWidth(), getInitHeight() );
			setVisibility(View.VISIBLE);
			((TouchEventListener) getParent()).onZoomInEnd(this);
			owner.onZoomInEnd(this);
			forceZoomIn = false;
			initZoom = false;
		}
		if (forceZoomIn && owner == null )
			initZoom = true;
	}
	
	protected int getInitWidth(){
		return initWidth;
	}
	
	protected int getInitHeight(){
		return initHeight;
	}
	
	protected void onInitialSizeSet( int w, int h ){
		
	}
	
	private void setInitSize( int w, int h )
	{	
		RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) this.getLayoutParams ( );
		lp.width = w;
		lp.height = h;
		lp.addRule ( CENTER_IN_PARENT );
		invalidate();
		requestLayout();
	}
	
	public void onLayoutChanged ( )
	{
		setCubeSideDimens ( );
	}
	
	public void onPause(){
		paused = true;
		if( currentAnimation != null )
		{
			currentAnimation.cancel();
		}
	}
	
	public void onResume(){
		paused = false;
	}
	
	private void setCubeSideDimens ( )
	{
		try {
			RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) getLayoutParams();
			lp.width = lp.height = Math.min(getGLRenderHeight(),
					getGLRenderWidth());
		} catch (NullPointerException nExp) {
			
		}
		invalidate ( );
		requestLayout ( );
	}
	
	// onZoomInEnd
	protected boolean onZoomInEnd ( )
	{
		return true;
	}
	
	protected void dispatchOnZoomInEnd() 
	{
		dispatchOnZoomInEnd(false);
	}

	protected void dispatchOnZoomInEnd(boolean force) 
	{
		currentAnimation = null;
		if (force || onZoomInEnd()) 
		{
			for (CubeSideListener listener : listeners) 
			{
				listener.onZoomInEnd(this);
			}
		}
	}
	// onZoomInStart
	protected boolean onZoomInStart( )
	{
		return true;
	}
	
	protected void dispatchOnZoomInStart() 
	{
		dispatchOnZoomInStart(false);
	}

	protected void dispatchOnZoomInStart(boolean force) 
	{
		if (force || onZoomInStart()) 
		{
			for (CubeSideListener listener : listeners) 
			{
				listener.onZoomInStart(this);
			}
		}
	}
	// onZoomInStarted
	protected boolean onZoomInStarted( )
	{
		return true;
	}

	protected void dispatchOnZoomInStarted( Animator animator ) 
	{
		dispatchOnZoomInStarted(animator,false);
	}

	protected void dispatchOnZoomInStarted( Animator animator, boolean force ) 
	{
		currentAnimation  = animator;
		if (force || onZoomInStarted()) 
		{
			for (CubeSideListener listener : listeners) 
			{
				listener.onZoomInStarted(this);
			}
		}
	}

    // onZoomOutEnd
	protected boolean onZoomOutEnd ( )
	{
		return true;
	}
	
	protected void dispatchOnZoomOutEnd ( )
	{
		dispatchOnZoomOutEnd(false);
	}
	
	protected void dispatchOnZoomOutEnd ( boolean force )
	{
		currentAnimation = null;
		if ( force || onZoomOutEnd() )
		{
			for ( CubeSideListener listener : listeners )
			{
				listener.onZoomOutEnd(this);
			}
		}
	}
	// onZoomOutStart
	protected boolean onZoomOutStart ( )
	{
		return true;
	}
	
	protected void dispatchOnZoomOutStart ( )
	{
		dispatchOnZoomOutStart(false);
	}
	
	protected void dispatchOnZoomOutStart ( boolean force )
	{
		if ( force || onZoomOutStart() )
		{
			for ( CubeSideListener listener : listeners )
			{
				listener.onZoomOutStart(this);
			}
		}
	}
	
	// onZoomOutStarted
	protected boolean onZoomOutStarted ( )
	{
		return true;
	}
	
	protected void dispatchOnZoomOutStarted ( Animator animator )
	{
		dispatchOnZoomOutStarted( animator, false );
	}
	
	protected void dispatchOnZoomOutStarted ( Animator animator, boolean force )
	{
		currentAnimation = animator;
		if (paused)
			animator.cancel();
		if ( force || onZoomOutStarted() )
		{
			for ( CubeSideListener listener : listeners )
			{
				listener.onZoomOutStarted(this);
			}
		}
	}
	
	public void onCubeRotateWhileZoom ( float scaleSpan )
	{
		if ( scaleSpan > 1f )
			return;
		if ( owner == null )
			return;
		owner.resetZoom ( );
	}
	
	public CountDownLatch setTextureAndDraw ( Bitmap bitmap, String side )
	{
		if ( owner == null )
			return null;
		return owner.setTextureAndDraw ( bitmap, side );
	}
	
	public int getGLRenderHeight ( )
	{
		return owner.getRenderer ( ).getFrameBuffer ( ).getHeight ( );
	}
	
	public int getGLRenderWidth ( )
	{
		return owner.getRenderer ( ).getFrameBuffer ( ).getWidth ( );
	}
	
	@Override
	public void setVisibility ( int visibility )
	{
		super.setVisibility ( visibility );
		if ( visibility == View.INVISIBLE || visibility == View.GONE )
		{
			setNullSize ( );
		}
		
		else
			if ( visibility == View.VISIBLE )
			{
				this.bringToFront ( );
				setSize ( );
				ViewHelper.setAlpha ( this, 1f );
			}
		
		this.requestLayout ( );
	}
	
	public void zoomOut ( int waitMilis )
	{
		owner.resetZoom ( waitMilis );
	}
	
	protected void setSize ( )
	{
		if ( owner == null )
			return;
		
		final FrameBuffer fb = owner.getRenderer ( ).getFrameBuffer ( );
		final int w = fb.getWidth ( );
		final int h = fb.getHeight ( );
		final int side = Math.min ( w, h );
		setInitSize(side, side);
	}
	
	private void setNullSize ( )
	{
		// RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams)
		// this.getLayoutParams ( );
		// lp.width = lp.height = 0;
		ViewHelper.setAlpha ( this, 0f );
	}
	
	protected void setScreenshotQuality ( )
	{
		long heapSize = Runtime.getRuntime ( ).maxMemory ( );
		int heap_power = (int) Math.floor ( Math.log ( heapSize ) / Math.log ( 2 ) );
		int texture_power_aproximation = ( heap_power - 9 ) / 2;
		cubeTextureSize = (int) Math.round ( Math.pow ( 2, texture_power_aproximation ) );
		if ( cubeTextureSize < 128 )
			cubeTextureQuality = DRAWING_CACHE_QUALITY_LOW;
		else
			cubeTextureQuality = DRAWING_CACHE_QUALITY_HIGH;
	}
	
	private class IlligalSolidColor extends RuntimeException
	{
		private static final long serialVersionUID = 1L;
	}
	
	public void addListener( CubeSideListener csl ){
		if ( !listeners.contains( csl ) )
			listeners.add(csl);
	}
	public void removeListener( CubeSideListener csl ){
		listeners.remove(csl);
	}
	
	@Override
	  public Parcelable onSaveInstanceState() {

	    Bundle bundle = new Bundle();
	    bundle.putParcelable("instanceState", super.onSaveInstanceState());
	    bundle.putBoolean("zoomed", TouchEventListener.onSide == this);
	    RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) this
				.getLayoutParams();
	    bundle.putInt("width", lp.width);
	    bundle.putInt("height", lp.height);
	    return bundle;
	  }

	  @Override
	  public void onRestoreInstanceState(Parcelable state) {

	    if (state instanceof Bundle) {
	      Bundle bundle = (Bundle) state;
	      boolean zoomed = bundle.getBoolean("zoomed");
	      if ( zoomed )
	      {
	    	  forceZoomIn = true;
	    	  ((TouchEventListener)getParent()).onZoomInEnd(this);
	    	  initWidth = bundle.getInt("width");
	    	  initHeight = bundle.getInt("height");
	      }
	      state = bundle.getParcelable("instanceState");
	    }
	    super.onRestoreInstanceState(state);
	  }
	  
	public static interface CubeSideListener{
		public void onZoomInStart(CubeSide caller);
		public void onZoomOutStart(CubeSide caller);
		public void onZoomInEnd(CubeSide caller);
		public void onZoomOutEnd(CubeSide caller);
		public void onZoomOutStarted(CubeSide caller);
		public void onZoomInStarted(CubeSide caller);
		
	}
}
