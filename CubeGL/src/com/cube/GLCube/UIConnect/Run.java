
package com.cube.GLCube.UIConnect;

import java.util.LinkedList;

public abstract class Run < T > implements Runnable
{
	private LinkedList < RunnableListener < T > > listeners = new LinkedList < RunnableListener < T > > ( );
	private T result;
	
	protected void setResult ( final T result )
	{
		this.result = result;
		for ( RunnableListener < T > listener : listeners )
		{
			listener.onResultComputed ( this, result );
		}
	}
	
	public T getLastResult (){
		return result;
	}
	
	public void addListener ( RunnableListener < T > listener )
	{
		listeners.addLast ( listener );
	}
	
	public void removeListener ( RunnableListener < T > listener )
	{
		listeners.remove ( listener );
	}
	
	public interface RunnableListener < T >
	{
		public void onResultComputed ( Run < T > runnable, T result );
	}
}
