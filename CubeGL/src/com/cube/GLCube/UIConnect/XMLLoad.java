
package com.cube.GLCube.UIConnect;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.content.Context;

import com.cube.AndroidUI.R;

public class XMLLoad
{
	public static List < Integer > getFoldingPivots ( Context context )
	{
		return getIntegers ( context, R.array.FoldingPivots );
	}
	
	public static List < Integer > getFixingEuclideanAngles ( Context context )
	{
		return getIntegers ( context, R.array.FixingEuclideanAngles );
	}
	
	public static List < Integer > getcubeIndeces ( Context context )
	{
		return getIntegers ( context, R.array.cubeIndeces );
	}
	
	public static List < String > getFoldingSides ( Context context )
	{
		return getStrings ( context, R.array.FoldingSides );
	}
	
	public static List < String > getSides ( Context context )
	{
		return getStrings ( context, R.array.Sides );
	}
	
	public static List < Float > getCubeVerteces ( Context context )
	{
		return getFloats ( context, R.array.cubeVerteces );
	}
	
	private static List < String > getStrings ( Context context, int arrayIndex )
	{
		String [ ] strs = context.getResources ( ).getStringArray ( arrayIndex );
		List < String > list = new ArrayList < String > ( );
		
		Collections.addAll ( list, strs );
		return Collections.unmodifiableList ( list );
	}
	
	private static List < Integer > getIntegers ( Context context, int arrayIndex )
	{
		int [ ] intArr = context.getResources ( ).getIntArray ( arrayIndex );
		List < Integer > list = new ArrayList < Integer > ( );
		
		for ( int i : intArr )
		{
			list.add ( i );
		}
		return Collections.unmodifiableList ( list );
	}
	
	private static List < Float > getFloats ( Context context, int arrayIndex )
	{
		String [ ] strs = context.getResources ( ).getStringArray ( arrayIndex );
		List < Float > list = new ArrayList < Float > ( );
		
		for ( String s : strs )
		{
			list.add ( Float.parseFloat ( s ) );
		}
		
		return Collections.unmodifiableList ( list );
	}
}
