package com.cube.SQL;

import android.content.Context;
import android.util.AttributeSet;

import com.cube.GLCube.UIConnect.CubeSide;

public abstract class Filter extends CubeSide {
	private boolean mChanged = true;
	
	public Filter ( Context context, AttributeSet attrs, int defStyle )
	{
		super ( context, attrs, defStyle );
		SQLQuery.setFilter ( this );
	}
	
	public Filter ( Context context, AttributeSet attrs )
	{
		super ( context, attrs );
		SQLQuery.setFilter ( this );
	}
	
	public Filter ( Context context )
	{
		super ( context );
		SQLQuery.setFilter ( this );
	}
	
	protected void change()
	{
		mChanged = true;
	}
	
	public void resetChangedStatus()
	{
		mChanged = false;
	}
	
	public boolean isChanged()
	{
		return mChanged;
	}
}
