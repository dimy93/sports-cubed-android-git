
package com.cube.SQL;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Vector;

import org.osmdroid.util.GeoPoint;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.util.SparseIntArray;

import com.cube.UI.cubeSides.Map;
import com.cube.UI.cubeSides.PriceFilter;
import com.cube.UI.cubeSides.SportsFilter;
import com.cube.UI.cubeSides.TimeFilter;
import com.cube.osmdroid.overlays.CustomMarker;

public class SQLQuery
{
	private static Map map;
	private static TimeFilter time;
	private static SportsFilter sports;
	private static SQLiteDatabase db;
	private static Context context;
	private static PriceFilter price;
	private static Query query;
	
	public static void setContext ( Context c )
	{
		context = c;
		try
		{
			String path = context.getFilesDir ( ) + "/database.sqlite";
			db = SQLiteDatabase.openDatabase ( path, null, SQLiteDatabase.OPEN_READONLY
					| SQLiteDatabase.NO_LOCALIZED_COLLATORS );
		}
		catch ( Exception e )
		{
			e.getMessage ( );
			Cursor cursor = SQLiteDatabase.openOrCreateDatabase ( ":memory:", null ).rawQuery (
					"select sqlite_version() AS sqlite_version", null );
			String sqliteVersion = "";
			while ( cursor.moveToNext ( ) )
			{
				sqliteVersion += cursor.getString ( 0 );
			}
			sqliteVersion = "Incompatible sqlite version : " + sqliteVersion;
			cursor.close ( );
			Log.e ( "Cube", sqliteVersion );
		}
	}
	
	static void setFilter ( Filter filter )
	{
		if (filter instanceof SportsFilter)
		{ 
			sports = (SportsFilter)filter;
			return;
		}
		if (filter instanceof TimeFilter)
		{ 
			time = (TimeFilter)filter;
			return;
		}
		if (filter instanceof PriceFilter)
		{ 
			price = (PriceFilter)filter;
			return;
		}
		throw new ClassCastException("SQLQuery's setFilter method called with illigal filter.");
	}
	
	public static void setMap ( Map mapView )
	{
		map = mapView;
	}
	
	public static boolean setUped()
	{
		return db != null && map != null && time != null && sports != null && price != null ;
	}
	
	public static boolean sidesChanged()
	{
		if( !setUped() )
			throw new SQLiteException("SQLQuery class not setuped properly.");
		return time.isChanged() || sports.isChanged() || price.isChanged();
	}
	
	@SuppressWarnings("deprecation")
	public static boolean setPlaces( )
	{
		if( !setUped() )
			throw new SQLiteException("SQLQuery class not setuped properly.");
		if ( !sidesChanged() )
		{
			return false;
		}
		time.resetChangedStatus();
		sports.resetChangedStatus();
		price.resetChangedStatus();
		if ( query != null && query.isAlive() )
			query.stop();
		query = new Query( Query.Type.SPORT, price.getMaxPrice(), price.isPriceTypeMember(), time.getDays(), sports.getSports() );
		query.start();
		return true;
	}
	
	public static void setSports ( )
	{
		if ( db == null || sports == null )
		{
			return;
		}
		Cursor cursor = getSports ( );
		int index_name = cursor.getColumnIndex ( "name" );
		int lenght = cursor.getCount ( );
		String [ ] labels = new String [ lenght ];
		cursor.moveToNext ( );
		for ( int i = 0; i < lenght; i++ , cursor.moveToNext ( ) )
		{
			String db_res = cursor.getString ( index_name );
			labels [ i ] = Character.toUpperCase ( db_res.charAt ( 0 ) )
					+ db_res.subSequence ( 1, db_res.length ( ) ).toString ( );
		}
		Log.d("CubeGL","SportsSet");
		sports.setSports ( labels );
		cursor.close ( );
	}
	
	public static Cursor getSports ( )
	{
		return getSportsOrderByName ( );
	}
	
	public static Cursor getSportsOrderById ( )
	{
		return executeQuery ( "select * from sports order by id" );
	}
	
	public static Cursor getSportsOrderByName ( )
	{
		return executeQuery ( "select * from sports order by name" );
	}
	
	public static Cursor getSportByName ( String name )
	{
		return executeQuery ( "select * from sports where name='" + name + "'" );
	}
	
	public static Cursor getSportsByClub ( String club_id )
	{
		String query = "select sports.name from sports, clubosport "
				+ "where clubosport.club_id = " + club_id + " and sports.id = clubosport.sport_id";
		query += " group by sports.name";
		return executeQuery ( query );
	}
	
	public static Cursor executeQuery ( String query )
	{
		return executeQuery ( query, null );
	}
	
	
	public static Cursor executeQuery ( String query, String [ ] args )
	{
		if ( db == null )
		{
			return null;
		}
		return db.rawQuery ( query, args );
	}
	
	private static HashMap < String, Integer > getColumns ( Cursor c )
	{
		HashMap < String, Integer > result = new HashMap < String, Integer > ( );
		for ( int i = 0; i < c.getColumnCount ( ); i++ )
		{
			result.put ( c.getColumnName ( i ), i );
		}
		return result;
	}
	
	private static int getColumnIndex ( HashMap < String, Integer > values, String column )
	{
		Integer i = values.get ( column );
		if ( i != null )
			return i;
		for ( Entry < String, Integer > entry : values.entrySet ( ) )
		{
			if ( entry.getKey ( ).endsWith ( "." + column ) )
			{
				return entry.getValue ( );
			}
		}
		return -1;
	}
	
	
	private static class Query extends Thread {
		
		private static final String SCHEDULE_TABLE = "club_schedule";
		private static final String CLUBS_TABLE = "clubs";
		private static final String ENHANCED_TABLE = "enhanced_table";
		private static final String ACTIVITIES_CLUB_TABLE = "clubothing_for_activities";
		
		private static final String MEMBER = "price_member";
		private static final String NON_MEMBER = "price_nonmember";
		private static final String DAY = "day_id";
		private static final String PRICE = "final_price";
		private static final String DAYS = "final_days";
		private static final String ACTIVITIES = "activities";
		private static final String SPORTS = "sport";
		private static final String HAVE_INFORMATION = "have_information";
		
		private static final SparseIntArray DAY_FLAGS = new SparseIntArray();
		
		private final int days;
		private final float maxPrice;
		private final boolean member;
		private final Type tables;
		private final Vector<String> sports;
		
		public static enum Type {
		    FACILITY("club_facilities","facilities","facility_id"), SPORT("clubosport","sports","sport_id");
		    
		    private final String table;
		    private final String clubTable;
		    private final String clubTableId;
		    Type( final String clubTable, final String table, final String clubTableId ) {
		    	this.clubTable = clubTable;
		        this.table = table;
		        this.clubTableId = clubTableId;
		    }
		    public String getTable(){
		    	return table;
		    }
		    public String getClubTable(){
		    	return clubTable;
		    }
		    public String getClubTableId(){
		    	return clubTableId;
		    }
		}
		
		public Query( Type type, float maxPrice, boolean member, int days, Vector<String> sports ){
			for ( int i = 1; i < 8; i++ )
			{
				DAY_FLAGS.append(i, TimeFilter.Day.getDay ( i ));
			}
			DAY_FLAGS.append(8, TimeFilter.Day.getDay ( 8 ) - 1 );
			DAY_FLAGS.append(9, TimeFilter.Day.getDay ( 6 ) - 1 );
			DAY_FLAGS.append(10, DAY_FLAGS.get(8) & ~ DAY_FLAGS.get(9) );
			
			this.tables = type;
			this.maxPrice = maxPrice;
			this.member = member;
			this.days = days;
			this.sports = sports;
		}
		
		private String initQuery(){
			final String IS_NOT_NULL = " is not null ";
			final String SEPARATOR = " , ";
			
			final String MEMBERSHIP =  member ? MEMBER : NON_MEMBER;
			final String SPORT_OR_FACILITY_PRICE = tables.getClubTable() + "." + MEMBERSHIP;
			final String SCHEDULE_PRICE = SCHEDULE_TABLE + "." + MEMBERSHIP;
			final String CLUBS_PRICE = CLUBS_TABLE + "." + MEMBERSHIP;
			final String FINAL_PRICE = "case " +
											"when " + SPORT_OR_FACILITY_PRICE + IS_NOT_NULL +
					                             "then " + SPORT_OR_FACILITY_PRICE + " " +
					                        "when " + SCHEDULE_PRICE + IS_NOT_NULL +
					                        	 "then " + SCHEDULE_PRICE + " " +
					                        	 "else " + CLUBS_PRICE + " " +
					                   "end as " + PRICE;
			
			
			final String SPORT_OR_FACILITY_DAY = tables.getClubTable() + "." + DAY;
			final String SCHEDULE_DAY = SCHEDULE_TABLE + "." + DAY;
			final String FINAL_DAYS = "case " +
									 	  "when " + SPORT_OR_FACILITY_DAY + " <> 8 " + 
									 	 	   "then " + SPORT_OR_FACILITY_DAY + " " +
									 	  "when " + SCHEDULE_DAY + IS_NOT_NULL +
					                           "then " + SCHEDULE_DAY + " " +
					                           "else " + SPORT_OR_FACILITY_DAY + " " +
					                 "end as " + DAYS;
			
			final String ACTIVITIES_COLUMN = "group_concat( distinct upper(substr(" + ACTIVITIES + ".name, 1, 1)) || substr(" + ACTIVITIES + ".name, 2) ) as " + ACTIVITIES ;
			
			final String[] columns = new String[]{ ACTIVITIES_COLUMN, FINAL_DAYS, FINAL_PRICE, CLUBS_TABLE + ".name" + " as " + "name", 
					                               CLUBS_TABLE + ".address"  + " as " + "address", tables.getTable() + ".name as " + SPORTS,
					                               CLUBS_TABLE + ".longtitude" + " as " + "longtitude", CLUBS_TABLE + ".latitude"  + " as " + "latitude", 
					                               CLUBS_TABLE + ".postcode" + " as " + "postcode", CLUBS_TABLE + ".comment" + " as " + "comment",
					                               CLUBS_TABLE + ".phone" + " as " + "phone", CLUBS_TABLE + ".email" + " as " + "email",
					                               CLUBS_TABLE + ".website" + " as " + "website" }; 
			String COLUMNS = "";
			for ( String col : columns )
			{
				COLUMNS += SEPARATOR + col ;
			}
			COLUMNS = COLUMNS.substring(SEPARATOR.length());
			
			final String AND_SEPARATOR = " and ";
			final String HAVE_INFORMATION_PRICE = this.isPriceSet() ? PRICE + IS_NOT_NULL : "1";
			final String HAVE_INFORMATION_DAYS = this.areDaysSet() ? DAYS + IS_NOT_NULL : "1";
			final String HAVE_INFORMATION_PRICE_DAYS = " ( " + HAVE_INFORMATION_PRICE + AND_SEPARATOR + HAVE_INFORMATION_DAYS + " ) ";
			final String HAVE_INFORMATION_COLUMN = "( sum" + HAVE_INFORMATION_PRICE_DAYS + " = count() ) as " + HAVE_INFORMATION; 
			
			final String[] columns_export = new String[]{ HAVE_INFORMATION_COLUMN, ACTIVITIES, "name", "address","longtitude","latitude", 
					                                      "postcode", "comment", "phone", "email", "website" }; 
			String COLUMNS_EXPORT = "";
			for (String col : columns_export)
			{
				COLUMNS_EXPORT += SEPARATOR + col;
			}
			COLUMNS_EXPORT = COLUMNS_EXPORT.substring(SEPARATOR.length());
			
			
			
			final String[] JOIN_CONDITIONS = new String[]{
                    CLUBS_TABLE + ".id = "+ SCHEDULE_TABLE +".club_id",
                    CLUBS_TABLE + ".id" + " = " + this.tables.getClubTable() + ".club_id",
                    this.tables.getClubTable() + "." + this.tables.getClubTableId() + " = " + this.tables.getTable() + ".id",
                    CLUBS_TABLE + ".id" + " = " + Query.ACTIVITIES_CLUB_TABLE + ".club_id",
                    Query.ACTIVITIES_CLUB_TABLE + "." + this.tables.getClubTableId() + " = " + ACTIVITIES + ".id",
                  };
			
			final String JOIN_SEPARATOR = " left outer join ";
			
			final String ACTIVITIES_TABLE = tables.getTable() + " as " + ACTIVITIES;
			final String ACTIVITIES_CLUB_TABLE = tables.getClubTable() + " as " + Query.ACTIVITIES_CLUB_TABLE; 		                                     

			final String[] tables = new String[]{ CLUBS_TABLE, SCHEDULE_TABLE, this.tables.getClubTable(),
					                              this.tables.getTable(), ACTIVITIES_CLUB_TABLE, ACTIVITIES_TABLE }; 
			String TABLES = tables[0];
			for ( int i = 1; i < tables.length; i++ )
			{
				TABLES += JOIN_SEPARATOR + tables[i] + " on " + JOIN_CONDITIONS[i-1];
			}

			
			final String GROUP_BY = CLUBS_TABLE + ".name" + " , " + DAYS + " , " + SPORTS;
			
			
			final String SELECT = "select " + COLUMNS_EXPORT + " " +
								  "from( " +
					                    "select " + COLUMNS + " " + 
					                    "from " + TABLES + " " + 
					                    "group by " + GROUP_BY + " " +
					              ") as " + ENHANCED_TABLE + " ";
			return SELECT;
		}
		
		private String createQuery()
		{
			final String SEPARATOR = " and ";
			final String DEFAULT = "1";
			String query = "";
			final String[] items = new String[]{createSports( ),createPrice(),createDays()};
			for ( String item : items )
			{
				query += SEPARATOR;
				query += item == null ? DEFAULT : item;
			}
			query = query.substring(SEPARATOR.length());
			final String GROUP_BY = "group by name";
			return initQuery() + " where " + query + " " + GROUP_BY;
		}
		
		
		private String createSports( )
		{
			if ( !areSportsSet() )
				return null;
			
			final String SEPARATOR = " or ";
			
			String query = "";
			for ( String sport : sports )
			{
				query += SEPARATOR + ENHANCED_TABLE + "." + SPORTS + " = '" + sport.toLowerCase() + "'";
			}
			query = query.substring( SEPARATOR.length() );
			return " ( " + query + " ) ";
		}
		
		private String createPrice( )
		{
			if( !isPriceSet( ) )
			{
				return null;
			}
			final String query = ENHANCED_TABLE + "." + PRICE + " <= " + maxPrice + " or " + ENHANCED_TABLE + "." + PRICE + " is null";
			return " ( " + query + " ) ";
		}
		
		private String createDays( )
		{
			if( !areDaysSet() )
			{
				return null;
			}
			final String SEPARATOR = " or ";
			String query = "";
			for (int i = 0; i < DAY_FLAGS.size(); i++) {
				int day = DAY_FLAGS.keyAt(i);
				int flag = DAY_FLAGS.get(day);
				if ( (flag & days) != 0 )
				{
					query += SEPARATOR + ENHANCED_TABLE + "." + DAYS + " = " + day ;
				}
			}
			query = ENHANCED_TABLE + "." + DAYS + " is null " + query;
			return " ( " + query + " ) ";
		}
		
		public boolean areDaysSet( )
		{
			return days != TimeFilter.Day.getDay ( 8 ) - 1 && days != 0;
		}
		
		public boolean isPriceSet( )
		{
			return maxPrice >= 0f;
		}
		
		public boolean areSportsSet(){
			return sports != null && sports.size() != 0;
		}
		
		@Override
		public void run() {
			String query = this.createQuery();
			Log.d("Query",query);
			Cursor c = executeQuery( query );
			HashMap < String, Integer > cols = getColumns ( c );
			int index_lat = getColumnIndex ( cols, "latitude" );
			
			int index_lon = getColumnIndex ( cols, "longtitude" );
			int index_name = getColumnIndex ( cols, "name" );
			int index_address = getColumnIndex ( cols, "address" );
			int index_postcode = getColumnIndex ( cols, "postcode" );
			int index_comment = getColumnIndex(cols, "comment");
			int index_activities = getColumnIndex(cols, "activities");
			int index_have_information = getColumnIndex(cols, "have_information");
			int index_telephone = getColumnIndex(cols, "phone");
			int index_email = getColumnIndex(cols, "email");
			int index_website = getColumnIndex(cols, "website");
			while ( c.moveToNext ( ) )
			{
				float lat = c.getFloat ( index_lat );
				float lon = c.getFloat ( index_lon );
				GeoPoint point = new GeoPoint ( lat, lon );
				
				Spanned address = Html.fromHtml( c.getString ( index_address ));
				String postcode = c.getString ( index_postcode );
				
				Spanned name = Html.fromHtml( c.getString ( index_name ) );
				Spanned comment = Html.fromHtml( c.getString(index_comment) );
				String activities = c.getString(index_activities);
				String telephone = c.getString(index_telephone);
				Spanned email = Html.fromHtml( c.getString(index_email) );
				Spanned website = Html.fromHtml( c.getString(index_website) );
				
				boolean information = c.getInt(index_have_information)>0;
				
				CustomMarker marker = new CustomMarker(name, address, postcode, telephone, email, website,
						                               comment, activities, information, point, context);
				map.addOverlay ( marker );
			}
			c.close ( );
			map.post(new Runnable() {
				
				@Override
				public void run() {
					map.showOverlays ( );
					map.invalidateMap();
			}});
			
		}
	}
}

