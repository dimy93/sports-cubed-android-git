package com.cube.UI.About;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.cube.AndroidUI.R;
import com.cube.UI.MainActivity;

public class AboutActivity extends Activity {
	
	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		if ((intent.getFlags() & Intent.FLAG_ACTIVITY_NO_ANIMATION) == 0) {
			overridePendingTransition(R.anim.bounce, R.anim.nothing);
		}
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		getApplicationInfo().targetSdkVersion = 19;
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about);
	}

	@Override
	protected void onStop() {
		getApplicationInfo().targetSdkVersion = 10;
		super.onStop();
	}
	
	@Override
	public void onBackPressed(){
		Intent i = new Intent(this, MainActivity.class);
		i.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
		startActivity(i);
	}
	@Override
    protected void onPause() {
        // Whenever this activity is paused (i.e. looses focus because another activity is started etc)
        // Override how this activity is animated out of view
        // The new activity is kept still and this activity is pushed out to the left
       
        super.onPause();
    }
	
}
