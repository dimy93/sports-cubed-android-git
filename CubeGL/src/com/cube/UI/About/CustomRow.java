package com.cube.UI.About;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.AttributeSet;
import android.widget.TextView;

public class CustomRow extends TextView {

	private float ratioW = 0.33f;
	private float ratioH = 0.85f;
	private final static Paint paintTransperant = new Paint();
	private final static Paint paintWhite = new Paint();
	private final static RectF rectTransperant = new RectF();
	private final static RectF rectBorder = new RectF();
	
	{
		paintTransperant.setStyle(Paint.Style.STROKE);
		paintTransperant.setColor(0xffffffff);
		paintTransperant.setXfermode(new PorterDuffXfermode(Mode.CLEAR));
		paintTransperant.setAntiAlias(true);
		paintTransperant.setStrokeWidth(10f);

		paintWhite.setStyle(Paint.Style.STROKE);
		paintWhite.setColor(0xffffffff);
		paintWhite.setXfermode(new PorterDuffXfermode(Mode.SRC_OVER));
		paintWhite.setAntiAlias(true);
		paintWhite.setStrokeWidth(5f);
	}
	
	protected void modifyDrawable(){
		Drawable[] drawables = getCompoundDrawables();
		for ( int i = 0; i < drawables.length; i++ ){
			if ( drawables[i] == null )
				continue;
			drawables[i] = new BitmapDrawable(getResources(),getRoundedCornerBitmap(drawables[i]));
		}
		
		setCompoundDrawablesWithIntrinsicBounds(drawables[0], drawables[1], drawables[2], drawables[3]);
	}
	public static Bitmap getRoundedCornerBitmap(Drawable original) {
		int w = original.getIntrinsicWidth();
		int h = original.getIntrinsicHeight();
		Bitmap output = Bitmap.createBitmap(w, h, Config.ARGB_8888);
	    Canvas canvas = new Canvas(output);
	    original.draw(canvas);
	  
		float border = 10f;
		Paint first = paintTransperant;
		Paint second = paintWhite;
		rectTransperant.set(0.0f, 0.0f, w, h);
		canvas.drawRoundRect(rectTransperant, 0, 0, first);
		rectBorder.set(border / 2, border / 2, w - border / 2, h - border / 2);
		canvas.drawRoundRect(rectBorder, border, border, second);

	    return output;
	  }

	public CustomRow(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		modifyDrawable();
		setText(Html.fromHtml(getText().toString()));
		setMovementMethod(LinkMovementMethod.getInstance());
	}

	public CustomRow(Context context, AttributeSet attrs) {
		super(context, attrs);
		modifyDrawable();
		setText(Html.fromHtml(getText().toString()));
		setMovementMethod(LinkMovementMethod.getInstance());
	}

	public CustomRow(Context context) {
		super(context);
		modifyDrawable();
		setText(Html.fromHtml(getText().toString()));
		setMovementMethod(LinkMovementMethod.getInstance());
	}

	@Override
	public void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		Drawable left = getCompoundDrawables()[0];
		if ( left == null )
			return;
		float drW = w * ratioW;
		float drH = h * ratioH;
		Rect bounds = left.getBounds();
		float ratio = Math.min(drW / bounds.width(), drH / bounds.height());
		Rect new_bounds = new Rect();
		new_bounds.set(bounds.left, bounds.top,(int)(bounds.left + bounds.width()*ratio), (int)(bounds.top + bounds.height()*ratio));
		left.setBounds(new_bounds);
		setCompoundDrawables(left,null,null,null);
	}
}
