package com.cube.UI.About;

import com.cube.AndroidUI.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ScrollView;

public class CustomScrollView extends ScrollView {	
	Drawable foreground;

	public void scrollFix(){
		post(new Runnable() {
			
			@Override
			public void run() {
				fullScroll(ScrollView.FOCUS_UP);
			}
		});
	}
	
	public CustomScrollView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		scrollFix();
		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomScrollView, defStyle, 0);
        try {
        	foreground = a.getDrawable(R.styleable.CustomScrollView_foreground);
        } finally {
            a.recycle();
        }
	}

	public CustomScrollView(Context context, AttributeSet attrs) {
		super(context, attrs);
		scrollFix();
		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomScrollView, 0, 0);
        try {
        	foreground = a.getDrawable(R.styleable.CustomScrollView_foreground);
        } finally {
            a.recycle();
        }
	}

	public CustomScrollView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void dispatchDraw(Canvas canvas) {
	  super.dispatchDraw(canvas);
	  foreground.setBounds(getScrollX(), getScrollY(), getScrollX()+canvas.getWidth(),getScrollY()+canvas.getHeight());
	  foreground.draw(canvas);
	}

}
