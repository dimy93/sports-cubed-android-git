package com.cube.UI;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.RelativeSizeSpan;
import android.widget.TextView;

import com.cube.AndroidUI.R;

public class DisplayPlaceActivity extends Activity {
	
	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		if ((intent.getFlags() & Intent.FLAG_ACTIVITY_NO_ANIMATION) == 0) {
			overridePendingTransition(R.anim.bounce, R.anim.nothing);
		}
	}
	
	@Override
	public void onBackPressed(){
		Intent i = new Intent(this, MainActivity.class);
		i.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
		startActivity(i);
		finish();
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		getApplicationInfo().targetSdkVersion = 19;
		super.onCreate(savedInstanceState); 
		setContentView(R.layout.activity_display_place);
		Intent intent = getIntent();
		
		// Set title
		/*
		 * String message = intent.getStringExtra("name");
		 * this.setTitle(message);
		 */
		
		if (!checkIntent(createDummyEmailIntent())) {
			TextView view = (TextView) this.findViewById(R.id.place_email);
			view.setAutoLinkMask(0);
			
		}

		// Set Title
		CharSequence title = intent.getCharSequenceExtra("name");
		if (title != null) {
			TextView view = (TextView) this.findViewById(R.id.place_name);
			view.setText(title);
		}

		// Set Description
		CharSequence description = intent.getCharSequenceExtra("description");
		if (description != null) {
			TextView view = (TextView) this
					.findViewById(R.id.place_description);
			SpannableStringBuilder builder = new SpannableStringBuilder(view.getText());
			int end = builder.length();
			builder.append(description);
			builder.append("\n");
			builder.setSpan(new RelativeSizeSpan(0.70f), end, builder.length(), 0);
			view.setText(builder);
		}

		// Set Email
		CharSequence email = intent.getCharSequenceExtra("email");
		if (email != null) {
			TextView view = (TextView) this.findViewById(R.id.place_email);
			if ( !checkIntent(createDummyEmailIntent()) ) {
				view.setAutoLinkMask(0);
			}
			SpannableStringBuilder builder = new SpannableStringBuilder(view.getText());
			int end = builder.length();
			builder.append(email);
			builder.append("\n");
			builder.setSpan(new RelativeSizeSpan(0.70f), end, builder.length(), 0);
			view.setText(builder);
		}

		CharSequence website = intent.getCharSequenceExtra("website");
		if (website != null) {
			TextView view = (TextView) this.findViewById(R.id.place_website);
			if ( !checkIntent(createDummyWebIntent()) ) {
				view.setAutoLinkMask(0);
			}
			SpannableStringBuilder builder = new SpannableStringBuilder(view.getText());
			int end = builder.length();
			builder.append(website);
			builder.append("\n");
			builder.setSpan(new RelativeSizeSpan(0.70f), end, builder.length(), 0);
			view.setText(builder);
		}

		// Set Phone
		String phone = intent.getStringExtra("phone");
		if (phone != null) {
			TextView view = (TextView) this.findViewById(R.id.place_telephone);
			if ( !checkIntent(createDummyPhoneIntent()) ) {
				view.setAutoLinkMask(0);
			}
			SpannableStringBuilder builder = new SpannableStringBuilder(view.getText());
			int end = builder.length();
			builder.append(phone);
			builder.append("\n");
			builder.setSpan(new RelativeSizeSpan(0.70f), end, builder.length(), 0);
			view.setText(builder);
		}

		// Set Address
		CharSequence address = intent.getCharSequenceExtra("address");
		if (address != null) {
			TextView view = (TextView) this.findViewById(R.id.place_address);
			SpannableStringBuilder builder = new SpannableStringBuilder(view.getText());
			int end = builder.length();
			builder.append(address);
			builder.clearSpans();
			builder.append("\n");
			builder.setSpan(new RelativeSizeSpan(0.70f), end, builder.length(), 0);
			view.setText(builder);
		}

		// Set Postcode
		String postcode = intent.getStringExtra("postcode");
		if (postcode != null) {
			TextView view = (TextView) this.findViewById(R.id.place_postcode);
			SpannableStringBuilder builder = new SpannableStringBuilder(view.getText());
			int end = builder.length();
			builder.append(postcode);
			builder.append("\n");
			builder.setSpan(new RelativeSizeSpan(0.70f), end, builder.length(), 0);
			view.setText(builder);
		}

		// Set Activities
		String activities = intent.getStringExtra("activities");
		if (activities != null) {
			TextView view = (TextView) this.findViewById(R.id.place_activities);
			SpannableStringBuilder builder = new SpannableStringBuilder(view.getText());
			int end = builder.length();
			builder.append(activities);
			builder.append("\n");
			builder.setSpan(new RelativeSizeSpan(0.70f), end, builder.length(), 0);
			view.setText(builder);
		}
	}
	
	@Override
	protected void onStop(){
		getApplicationInfo().targetSdkVersion = 10;
		super.onStop();
	}

	protected boolean checkIntent(Intent intent)
	{
		PackageManager packageManager = getPackageManager();
		List<ResolveInfo> apps = packageManager.queryIntentActivities(intent, 0);
		return apps.size() > 0 && !( apps.get(0).activityInfo.name.equals(getClass().getName()) && apps.size() == 1) ;
	}
	
	
	protected Intent createDummyEmailIntent()
	{
		final Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
				"mailto", "abc@gmail.com", null));
		emailIntent.putExtra(Intent.EXTRA_SUBJECT, "EXTRA_SUBJECT");
		return emailIntent;
	}
	
	protected Intent createDummyWebIntent()
	{
		final Intent webIntent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("http://www.google.co.uk"));
		return webIntent;
	}
	
	protected Intent createDummyPhoneIntent(){
		String uri = "tel:" + "0131 666 7777".trim() ;
		final Intent phoneIntent = new Intent(Intent.ACTION_DIAL);
		phoneIntent.setData(Uri.parse(uri));
		return phoneIntent;
	}
}
