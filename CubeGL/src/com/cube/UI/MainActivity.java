package com.cube.UI;

import java.lang.reflect.Method;

import org.osmdroid.tileprovider.IRegisterReceiver;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.cube.AndroidUI.R;
import com.cube.SQL.SQLQuery;
import com.cube.UI.About.AboutActivity;
import com.cube.UI.customWidgets.CustomProgressbar;
import com.cube.UI.updateFromServer.ServerConnector;

public class MainActivity extends Activity implements IRegisterReceiver {

	private CustomProgressbar pb;

	private int realTargetSdkVersion;

	@Override
	protected void onNewIntent(Intent intent) {
		Log.d("CubeGL", "NEW INTENT");
		super.onNewIntent(intent);
		if ((intent.getFlags() & Intent.FLAG_ACTIVITY_NO_ANIMATION) == 0) {
			overridePendingTransition(R.anim.bounce, R.anim.nothing);
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		realTargetSdkVersion = getApplicationInfo().targetSdkVersion;
		getApplicationInfo().targetSdkVersion = 10;

		super.onCreate(savedInstanceState);
		if (savedInstanceState != null) {
			Log.d("CubeGL", "onCreateFromInstanse");
			setContentView(R.layout.main);
			return;
		}
		Log.d("CubeGL", "onRegularCreate");
		setContentView(R.layout.activity_main);
		pb = (CustomProgressbar) findViewById(R.id.progress);
		pb.setVisibility(View.INVISIBLE);

		final String db_url = getResources().getString(R.string.db_server_path);
		final String map_url = getResources().getString(
				R.string.map_server_path);
		ServerConnector sc = new ServerConnector(this, pb,
				new InitializeCube(), new OnDownloadFailed());
		sc.execute(db_url, "database.sqlite", map_url, "Edinburgh.mbtiles");

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return (super.onCreateOptionsMenu(menu));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		case R.id.about:
			Intent intent = new Intent(this, AboutActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			startActivity(intent);
			overridePendingTransition(R.anim.bounce, R.anim.nothing);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected void onPause() {
		Log.d("CubeGL", "onPause");
		super.onPause();
		View root = this.findViewById(android.R.id.content).getRootView();
		dispatchEvent(root, "onPause");
	}

	private void dispatchEvent(View view, String event) {
		try {
			Method m = view.getClass().getMethod(event);
			m.invoke(view);
		} catch (Exception e) {
			// DO NOTHING
		}
		if (!(view instanceof ViewGroup))
			return;
		ViewGroup parent = (ViewGroup) view;
		for (int i = 0; i < parent.getChildCount(); i++) {
			View child = parent.getChildAt(i);
			dispatchEvent(child, event);
		}
	}

	@Override
	protected void onResume() {
		Log.d("CubeGL", "onResume");
		super.onResume();
		View root = this.findViewById(android.R.id.content).getRootView();
		dispatchEvent(root, "onResume");
	}

	@Override
	public void onSaveInstanceState(Bundle instance) {
		super.onSaveInstanceState(instance);
		Log.d("CubeGL", "onSaveInstanceState:" + instance);
		Log.d("CubeGL", findViewById(R.id.main) + "");
	}

	@Override
	protected void onStop() {
		Log.d("CubeGL", "onStop");
		getApplicationInfo().targetSdkVersion = realTargetSdkVersion;
		super.onStop();
	}

	protected boolean isFullscreenOpaque() {
		return true;
	}

	private class InitializeCube implements Runnable {

		@Override
		public void run() {
			SQLQuery.setContext(MainActivity.this);
			View wrap = findViewById(R.id.wrap);
			setContentView(R.layout.main);
			wrap.setVisibility(View.GONE);

			// LayoutInflater inflater = (LayoutInflater) getSystemService (
			// LAYOUT_INFLATER_SERVICE );
			// TutorialManager tm = (TutorialManager) inflater.inflate (
			// R.layout.tutorial_manager, (ViewGroup) findViewById ( R.id.wrap
			// ));
			// tm.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void onBackPressed() {
		TouchEventListener v = (TouchEventListener) this
				.findViewById(R.id.main);
		if (v == null || !v.onBackPressed()) {
			moveTaskToBack(true);
		}
	}

	private class OnDownloadFailed implements Runnable {

		@Override
		public void run() {
			AlertDialog.Builder builder = new AlertDialog.Builder(
					MainActivity.this);
			builder.setTitle("Exception");
			builder.setMessage("The application failed to download required files. The application will now terminate.");
			builder.setCancelable(true);

			builder.setNeutralButton("OK",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							MainActivity.this.finish();
						}
					});

			builder.setOnCancelListener(new DialogInterface.OnCancelListener() {

				@Override
				public void onCancel(DialogInterface dialog) {
					MainActivity.this.finish();
				}
			});

			AlertDialog alert = builder.create();
			alert.show();
		}

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.d("CubeGL", "onDestroy");
	}
}
