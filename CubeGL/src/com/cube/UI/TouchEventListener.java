package com.cube.UI;

import android.content.Context;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.OnHierarchyChangeListener;
import android.widget.RelativeLayout;
import com.cube.GLCube.UIConnect.CubeSide;
import com.cube.GLCube.UIConnect.CubeSide.CubeSideListener;
import com.cube.UI.cubeSides.Map;
import com.cube.UI.gestureDetection.IBCScaleGestureDetector;
import com.cube.UI.gestureDetection.RotationGestureDetector;
import com.cube.UI.gestureDetection.ScaleGestureDetector;
import com.cube.UI.gestureDetection.SimpleOnScaleGestureListener;

public class TouchEventListener extends RelativeLayout implements
		CubeSideListener, OnHierarchyChangeListener {
	public static float velocityX, velocityY;
	public static float displacementX, displacementY;
	public static float scaleSpan;
	public static float rotationZ;

	private static float mLastTouchX, mLastTouchY;
	private OnTouchListener listener;
	private static float density;
	private final static int INVALID_POINTER_ID = -1;

	// For simulating a touch event after the cube has begun
	// to zoom out so that to fix the bug when you are traped
	// on a cube side when Garbage Colection is invoked
	private static boolean lock = false;
	public static CubeSide onSide = null;
	private static boolean inZoom = true;
	private static boolean scaleEnabled = false;

	private static int mActivePointerId = INVALID_POINTER_ID;

	// For simulating a touch event after the cube has begun
	// to zoom out so that to fix the bug when you are traped
	// on a cube side when Garbage Colection is invoked
	public static void lock() {
		lock = true;
	}

	// For simulating a touch event after the cube has begun
	// to zoom out so that to fix the bug when you are traped
	// on a cube side when Garbage Colection is invoked
	public static boolean isLocked() {
		return lock;
	}

	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		final int action = ev.getAction();
		switch (action & MotionEvent.ACTION_MASK) {
		case MotionEvent.ACTION_DOWN: {
			lock = true;
			final float x = ev.getX();
			final float y = ev.getY();

			mLastTouchX = x;
			mLastTouchY = y;

			// Save the ID of this pointer
			mActivePointerId = ev.getPointerId(0);
			break;
		}

		case MotionEvent.ACTION_MOVE: {
			// Find the index of the active pointer and fetch its position
			final int pointerIndex = ev.findPointerIndex(mActivePointerId);
			if (pointerIndex == INVALID_POINTER_ID
					|| pointerIndex >= ev.getPointerCount())
				break;
			final float x = ev.getX(pointerIndex);
			final float y = ev.getY(pointerIndex);

			final float dx = x - mLastTouchX;
			final float dy = y - mLastTouchY;

			displacementX += dx / -100f;
			displacementY += dy / -100f;

			mLastTouchX = x;
			mLastTouchY = y;

			break;
		}

		case MotionEvent.ACTION_UP: {
			lock = false;
			mActivePointerId = INVALID_POINTER_ID;
			break;
		}

		case MotionEvent.ACTION_CANCEL: {
			lock = false;
			mActivePointerId = INVALID_POINTER_ID;
			break;
		}

		case MotionEvent.ACTION_POINTER_UP: {
			// Extract the index of the pointer that left the touch sensor
			final int pointerIndex = (action & MotionEvent.ACTION_POINTER_INDEX_MASK) >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
			final int pointerId = ev.getPointerId(pointerIndex);
			if (pointerId == mActivePointerId) {
				// This was our active pointer going up. Choose a new
				// active pointer and adjust accordingly.
				final int newPointerIndex = pointerIndex == 0 ? 1 : 0;
				mLastTouchX = ev.getX(newPointerIndex);
				mLastTouchY = ev.getY(newPointerIndex);
				mActivePointerId = ev.getPointerId(newPointerIndex);
			}
			break;
		}
		}

		return true;
	}

	public TouchEventListener(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setupListener(this);
	}

	public TouchEventListener(Context context, AttributeSet attrs) {
		super(context, attrs);
		setupListener(this);
	}

	public TouchEventListener(Context context) {
		super(context);
		setupListener(this);
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		boolean scaleEvent = ev.getPointerCount() > 1;
		onTouchEvent(ev);
		if ( inZoom  || (scaleEvent && scaleEnabled))
			return true;
		/*if (scaleEvent && !scaleEnabled) {
			listener.onTouch(this, ev);
			return false;
		}*/
		return false;
	}

	public static void resetTouchInfo() {
		velocityX = velocityY = displacementX = displacementY = rotationZ = 0;
		scaleSpan = -1f;
	}

	private void setupListener(final View v) {
		this.setOnHierarchyChangeListener(this);
		GestureDetector.SimpleOnGestureListener gestureListener = new GestureDetector.SimpleOnGestureListener() {
			@Override
			public boolean onFling(MotionEvent e1, MotionEvent e2,
					float velocityX, float velocityY) {
				TouchEventListener.velocityX = velocityX;
				TouchEventListener.velocityY = velocityY;
				return true;
			}
		};

		SimpleOnScaleGestureListener pinchListener = new SimpleOnScaleGestureListener() {
			public float span;

			@Override
			public boolean onScaleBegin(IBCScaleGestureDetector detector) {
				span = detector.getCurrentSpan();
				span /= density;
				if (span < 5f) {
					span = 0f;
					return false;
				}
				return true;
			}

			@Override
			public void onScaleEnd(IBCScaleGestureDetector detector) {
				scaleSpan = detector.getCurrentSpan() / span;
			}

		};

		RotationGestureDetector.RotationListener rotateListener = new RotationGestureDetector.RotationListener() {

			@Override
			public void onRotate(float deltaAngle) {
				if (Math.abs(deltaAngle) > 5f) {
					// Log.d("CubeGL","PutkiMaini delta:"+deltaAngle +
					// "rotationZ:"+ rotationZ);
					deltaAngle = 0f;
				}
				rotationZ = -deltaAngle;
			}
		};
		final RotationGestureDetector rotateDetector = new RotationGestureDetector(
				rotateListener);

		final GestureDetector gestureDetector = new GestureDetector(
				v.getContext(), gestureListener);

		final ScaleGestureDetector scaleDetector = new ScaleGestureDetector(
				v.getContext(), pinchListener);

		listener = new View.OnTouchListener() {
			@Override
			public boolean onTouch(View view, MotionEvent motionEvent) {
				gestureDetector.onTouchEvent(motionEvent);
				TouchEventListener.this.onTouchEvent(motionEvent);
				rotateDetector.onTouch(motionEvent);
				if ( inZoom || !scaleEnabled )
					return true;
				scaleDetector.onTouchEvent(motionEvent);
				return true;
			}
		};
		v.setOnTouchListener(listener);
		DisplayMetrics metrics = getResources().getDisplayMetrics();
		density = metrics.density;
	}

	public boolean onBackPressed() {
		if (onSide == null)
			return false;
        onSide.zoomOut(100);
		return true;
	}

	@Override
	public void onZoomInStart(CubeSide caller) {
	}

	@Override
	public void onZoomOutStart(CubeSide caller) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onZoomInEnd(CubeSide caller) {
		inZoom = false;
		onSide = caller;
		if ( caller instanceof Map )
		{
			scaleEnabled = false;
		}
		else
		{
			scaleEnabled = true;
		}
	}

	@Override
	public void onZoomOutEnd(CubeSide caller) {
		inZoom = false;
	}

	@Override
	public void onZoomOutStarted(CubeSide caller) {
		inZoom = true;
		onSide = null;
		lock = true;
		scaleEnabled = false;
	}

	@Override
	public void onZoomInStarted(CubeSide caller) {
		inZoom = true;
	}

	@Override
	public void onChildViewAdded(View parent, View child) {
		if (child instanceof CubeSide)
			((CubeSide) child).addListener(this);
	}

	@Override
	public void onChildViewRemoved(View parent, View child) {
		// TODO Auto-generated method stub

	}
}
