package com.cube.UI.cubeSides;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.cube.AndroidUI.R;
import com.cube.GLCube.UIConnect.CubeSide;

public class Ad extends CubeSide {
	
	public Ad(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initObj( );
	}

	public Ad(Context context, AttributeSet attrs) {
		super(context, attrs);
		initObj( );
	}
	
	public Ad(Context context) {
		super(context);
		initObj( );
	}
	
	private void initObj( )
	{
		setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//Fixes a bug on Android 2.3 where it's clickable even when Ad is not visible
				if ( Ad.this.getVisibility() != View.VISIBLE )
					return;
				Context c = v.getContext();
				String url = c.getResources().getString(R.string.ad_url);
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
				c.startActivity(browserIntent);
			}
		});
	}
	
	
	@Override
	public void onMeasure( int widthMeasureSpec, int heightMeasureSpec ){
		super.onMeasure( widthMeasureSpec, heightMeasureSpec );
		int widthSize = MeasureSpec.getSize(widthMeasureSpec);
		TextView tv = ((TextView) findViewById(R.id.bottom_image));
		Drawable TextViewDrawable = getContext().getResources().getDrawable(R.drawable.edinburgh_council_logo);
		int drawableH = TextViewDrawable.getIntrinsicHeight();
		int drawableW = TextViewDrawable.getIntrinsicWidth();
		float scaleFactor = widthSize/(float)drawableW;
		drawableH = (int) (drawableH*scaleFactor);
		drawableW = (int) (drawableW*scaleFactor);
        TextViewDrawable.setBounds(0, 0, drawableW,drawableH);
        tv.setCompoundDrawables(null, TextViewDrawable, null, null);
	}
}
