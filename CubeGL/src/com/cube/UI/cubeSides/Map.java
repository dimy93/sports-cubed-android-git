package com.cube.UI.cubeSides;

import java.io.File;
import java.util.ArrayList;

import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.bonuspack.overlays.ExtendedOverlayItem;
import org.osmdroid.events.MapListener;
import org.osmdroid.events.ScrollEvent;
import org.osmdroid.events.ZoomEvent;
import org.osmdroid.util.BoundingBoxE6;
import org.osmdroid.views.MapView;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.cube.AndroidUI.R;
import com.cube.GLCube.UIConnect.CubeSide;
import com.cube.GLCube.UIConnect.Run;
import com.cube.SQL.SQLQuery;
import com.cube.UI.MainActivity;
import com.cube.UI.TouchEventListener;
import com.cube.osmdroid.markers.CustomInfoWindow;
import com.cube.osmdroid.overlays.CustomItemizedOverlay;
import com.cube.osmdroid.overlays.CustomMinimapOverlay;
import com.cube.osmdroid.tileprovider.MBTileProvider;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.Animator.AnimatorListener;
import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.animation.ObjectAnimator;
import com.nineoldandroids.animation.TypeEvaluator;
import com.nineoldandroids.animation.ValueAnimator;

public class Map extends CubeSide {
	// private ImageView bypassAnim;
	private MapView map;
	private ImageButton zoomButton;
	boolean zoomed = false;
	private CustomItemizedOverlay<ExtendedOverlayItem> markers;
	private CustomMinimapOverlay minimap;
	private boolean drawingForSide = false;

	private final Paint paintTransperant = new Paint();
	private final Paint paintWhite = new Paint();
	private final RectF rectTransperant = new RectF();
	private final RectF rectBorder = new RectF();
	private View mZoomInControl;
	private View mZoomOutControl;
	private final int mapBackGround = 0xff666666;
	
	private Bitmap screenshot = null;

	public Map(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initMap(context);
		initDrawing();
		SQLQuery.setMap(this);
	}

	public Map(Context context, AttributeSet attrs) {
		super(context, attrs);
		initMap(context);
		initDrawing();
		SQLQuery.setMap(this);
	}

	public Map(Context context) {
		super(context);
		initDrawing();
		initMap(context);
		SQLQuery.setMap(this);
	}

	@Override
	public void onChildViewAdded(View child, int id) {
		initZoomButton(id);
		initZoomControls(id);
	}

	@Override
	public void zoomOut(int waitMilis) {
		int fullHeight = getGLRenderHeight();
		int fullWidth = getGLRenderWidth();

		popOut(fullWidth, fullHeight);
	}

	@Override
	protected void setSize() {
		if (TouchEventListener.onSide != this) {
			Map.super.setSize();
			return;
		}
	}

	@Override
	protected void onInitialSizeSet(int w, int h) {
		super.onInitialSizeSet(w, h);
		onPopInEnd();
	}

	@Override
	public boolean onZoomInEnd() {
		super.onZoomInEnd();
		popIn();
		return false;
	}

	@Override
	public boolean onZoomInStart() {
		return true;
	}

	@Override
	public boolean onZoomInStarted() {
		super.onZoomInStarted();
		zoomed = true;
		return true;
	}

	@Override
	public boolean onZoomOutStart() {
		super.onZoomOutStart();
		return false;
	}

	@Override
	public boolean onZoomOutStarted() {
		super.onZoomOutStarted();
		return false;
	}

	@Override
	public boolean onZoomOutEnd() {
		super.onZoomOutEnd();
		zoomed = false;
		return true;
	}

	@Override
	public void onCubeRotateWhileZoom(float scaleSpan) {
	}

	private void measureMinimap() {
		WindowManager wm = (WindowManager) getContext().getSystemService(
				Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		int h = display.getHeight() / 3;
		int w = display.getWidth() / 3;
		boolean changed = false;
		if (h != minimap.getHeight()) {
			minimap.setHeight(h);
			changed = true;
		}
		if (w != minimap.getWidth()) {
			minimap.setWidth(display.getWidth() / 3);
			changed = true;
		}
		if (changed) {
			map.invalidate();
		}
	}

	@Override
	public void onRestoreInstanceState(android.os.Parcelable state) {
		super.onRestoreInstanceState(state);
		zoomed = forceZoomIn;
	};

	@Override
	@SuppressWarnings("deprecation")
	public void onLayoutChanged() {
		super.onLayoutChanged();
		if (zoomed) {
			RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) getLayoutParams();
			lp.width = RelativeLayout.LayoutParams.FILL_PARENT;
			lp.height = RelativeLayout.LayoutParams.FILL_PARENT;
			requestLayout();
		}
		measureMinimap();
	}

	public void addOverlay(ExtendedOverlayItem item) {
		markers.addItem(item);
	}

	public void clearOverlays() {
		markers.removeAllItems();
	}

	public void showOverlays() {
		markers.setEnabled(true);
	}

	public void invalidateMap() {
		if (map != null)
			map.invalidate();
	}

	private void popIn() {
		int requiredHeight = getGLRenderHeight();
		int requiredWidth = getGLRenderWidth();
		int currentHeight = getMeasuredHeight();
		int currentWidth = getMeasuredWidth();

		Rect st = new Rect(0, 0, currentWidth, currentHeight);
		Rect end = new Rect(0, 0, requiredWidth, requiredHeight);

		Animator animator = ValueAnimator.ofObject(
				new ChangeDimentionsEvaluator(this), st, end).setDuration(800);
		animator.addListener(new PopInListener());
		animator.setStartDelay(100);
		animator.start();
	}

	private void popOut(int currentWidth, int currentHeight) {
		dispatchOnZoomOutStart(true);
		int requiredHeight = Math.min(currentHeight, currentWidth);
		int requiredWidth = Math.min(currentHeight, currentWidth);

		Rect st = new Rect(0, 0, currentWidth, currentHeight);
		Rect end = new Rect(0, 0, requiredWidth, requiredHeight);

		Animator animator = ValueAnimator.ofObject(
				new ChangeDimentionsEvaluator(this), st, end).setDuration(800);
		animator.addListener(new PopOutListener());
		animator.start();
	}

	@SuppressWarnings("deprecation")
	private void initMap(Context c) {
		// Use dpi to calculate MbTilesProvider's tiles size
		double dpi = getContext().getResources().getDisplayMetrics().density;
		int tileSize = (int) dpi * 256;

		// Create the mapView with an MBTileProvider
		DefaultResourceProxyImpl resProxy = new DefaultResourceProxyImpl(
				c.getApplicationContext());

		File path = this.getContext().getFilesDir();
		File file = new File(path, "Edinburgh.mbtiles");

		MBTileProvider provider = new MBTileProvider(
				(MainActivity) getContext(), file, tileSize);
		MBTileProvider provider_mini = new MBTileProvider(
				(MainActivity) getContext(), file, tileSize);
		map = new MapView(c, provider.getTileSource().getTileSizePixels(),
				resProxy, provider);

		map.setBackgroundColor(mapBackGround);
		
		map.setMapListener( new MapListener() {
			
			@Override
			public boolean onZoom(ZoomEvent event) {
				return Map.this.zoomControlsInit(event);
			}
			
			@Override
			public boolean onScroll(ScrollEvent event) {
				return false;
			}
		});

		// Enable touch controls
		map.setClickable(true);
		map.setMultiTouchControls(true);
		map.setUseDataConnection(false);
		map.setBuiltInZoomControls(false);
		map.setUseSafeCanvas(true);

		// Adjust zooming
		map.setMinZoomLevel(13);
		map.getController().setZoom(13);

		BoundingBoxE6 bb = new BoundingBoxE6(55.98, -3.091724, 55.889560, -3.30);
		map.getController().setCenter(bb.getCenter());
		map.setScrollableAreaLimit(bb);

		// Adding Markers Overlay
		markers = new CustomItemizedOverlay<ExtendedOverlayItem>(
				this.getContext(), new ArrayList<ExtendedOverlayItem>(), map,
				new CustomInfoWindow(map));
		map.getOverlays().add(markers);

		// Add MinimapOverlay
		minimap = new CustomMinimapOverlay(this.getContext(),
				map.getTileRequestCompleteHandler(), provider_mini, markers, 4);
		minimap.setMaxZoomLevel(12);
		minimap.setPadding(15);
		measureMinimap();

		addView(map);
		map.getOverlayManager().add(minimap);

		RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) map
				.getLayoutParams();
		lp.width = lp.height = RelativeLayout.LayoutParams.FILL_PARENT;
		map.requestLayout();

		/*
		 * // Turn off hardware acceleration here, or in manifest if
		 * (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) { Class < ?
		 * > clazz; try { clazz = Class.forName ( "android.view.View" ); Method
		 * m = clazz.getDeclaredMethod ( "setLayerType", int.class, Paint.class
		 * ); m.setAccessible ( true ); m.invoke ( this, 2, null ); } catch (
		 * ClassNotFoundException e ) { // TODO Auto-generated catch block
		 * e.printStackTrace(); } catch ( NoSuchMethodException e ) { // TODO
		 * Auto-generated catch block e.printStackTrace(); } catch (
		 * IllegalArgumentException e ) { // TODO Auto-generated catch block
		 * e.printStackTrace(); } catch ( IllegalAccessException e ) { // TODO
		 * Auto-generated catch block e.printStackTrace(); } catch (
		 * InvocationTargetException e ) { // TODO Auto-generated catch block
		 * e.printStackTrace(); } }
		 */

	}

	private void initZoomButton(int current_id) {
		int id = R.id.zoom_button;
		if (id != current_id)
			return;
		zoomButton = (ImageButton) findViewById(id);
		zoomButton.setOnClickListener(new zoomClicked());
	}

	private void initZoomControls(int current_id) {
		if (R.id.zoom_in_button == current_id) {
			mZoomInControl = findViewById(current_id);
			mZoomInControl.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					map.getController().zoomIn();
				}
			});
			zoomControlsInit(null);
		}
		if (R.id.zoom_out_button == current_id) {
			mZoomOutControl = findViewById(current_id);
			mZoomOutControl.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					map.getController().zoomOut();
				}
			});
			zoomControlsInit(null);
		}
	}

	private final class zoomClicked implements OnClickListener {
		@Override
		public void onClick(View v) {
			zoomOut(0);
		}
	}

	private void onPopInEnd() {
		Map.this.postDelayed(new Runnable() {

			@Override
			public void run() {
				Map.this.dispatchOnZoomInEnd(true);
				map.setBuiltInZoomControls(false);
				SQLQuery.setPlaces();
				Map.this.requestLayout();
			}
		}, 10);
	}

	private final class PopOutListener implements AnimatorListener {
		@Override
		public void onAnimationCancel(Animator arg0) {
			// TODO Auto-generated method stub
			onAnimationEnd(arg0);
		}

		@Override
		public void onAnimationEnd(Animator arg0) {
			map.setBuiltInZoomControls(false);
			Map.super.zoomOut(100); // adds a little time between the popout and
									// the zoomout
		}

		@Override
		public void onAnimationRepeat(Animator arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onAnimationStart(Animator arg0) {
			Map.this.dispatchOnZoomOutStarted(arg0, true);
		}
	}

	private final class PopInListener implements AnimatorListener {

		@Override
		public void onAnimationEnd(Animator arg0) {
			Map.this.onPopInEnd();
		}

		@Override
		public void onAnimationRepeat(Animator arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onAnimationStart(Animator arg0) {
			if (SQLQuery.sidesChanged()) {
				Map.this.clearOverlays();
				markers.setEnabled(false);
			}
		}

		@Override
		public void onAnimationCancel(Animator arg0) {
			// TODO Auto-generated method stub

		}
	}

	private final class ChangeDimentionsEvaluator implements
			TypeEvaluator<Rect> {

		private View v;

		public ChangeDimentionsEvaluator(View v) {
			this.v = v;
		}

		@Override
		public Rect evaluate(float fraction, Rect startValue, Rect endValue) {
			int ws = startValue.width();
			int we = endValue.width();
			int hs = startValue.height();
			int he = endValue.height();
			int resWidth = (int) (ws + fraction * (we - ws));
			int resHeigh = (int) (hs + fraction * (he - hs));
			ViewGroup.LayoutParams params = v.getLayoutParams();
			params.height = resHeigh;
			params.width = resWidth;
			v.requestLayout();
			return null;
		}
	}
	
    boolean zoomControlsInit(ZoomEvent event) {
		Log.d("CubeGL","Event");
		int zoom = event == null ? map.getZoomLevel() : event.getZoomLevel();
		
		//Disable Zoom In
		if (zoom >= map.getMaxZoomLevel()  && mZoomInControl != null) {
			mZoomInControl.setEnabled(false);
			AnimatorSet set = new AnimatorSet();
			set.playTogether(
					ObjectAnimator.ofFloat(mZoomInControl,"scaleX", 1f, 0.75f), 
					ObjectAnimator.ofFloat(mZoomInControl, "scaleY", 1f, 0.75f)
			);
			set.setDuration(200).start();
		}
		
		//Disable Zoom Out
		if (zoom <= map.getMinZoomLevel() && mZoomOutControl != null ) {
			mZoomOutControl.setEnabled(false);
			AnimatorSet set = new AnimatorSet();
			set.playTogether(
					ObjectAnimator.ofFloat(mZoomOutControl,"scaleX", 1f, 0.75f), 
					ObjectAnimator.ofFloat(mZoomOutControl, "scaleY", 1f, 0.75f)
			);
			set.setDuration(200).start();
		}
		
		//Enable Zoom In
		if ( zoom < map.getMaxZoomLevel() && mZoomInControl != null && !mZoomInControl.isEnabled() ){
			mZoomInControl.setEnabled(true);
			AnimatorSet set = new AnimatorSet();
			set.playTogether(ObjectAnimator.ofFloat(mZoomInControl,
					"scaleX", 0.75f, 1f), ObjectAnimator.ofFloat(
					mZoomInControl, "scaleY", 0.75f, 1f));
			set.setDuration(200).start();
		}
		
		//Enable Zoom Out
		if ( zoom > map.getMinZoomLevel() && mZoomOutControl != null && !mZoomOutControl.isEnabled() ){
			mZoomOutControl.setEnabled(true);
			AnimatorSet set = new AnimatorSet();
			set.playTogether(
					ObjectAnimator.ofFloat(mZoomOutControl,"scaleX", 0.75f, 1f),
					ObjectAnimator.ofFloat(mZoomOutControl, "scaleY", 0.75f, 1f)
			);
			set.setDuration(200).start();
		}
		return false;
	}

	private void initDrawing() {
		paintTransperant.setStyle(Paint.Style.STROKE);
		paintTransperant.setColor(0xffffffff);
		paintTransperant.setXfermode(new PorterDuffXfermode(Mode.CLEAR));
		paintTransperant.setAntiAlias(true);
		paintTransperant.setStrokeWidth(10f);

		paintWhite.setStyle(Paint.Style.STROKE);
		paintWhite.setColor(0xffffffff);
		paintWhite.setXfermode(new PorterDuffXfermode(Mode.SRC_OVER));
		paintWhite.setAntiAlias(true);
		paintWhite.setStrokeWidth(5f);
	}

	@Override
	protected void dispatchDraw(Canvas c) {
		super.dispatchDraw(c);
		int w = getWidth();
		int h = getHeight();
		float border = 5f;
		Paint first = null;
		Paint second = paintWhite;
		if (drawingForSide)
			first = paintWhite;
		else
			first = paintTransperant;
		rectTransperant.set(0.0f, 0.0f, w, h);
		c.drawRoundRect(rectTransperant, 0, 0, first);
		rectBorder.set(border / 2, border / 2, w - border / 2, h - border / 2);
		c.drawRoundRect(rectBorder, border, border, second);
	}
	
	@Override
	public Bitmap getScreenViewBitmap() {
		getScreenViewBitmapReal();
		return screenshot;
	}
	
	/*
	 * This is the usual getScreenViewBitmap but modified to hide buttons and
	 * display Expole Map text as well as to return whether the maptiles was
	 * loaded during the screen
	 */
	public boolean getScreenViewBitmapReal() {
		drawingForSide = true;
		View v = this.findViewById(R.id.show_on_picture_taken);
		if (v != null)
			v.setVisibility(View.VISIBLE);
		zoomButton.setVisibility(View.INVISIBLE);
		minimap.setEnabled(false);
		mZoomInControl.setVisibility(View.INVISIBLE);
		mZoomOutControl.setVisibility(View.INVISIBLE);
		Bitmap result = super.getScreenViewBitmap();
		if (v != null)
			v.setVisibility(View.INVISIBLE);
		mZoomInControl.setVisibility(View.VISIBLE);
		mZoomOutControl.setVisibility(View.VISIBLE);
		minimap.setEnabled(true);
		zoomButton.setVisibility(View.VISIBLE);
		drawingForSide = false;
		if(  isMapLoaded( result ) ){
			Log.d("CubeGL","Loaded");
		}
		else
		{
			Log.d("CubeGL","NotLoaded");
		}
		screenshot = result;
		return isMapLoaded( result );
	}
	 
	public boolean isMapLoaded( Bitmap screen ){
		float density = getResources().getDisplayMetrics().density;
		int[] testingPointX = {(int)density*23,(int)density*29,(int)(screen.getWidth()-23*density), (int)(screen.getWidth()-29*density)};
		int[] testingPointY = {(int)density*23,(int)density*29,(int)(screen.getHeight()-23*density), (int)(screen.getHeight()-29*density)};
		for ( int x = 0; x < testingPointX.length; x++ ){
			for ( int y = 0; y < testingPointY.length; y++ ) {
				int bX = testingPointX[x];
				int bY = testingPointY[y];
				int color = screen.getPixel(bX, bY);
				if ( color == mapBackGround ){
					return false;
				}
			}
		}
		
		return true;
	}
	
	//Delays The runnable that takes initial picture of the view untill Map has loaded tiles
	@Override
	public boolean postDelayed(final Runnable r,final long milis){
		if ( r instanceof Run ) {
			Log.d("CubeGL","Request Screen");
			Runnable exec = new Runnable() {
				
				@Override
				public void run() {
					boolean usable = getScreenViewBitmapReal();
					if ( usable ) {
						Map.super.postDelayed(r,milis);
					}
					else {
						Map.this.postDelayed(r,milis);
					}
				}
			};
			return super.postDelayed(exec, 100);
		}
		return super.postDelayed(r,milis);
	}
}
