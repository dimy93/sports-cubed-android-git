package com.cube.UI.cubeSides;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

import com.appscumen.example.MySwitch;
import com.cube.AndroidUI.R;
import com.cube.SQL.Filter;
import com.triggertrap.seekarc.SeekArc;
import com.triggertrap.seekarc.SeekArc.OnSeekArcChangeListener;

public class PriceFilter extends Filter {
	private SeekArc price;
	private MySwitch price_type;
	private float priceSet = -1f;

	public PriceFilter(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public PriceFilter(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public PriceFilter(Context context) {
		super(context);
	}

	public float getMaxPrice() {
		return priceSet;
	}
	
	public boolean isPriceTypeMember() {
		return !price_type.isChecked();
	}

	@Override
	public void onChildViewAdded(View child, int id) {
		if (child instanceof MySwitch && id == R.id.price_type_picker) {
			price_type = (MySwitch) child;
			price_type.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					PriceFilter.this.change();
				}
			});
		}
		if (child instanceof SeekArc && id == R.id.price_seek) {
			price = (SeekArc) child;
			price.setOnSeekArcChangeListener(new OnSeekArcChangeListener() {
				
				@Override
				public void onStopTrackingTouch(SeekArc seekBar) {
					PriceFilter.this.change();
				}
				
				@Override
				public void onStartTrackingTouch(SeekArc seekBar) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onProgressChanged(SeekArc seekBar, int progress,
						boolean fromUser) {
					if( progress == seekBar.getMax() )
					{
						priceSet = -1;
						return;
					}
					priceSet = 50f*progress/seekBar.getMax();
				}
			});
		}
	}

}
