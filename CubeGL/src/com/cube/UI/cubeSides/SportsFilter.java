
package com.cube.UI.cubeSides;


import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.SectionIndexer;

import com.cube.AndroidUI.R;
import com.cube.SQL.Filter;
import com.cube.SQL.SQLQuery;
import com.cube.UI.customWidgets.CustomListView;

public class SportsFilter extends Filter
{
	private CustomListView sportPicker;
	private String [ ] sports;
	private InfiniteAdapter<String> adapter;
	
	public SportsFilter ( Context context, AttributeSet attrs, int defStyle )
	{
		super ( context, attrs, defStyle );
		SQLQuery.setSports ( );
	}
	
	public SportsFilter ( Context context, AttributeSet attrs )
	{
		super ( context, attrs );
		SQLQuery.setSports ( );
	}
	
	public SportsFilter ( Context context )
	{
		super ( context );
		SQLQuery.setSports ( );
	}
	
	public void setSports ( String [ ] sports )
	{
		this.sports = sports;
	}
	
	@Override
	public void onChildViewAdded ( View child, int id )
	{
		initTable ( child, id );
	}
	
	@Override
	public boolean onZoomOutEnd() {
		//Fixes the bug of listview remaining focused and selectable
		sportPicker.setVisibility(View.INVISIBLE) ;
		return super.onZoomOutStart();
	}
	
	@Override
	public boolean onZoomInStart() {
		//Fixes the bug of listview remaining focused and selectable
		sportPicker.setVisibility(View.VISIBLE) ;
		return super.onZoomOutStart();
	}
	
	public void initTable ( View child, int id )
	{
		if ( R.id.sports_chooser == id )
		{
			
			sportPicker = (CustomListView) findViewById(id);
			sportPicker.setFastScrollEnabled(true);
			adapter = new InfiniteAdapter<String>(
					sportPicker, R.layout.sport_list_item, sports);
			sportPicker.setAdapter(adapter);
			sportPicker.setSelection(adapter.getInitial());
			sportPicker.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					adapter.setState(!adapter.getState(position), position);
					adapter.getView(position, view, sportPicker);
				}
			});
		}
	}
	
	public Vector < String > getSports ( )
	{
		if ( sportPicker == null )
			return null;
		return adapter.getChecked();
	}
	
	public void onChecked( int checkboxIndex, int slotIndex )
	{
		this.change();
	}
	public static class InfiniteAdapter<T> extends ArrayAdapter<T> implements SectionIndexer {
		
		private ArrayList<Boolean> checked = new ArrayList<Boolean>();
		private HashMap<String, Integer> alphaIndexer;
		private String[] sections;
		private final CustomListView mListView;

		public InfiniteAdapter(CustomListView listView, int resource, T[] objects) {
			super(listView.getContext(), resource, objects);
			for ( int i = 0; i < objects.length; i++ ){
				checked.add(false);
			}
			
			 alphaIndexer = new HashMap<String, Integer>();
             // in this hashmap we will store here the positions for
             // the sections

             int size = objects.length;
             for (int i = size - 1; i >= 0; i--) {
                     String element = (String) objects[i];
                     alphaIndexer.put(element.substring(0, 1), i);
             //We store the first letter of the word, and its index.
             //The Hashmap will replace the value for identical keys are putted in
             }

             // now we have an hashmap containing for each first-letter
             // sections(key), the index(value) in where this sections begins

             // we have now to build the sections(letters to be displayed)
             // array .it must contains the keys, and must (I do so...) be
             // ordered alphabetically

             Set<String> keys = alphaIndexer.keySet(); // set of letters ...sets
             // cannot be sorted...

             Iterator<String> it = keys.iterator();
             ArrayList<String> keyList = new ArrayList<String>(); // list can be
             // sorted

             while (it.hasNext()) {
                     String key = it.next();
                     keyList.add(key);
             }

             Collections.sort(keyList);

             sections = new String[keyList.size()]; // simple conversion to an
             // array of object
             keyList.toArray(sections);
             mListView = listView;
		}

		
		@Override
		public T getItem(int position) {
		    if ( getRealCount()==0 ) {
		        return null;
		    }
		    return super.getItem( position%getRealCount() );
		}

		@Override
		public long getItemId(int position) {
			if ( getRealCount()==0 ) {
		        return -1;
		    }

		    return super.getItemId( position%getRealCount());
		}
		
		public void setState( boolean check, int position ) {
			position = position%getRealCount();
			checked.set(position, check);
		}
		
		public boolean getState( int position ) {
			position = position%getRealCount();
			return checked.get(position);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

		    if ( getRealCount()==0 ) {
		        return null;
		    }
		    position = position%getRealCount();
		    View result = super.getView(position, convertView, parent);
		    if( !(result instanceof CheckedTextView ) )
		    	return result;
		    if (checked.get(position)){
		    	//((CheckedTextView)result).setTextColor(Color.parseColor("#FF00ff7f"));
		    	((CheckedTextView)result).setChecked(true);
		    }
		    else {
		    	//((TextView)result).setTextColor(Color.parseColor("#FFFFFFFF"));
		    	((CheckedTextView)result).setChecked(false);
		    }
		    return result;
		}
		
	    @Override
		public int getCount() {
			return Integer.MAX_VALUE;
		}
		
		public Vector<String> getChecked(){
			Vector<String> vec = new Vector<String>();
			for ( int index = 0; index < checked.size(); index++ ) {
				boolean enabled = checked.get(index);
				T item = this.getItem(index);
				if ( enabled ) {
					vec.add(item.toString());
				}
			}
			return vec;
		}
		
		public int getInitial(){
			int max_2 = Integer.MAX_VALUE/2;
			return max_2 - max_2 % getRealCount();
		}
		
		public int getRealCount(){
			return super.getCount();
		}
		
		@Override
        public int getPositionForSection(int section) {
			
                String letter = sections[section];
                int current = mListView.getFirstVisiblePosition();
                int pos = alphaIndexer.get(letter);
                current = current - current % getRealCount();
                return current + pos;
        }

        @Override
        public int getSectionForPosition(int position) {
                return 0;
        }

        @Override
        public Object[] getSections() {

                return sections; // to string will be called each object, to display
                // the letter
        }
		
	}
}
