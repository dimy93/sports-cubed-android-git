
package com.cube.UI.cubeSides;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ToggleButton;

import com.cube.AndroidUI.R;
import com.cube.SQL.Filter;

public class TimeFilter extends Filter
{
	private ToggleButton [ ] weekToggles = new ToggleButton [ 7 ];
	private ToggleButton wholeToggle;
	
	public TimeFilter ( Context context, AttributeSet attrs, int defStyle )
	{
		super ( context, attrs, defStyle );
	}
	
	public TimeFilter ( Context context, AttributeSet attrs )
	{
		super ( context, attrs );
	}
	
	public TimeFilter ( Context context )
	{
		super ( context );
	}
	
	@Override
	public void onChildViewAdded ( View child, int id )
	{
		initWholeWeekToggle ( child, id );
		initWeekDayToggle ( child, id );
		
	}
	
	public int getDays(){
		int days = 0;
		for(ToggleButton day : weekToggles)
		{
			if ( !day.isChecked() )
				continue;
			int i;
			for ( i = 1; i < 8; i++ )
			{
				int id_weekday = getContext ( ).getResources ( ).getIdentifier ( "weekday_" + i, "id",
						"com.cube.AndroidUI" );
				if ( id_weekday == day.getId() )
					break;
			}
			days = days | Day.getDay(i);
		}
		return days & Day.getDay(8)-1;
	}
	
	private void initWholeWeekToggle ( View child, int id )
	{
		if ( id == R.id.whole_week )
		{
			wholeToggle = (ToggleButton) child;
			wholeToggle.setOnClickListener ( new OnClickListener ( ) {
				
				@Override
				public void onClick ( View toggle )
				{
					TimeFilter.this.change();
					for ( ToggleButton tb : weekToggles )
					{
						tb.setChecked ( wholeToggle.isChecked ( ) );
					}
				}
			} );
		}
	}
	
	private void initWeekDayToggle ( View child, int id )
	{
		for ( int i = 1; i < 8; i++ )
		{
			int id_weekday = getContext ( ).getResources ( ).getIdentifier ( "weekday_" + i, "id",
					"com.cube.AndroidUI" );
			if ( id_weekday == id )
			{
				weekToggles [ i - 1 ] = (ToggleButton) child;
				weekToggles [ i - 1 ]
						.setOnCheckedChangeListener ( new ToggleButton.OnCheckedChangeListener ( ) {
							
							@Override
							public void onCheckedChanged ( CompoundButton btn, boolean checked )
							{
								TimeFilter.this.change();
								if ( !checked )
								{
									wholeToggle.setChecked ( false );
								}
								
							}
							
						} );
				return;
			}
		}
	}
	
	public static class Day
	{
		public final static int MONDAY = 1;
		public final static int TUESDAY = 2;
		public final static int WEDNESDAY = 4;
		public final static int THURSDAY = 8;
		public final static int FRIDAY = 16;
		public final static int SATURDAY = 32;
		public final static int SUNDAY = 64;
		
		public static int getDay ( int index )
		{
			return 1 << index-1;
		}
	}
	
	public int getWeekDays ( )
	{
		int result = 0;
		for ( int i = 0; i < weekToggles.length; i++ )
		{
			result |= weekToggles [ i ].isChecked ( ) ? Day.getDay ( i ) : 0;
		}
		return result;
	}
}
