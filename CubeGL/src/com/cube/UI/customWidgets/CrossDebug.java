package com.cube.UI.customWidgets;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import com.nineoldandroids.view.ViewHelper;

public class CrossDebug extends View {

	private Paint p = new Paint();
	private int offsetY;

	public CrossDebug(final Context context) {
		super(context);
		((ViewGroup) ((Activity) this.getContext()).findViewById(
				android.R.id.content).getRootView()).addView(this);
		offsetY = -1;

		p.setColor(0xffff0000);
		p.setStyle(Style.FILL);
		p.setStrokeWidth(10);
		final ViewTreeObserver vto = this.getViewTreeObserver();
		ViewTreeObserver.OnGlobalLayoutListener layoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
			@Override
			public void onGlobalLayout() {
				setOffSetY();
				CrossDebug.this.getViewTreeObserver().removeGlobalOnLayoutListener(this);
			}
		};
		vto.addOnGlobalLayoutListener(layoutListener);
	}

	private int getActionBarHeight(Context c) {
		int[] loc = new int[2];
		((Activity) this.getContext()).findViewById(
				android.R.id.content).getLocationOnScreen(loc);
		return loc[1];
	}

	private void setOffSetY() {
		offsetY = getActionBarHeight(getContext());
	}

	@Override
	public void onDraw(Canvas c) {
		super.onDraw(c);
		int x = (int) getx();
		int y = (int) (gety() + offsetY);
		
		c.drawLine(x-10, y-10, x+10, y+10, p);
		c.drawLine(x - 10, y  + 10, x + 10, y - 10, p);
	}

	public float getx() {
		return ViewHelper.getX(this);
	}

	public float gety() {
		return ViewHelper.getY(this);
	}

	public void setx(float x) {
		ViewHelper.setX(this, x);
	}

	public void sety(float y) {
		ViewHelper.setY(this, y );
	}

}
