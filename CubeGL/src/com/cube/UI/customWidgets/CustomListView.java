package com.cube.UI.customWidgets;

import java.lang.reflect.Field;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollBarDrawable;

import com.cube.UI.cubeSides.SportsFilter.InfiniteAdapter;

public class CustomListView extends ListView {
	private Field scrollBarDrawable;
	private Field scrollCache;
	private boolean scrollInterceptedTouch = false;
	private boolean preventClicking = false;
	
	public boolean getPreventClicking( ){
		return preventClicking;
	}
	
	@Override
	public void setVisibility(int visibility){
		if ( visibility == ListView.VISIBLE )
		{
			this.postDelayed(new Runnable() {
			
				@Override
				public void run() {
					if ( mScroller != null )
						mScroller.show();
				}
			},1000);
		}
		super.setVisibility(visibility);
	}
	
	//Load fields
	private void loadReflection(){
		try {
			scrollCache = View.class.getDeclaredField("mScrollCache");
			scrollCache.setAccessible(true);
			scrollBarDrawable = getScrollCache().getClass().getDeclaredField("scrollBar");
			scrollBarDrawable.setAccessible(true);
			changeClassOfScrollBarDrawable();
		} catch (Exception e) {
			
		}
	}
	// Apply fix so that the ScrollDrawable is of class ScrollBarDrawableMinHeightFix who skips the scrollbar min height application
	private void changeClassOfScrollBarDrawable(){
		try {
			ScrollBarDrawable oldvalue = getScrollBarDrawable();
			if ( !( oldvalue instanceof ScrollBarDrawableMinHeightFix ) )
				scrollBarDrawable.set( getScrollCache(), new ScrollBarDrawableMinHeightFix(oldvalue));
		} catch (Exception e) {
		}
	}
	// Get the current value of the ScrollCache
	private Object getScrollCache(){
		try {
			return scrollCache.get(this);
		} catch (Exception e) {
			return null;
		}
	}
	// Get the current value of the ScrollBarDrawable
	private ScrollBarDrawable getScrollBarDrawable(){
		try {
			return (ScrollBarDrawable) scrollBarDrawable.get( getScrollCache() );
		} catch (Exception e) {
			return null;
		}
	}

	public CustomListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		loadReflection();
	}

	public CustomListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		loadReflection();
	}

	public CustomListView(Context context) {
		super(context);
		loadReflection();
	}
	
	public int computRealExtent(){
		final boolean mSmoothScrollbarEnabled = isSmoothScrollbarEnabled();
        final int count = getChildCount();
        if (count > 0) {
            if (mSmoothScrollbarEnabled) {
                int extent = count * 1000;

                View view = getChildAt(0);
                final int top = view.getTop();
                int height = view.getHeight();
                if (height > 0) {
                    extent += (top * 1000) / height;
                }

                view = getChildAt(count - 1);
                final int bottom = view.getBottom();
                height = view.getHeight();
                if (height > 0) {
                    extent -= ((bottom - getHeight()) * 1000) / height;
                }
                
                return extent;
            } else {
                return 1;
            }
        }
        return 0;
	}
	
	// @Override but hidden
	protected boolean isVerticalScrollBarHidden() {
		return false;
	}
	
	@Override
	protected int computeVerticalScrollOffset () {
		if (!( getAdapter() instanceof InfiniteAdapter<?> )){
    		return super.computeVerticalScrollOffset();
    	}
    	final int mItemCount = ((InfiniteAdapter<?>)getAdapter()).getRealCount();
        final boolean mSmoothScrollbarEnabled = isSmoothScrollbarEnabled();
        final int mScrollY = this.getScrollY();
		final int firstPosition = getFirstVisiblePosition()%mItemCount;
        final int childCount = getChildCount();
        if (firstPosition >= 0 && childCount > 0) {
            if (mSmoothScrollbarEnabled) {
                final View view = getChildAt(0);
                final int top = view.getTop();
                int height = view.getHeight();
                if (height > 0) {
                    return Math.max(firstPosition * 1000 - (top * 1000) / height +
                            (int)((float)mScrollY / getHeight() * mItemCount * 1000), 0);
                }
            } else {
                int index;
                final int count = mItemCount;
                if (firstPosition == 0) {
                    index = 0;
                } else if (firstPosition + childCount == count) {
                    index = count;
                } else {
                    index = firstPosition + childCount / 2;
                }
                return (int) (firstPosition + childCount * (index / (float) count));
            }
        }
        return 0;
	}
	
	@Override
	protected int computeVerticalScrollExtent() {
		if (!( getAdapter() instanceof InfiniteAdapter<?> )){
    		return super.computeVerticalScrollExtent();
    	}
    	
		final boolean mSmoothScrollbarEnabled = isSmoothScrollbarEnabled();
        final int count = getChildCount();
        if (count > 0) {
            if (mSmoothScrollbarEnabled) {
                int extent = count * 1000;

                View view = getChildAt(0);
                final int top = view.getTop();
                int height = view.getHeight();
                if (height > 0) {
                    extent += (top * 1000) / height;
                }

                view = getChildAt(count - 1);
                final int bottom = view.getBottom();
                height = view.getHeight();
                
                if (height > 0) {
                    extent -= ((bottom - getHeight()) * 1000) / height;
                }
                
                return extent;
            } else {
                return 1;
            }
        }
        return 0;
    }
	
	
	// Copy of the original computeVerticalScrollRange but with mItemCount modified 
    @Override
    protected int computeVerticalScrollRange() {
    	if (!( getAdapter() instanceof InfiniteAdapter<?> )){
    		return super.computeVerticalScrollRange();
    	}
    	
    	final int mItemCount = ((InfiniteAdapter<?>)getAdapter()).getRealCount();
        final boolean mSmoothScrollbarEnabled = isSmoothScrollbarEnabled();
        final int mScrollY = this.getScrollY();
    	
        int result;
        if (mSmoothScrollbarEnabled) {
            result = Math.max(mItemCount * 1000, 0);
            if (mScrollY != 0) {
                // Compensate for overscroll
                result += Math.abs((int) ((float) mScrollY / getHeight() * mItemCount * 1000));
            }
        } else {
            result = mItemCount;
        }
        return result;
    }
    
    // Draw Upper part of the scrollbar as another scrollbar 
    protected void needsScrollbarRedraw(){
    	changeClassOfScrollBarDrawable();
    }
    
	private boolean mIsFastScrollEnabled = false;
	private IndexScroller mScroller = null;

	@Override
	public boolean isFastScrollEnabled() {
		return mIsFastScrollEnabled;
	}

	@Override
	public void setFastScrollEnabled(boolean enabled) {
		mIsFastScrollEnabled = enabled;
		if (mIsFastScrollEnabled) {
			if (mScroller == null)
				mScroller = new IndexScroller(getContext(), this);
		} else {
			if (mScroller != null) {
				mScroller.hide();
				mScroller = null;
			}
		}
	}

	@Override
	public void draw(Canvas canvas) {
		super.draw(canvas);
		
		// Overlay index bar
		if (mScroller != null)
			mScroller.draw(canvas);
	}

	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		int x = (int)ev.getX();
		int y = (int)ev.getY();
		boolean insideScroller = mScroller != null ? mScroller.getBounds().contains(x, y) : false;
		if (mScroller != null && ( scrollInterceptedTouch || insideScroller)){
			scrollInterceptedTouch = mScroller.onTouchEvent(ev);
			if ( scrollInterceptedTouch ){
				return true;
			}
		}
		return super.onTouchEvent(ev);
	}
	
	@Override
	public void smoothScrollToPosition(final int position){
		
	}
	
	@Override
	public void setSelectionFromTop(int position, int y) 
	{
		super.setSelectionFromTop(position,y);
		awakenScrollBars();
	}
	
	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		return true;
	}

	@Override
	public void setAdapter(ListAdapter adapter) {
		super.setAdapter(adapter);
		if (mScroller != null)
			mScroller.setAdapter(adapter);
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		if (mScroller != null)
			mScroller.onSizeChanged(w, h, oldw, oldh);
	}
    
    //Skip minHeight of ScrollBarDrawable
    public class ScrollBarDrawableMinHeightFix extends ScrollBarDrawable {
    	
    	private int mExtent;
    	private int mRange;
    	private int mOffset;
		private Drawable mVerticalThumb;
		
    	// Copy all the the fields of the original srollBar
    	public ScrollBarDrawableMinHeightFix( ScrollBarDrawable original ){
    		for ( Field f : ScrollBarDrawable.class.getDeclaredFields( ) ) {
    			f.setAccessible(true);
    			try {
    				Object value = f.get(original);
					f.set(this, value);
					if ( f.getName().equals("mVerticalThumb") ){
						mVerticalThumb = (Drawable) value;
					}
				} catch (Exception e) {
					
				}
    		}
    	}
    	// When thumb is drawn length and offset are recalculated to bypass the min height application
    	@Override
    	protected void drawThumb(Canvas canvas, Rect bounds, int offset, int length, boolean vertical) {
    		int size = vertical ? bounds.height() : bounds.width();
    		length = Math.round((float) size * mExtent / mRange);
    		offset = Math.round((float) (size - length) * mOffset / (mRange - mExtent));
    		if (offset + length > size) {
    			int upperLength = length - size + offset;
    			upperLength = upperLength < mVerticalThumb.getMinimumHeight()?0:upperLength;
    			super.drawThumb(canvas, bounds, 0, upperLength, vertical);
    			length = size - offset > 0 ? size - offset : 0;
    		}
    		if ( mVerticalThumb != null ){
    			length = length < mVerticalThumb.getMinimumHeight()?0:length;
    		}
    		super.drawThumb(canvas, bounds, offset, length, vertical);
        }
    
    	//Get range, extent and offset
    	@Override
    	public void setParameters(int range, int offset, int extent, boolean vertical) {
    		mRange = range;
    		mExtent = extent;
    		mOffset = offset;
    		super.setParameters(range, offset, extent, vertical);
    	}
    	
    }
}
