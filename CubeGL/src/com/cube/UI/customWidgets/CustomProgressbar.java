
package com.cube.UI.customWidgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class CustomProgressbar extends RelativeLayout
{
	private ProgressBar pb;
	private TextView tv;
	
	public CustomProgressbar ( Context context, AttributeSet attrs, int defStyle )
	{
		super ( context, attrs, defStyle );
		addComponents ( context );
	}
	
	public CustomProgressbar ( Context context, AttributeSet attrs )
	{
		super ( context, attrs );
		addComponents ( context );
	}
	
	public CustomProgressbar ( Context context )
	{
		super ( context );
		addComponents ( context );
	}
	
	@SuppressWarnings ( "deprecation" )
	private void addComponents ( Context c )
	{
		pb = new ProgressBar ( c );
		addView ( pb );
		RelativeLayout.LayoutParams rlpPB = (RelativeLayout.LayoutParams) pb.getLayoutParams ( );
		tv = new TextView ( c );
		addView ( tv );
		RelativeLayout.LayoutParams rlpTV = (RelativeLayout.LayoutParams) tv.getLayoutParams ( );
		rlpPB.width = rlpPB.height = rlpTV.width = rlpTV.height = RelativeLayout.LayoutParams.FILL_PARENT;
		tv.setGravity ( Gravity.CENTER );
	}
	
	public void setProgress ( int progress )
	{
		try
		{
			pb.setProgress ( progress );
			if ( 0 <= progress && progress <= 100 )
			{
				tv.setText ( progress + "%" );
				tv.setVisibility ( View.VISIBLE );
			}
			else
			{
				tv.setVisibility ( View.INVISIBLE );
			}
			pb.setVisibility ( View.VISIBLE );
		}
		catch ( Exception e )
		{
			e.printStackTrace ( );
		}
	}
	
	public void resetProgress ( )
	{
		setProgress ( 0 );
	}
	
}
