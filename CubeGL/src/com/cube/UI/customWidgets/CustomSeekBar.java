package com.cube.UI.customWidgets;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.cube.AndroidUI.R;
import com.nineoldandroids.view.ViewHelper;

public class CustomSeekBar extends SeekBar implements OnSeekBarChangeListener {

	private TextView tv;
	private Rect boundsFree = new Rect();
	private Rect boundsNonFree = new Rect();
	private Rect boundsAny = new Rect();
	private int initX = 0;
	private int initY = 0;
	private ColorStateList mTextColors;
	private int mTextSize;
	private int mTypefaceIndex;
	private int mStyleIndex;
	private boolean textChanged;
	private OnSeekBarChangeListener listener;
	private Drawable mThumb;
	private float mShadowDx;
	private float mShadowDy;
	private float mShadowRadius;
	private int mShadowColor;
	private float fraction;

	public CustomSeekBar(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		parseAttr(context, attrs);
	}
	
	public CustomSeekBar(Context context, AttributeSet attrs) {
		super(context, attrs);
		parseAttr(context, attrs);
	}

	public CustomSeekBar(Context context) {
		super(context);
	}

	private void parseAttr(Context context, AttributeSet attrs) {
		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.CustomSeekBar);
		int appearance = a.getResourceId(
				R.styleable.CustomSeekBar_textAppearance, 0);
		if (appearance != 0) {
			changeTextAppearance(context, appearance);
		}
		a.recycle();
		fraction = (getSecondaryProgress()+0f)/getMax();
	}

	private void changeTextLook() {
		if (!textChanged)
			return;
		if (mTextColors != null) {
			tv.setTextColor(mTextColors);
		}
		Typeface tf = null;
		switch (mTypefaceIndex) {
		case 1:
			tf = Typeface.SANS_SERIF;
			break;

		case 2:
			tf = Typeface.SERIF;
			break;

		case 3:
			tf = Typeface.MONOSPACE;
			break;
		}
		tv.setTypeface(tf, mStyleIndex);
		if (mTextSize != 0)
			tv.setTextSize(TypedValue.COMPLEX_UNIT_PX,mTextSize);
		tv.getPaint().getTextBounds("Free", 0, 4, boundsFree);
		tv.getPaint().getTextBounds("£66.66", 0, 6, boundsNonFree);
		tv.getPaint().getTextBounds("Any", 0, 3, boundsAny);
		tv.setShadowLayer(mShadowRadius, mShadowDx, mShadowDy, mShadowColor);
		textChanged = false;
	}

	public void changeTextAppearance(Context context, int resid) {
		TypedArray appearance = context.obtainStyledAttributes(resid,
				R.styleable.CustomTextAppearanceAttrib);

		mTextColors = appearance
				.getColorStateList(R.styleable.CustomTextAppearanceAttrib_android_textColor);

		mTextSize = appearance.getDimensionPixelSize(
				R.styleable.CustomTextAppearanceAttrib_android_textSize, 0);
		
		mShadowDx = appearance.getFloat(
				R.styleable.CustomTextAppearanceAttrib_android_shadowDx, 0);
		mShadowDy = appearance.getFloat(
				R.styleable.CustomTextAppearanceAttrib_android_shadowDy, 0);
		mShadowRadius = appearance.getFloat(
				R.styleable.CustomTextAppearanceAttrib_android_shadowRadius, 0);
		mShadowColor = appearance.getColor(
				R.styleable.CustomTextAppearanceAttrib_android_shadowColor, 0);

		mTypefaceIndex = appearance.getInt(
				R.styleable.CustomTextAppearanceAttrib_android_typeface, -1);
		mStyleIndex = appearance.getInt(
				R.styleable.CustomTextAppearanceAttrib_android_textStyle, -1);

		appearance.recycle();
		invalidate();
		textChanged = true;
	}

	public float getPrice() {
		int progress = getProgress();
		int max = getMax();
		if ( progress == max ) {
			return -1f;
		}
		if (progress <= max * fraction) {
			return 0f;
		}

		float realProgress = progress - max * fraction;
		realProgress /= (1-fraction)*max;
		return realProgress * 50f;
	}

	@Override
	protected void onAttachedToWindow() {
		super.onAttachedToWindow();
		super.setOnSeekBarChangeListener(this);
		tv = new TextView(getContext());
		View parent = this;
		while (!(parent.getParent() instanceof RelativeLayout))
			parent = (View) parent.getParent();
		parent = (View) parent.getParent();
		((RelativeLayout) parent).addView(tv);

		tv.bringToFront();
	}

	@Override
	public void setThumb(Drawable thumb) {
		super.setThumb(thumb);
		mThumb = thumb;
	}

	public Drawable getSeekBarThumb() {
		return mThumb;
	}

	@Override
	public void onLayout(boolean changed, int left, int top, int right,
			int bottom) {
		super.onLayout(changed, left, top, right, bottom);
		initX = left + getPaddingLeft();
		initY = top - 10;
		changeTextLook();
		onProgressChanged(this, getProgress(), false);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
	}

	
	@Override
	public void setOnSeekBarChangeListener(OnSeekBarChangeListener listener){
		this.listener = listener;
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		int max = getMax();
		String text = null;
		Rect bounds = null;
		if (progress <= max*fraction) {
			setProgress((int) (max*fraction*0.5f));
			text = "Free";
			bounds = boundsFree;
		} else if ( progress == max ){
			text = "Any";
			bounds = boundsAny;
		}
		else {
			float price = getPrice();
			String price_formated;
			if ( Math.floor(price) + Math.floor(price) - Math.floor(2*price)  > -0.5 ) // if first digit after the decimal point is 5 or more
				price_formated = (int)(price-0.5f) + ".50";
			else
				price_formated = (int)(price-0.5f) + ".00";
			text = "£" + price_formated;
			bounds = boundsNonFree;
		}
		tv.setText(text);

		float x = initX+mThumb.getBounds().left;
		x -= bounds.width()/2f;
		float y = initY;
		y -= bounds.height();
		ViewHelper.setX(tv, x);
		ViewHelper.setY(tv, y);
		if ( listener != null )
			listener.onProgressChanged(seekBar, progress, fromUser);
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		if ( listener != null )
			listener.onStartTrackingTouch(seekBar);
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		if ( listener != null )
			listener.onStopTrackingTouch(seekBar);
	}

}
