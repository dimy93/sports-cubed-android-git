
package com.cube.UI.customWidgets;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

public class HeapView extends View
{
	private Paint paint = new Paint ( );
	private long last_display;
	private String last;
	
	public HeapView ( Context context )
	{
		super ( context );
		paint.setStyle ( Paint.Style.FILL_AND_STROKE );
		paint.setColor ( Color.RED );
		paint.setStrokeWidth ( 1 );
		determineMaxTextSize ( "128.00 MB / 128.00 MB", 500 );
		last_display = System.currentTimeMillis ( );
	}
	
	public HeapView ( Context context, AttributeSet attrs, int defStyle )
	{
		super ( context, attrs, defStyle );
		paint.setStyle ( Paint.Style.FILL_AND_STROKE );
		paint.setColor ( Color.RED );
		paint.setStrokeWidth ( 1 );
		determineMaxTextSize ( "128.00 MB / 128.00 MB", 500 );
		last_display = System.currentTimeMillis ( );
	}
	
	public HeapView ( Context context, AttributeSet attrs )
	{
		super ( context, attrs );
		paint.setStyle ( Paint.Style.FILL_AND_STROKE );
		paint.setColor ( Color.RED );
		paint.setStrokeWidth ( 1 );
		determineMaxTextSize ( "128.00 MB / 128.00 MB", 500 );
		last_display = System.currentTimeMillis ( );
	}
	
	@Override
	protected void onDraw ( Canvas canvas )
	{
		super.onDraw ( canvas );
		invalidate ( );
		
		long current = System.currentTimeMillis ( );
		if ( current - last_display < 500 && last != null )
		{
			canvas.drawText ( last, 50, 50, paint );
			return;
		}
		
		last_display = current;
		long available = Runtime.getRuntime ( ).maxMemory ( ) * 100 / 1048576;
		long free = Runtime.getRuntime ( ).freeMemory ( ) * 100 / 1048576;
		long used = available - free;
		last = used / 100 + "." + used % 100 + " MB / " + available / 100 + "." + available % 100
				+ " MB";
		
		canvas.drawText ( last, 50, 50, paint );
	}
	
	private int determineMaxTextSize ( String str, float maxWidth )
	{
		int size = 0;
		Paint paint = new Paint ( );
		
		do
		{
			paint.setTextSize ( ++size );
		}
		while ( paint.measureText ( str ) < maxWidth );
		
		return size;
	} // End getMaxTextSize()
}
