
package com.cube.UI.updateFromServer;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.util.Date;
import java.util.Vector;
import java.util.concurrent.CountDownLatch;

import com.cube.UI.customWidgets.CustomProgressbar;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.view.View;


public class ServerConnector extends AsyncTask < String, String, Void >
{
	private final Activity activity;
	private final CustomProgressbar progress;
	private final Vector < DownloadJob > downloads;
	private final Runnable postExecuteError;
	private final Runnable postExecute;
	private final long lastUpdate = -1;
	private final TimeoutThread timeout = new TimeoutThread();
	
	public ServerConnector ( Activity activity, CustomProgressbar progress, Runnable postExecute,
			Runnable postExecuteError )
	{
		this.activity = activity;
		this.progress = progress;
		this.postExecute = postExecute;
		this.postExecuteError = postExecuteError;
		downloads = new Vector < DownloadJob > ( );
	}
	
	/**
	 * Downloading file in background thread
	 * */
	@Override
	protected Void doInBackground ( String... f_url )
	{
		long total_fileSize = 0;
		try
		{
			for ( int i = 0; i < f_url.length; i += 2 )
			{
				DownloadJob dj = new DownloadJob ( f_url [ i ], f_url [ i + 1 ] );
				boolean willDw = dj.willDownload ( );
				if ( willDw )
				{
					downloads.add ( dj );
					total_fileSize += dj.fileSize;
				}
			}
			
			long current_fileSize = 0;
			for ( DownloadJob dj : downloads )
			{
				boolean downloaded = downloadFile ( dj, current_fileSize, total_fileSize );
				if ( downloaded )
				{
					setCurrentVersion ( dj );
				}
				else
					if ( dj.mustDownload ( ) == true )
					{
						throw new NoFileAvailable ( buildPath ( dj.fileName ) );
					}
				current_fileSize += dj.fileSize;
			}
		}
		catch ( Exception e )
		{
			e.printStackTrace ( );
		}
		return null;
	}
	
	/**
	 * Before starting background thread
	 * Show Progress Bar Dialog
	 * */
	@Override
	protected void onPreExecute ( )
	{
		super.onPreExecute ( );
		progress.setVisibility ( View.VISIBLE );
		progress.resetProgress ( );
		timeout.start();
	}
	
	/**
	 * Updating progress bar
	 * */
	@Override
	protected void onProgressUpdate ( String... progress )
	{
		// setting progress percentage
		int percents = Integer.parseInt ( progress [ 0 ] );
		this.progress.setProgress ( percents );
		timeout.restart(percents);
	}
	
	@Override
	protected void onCancelled() {
		onPostExecute(null);
	}

	/**
	 * After completing background task
	 * Dismiss the progress dialog
	 * **/
	@Override
	protected void onPostExecute ( Void result )
	{
		timeout.onDownloadFinished();
		progress.setVisibility ( View.INVISIBLE );
		boolean downloaded = true;
		for ( DownloadJob dj : downloads )
		{
			if ( dj.mustDownload ( ) )
			{
				downloaded &= getCurrentVersion ( dj.fileName ) != -1;
			}
			if ( downloaded == false )
			{
				break;
			}
		}
		if ( downloaded )
		{
			postExecute.run ( );
		}
		else
		{
			postExecuteError.run ( );
		}
	}
	
	private long getCurrentVersion ( String fileName )
	{
		String log = buildLogPath ( fileName );
		try
		{
			BufferedReader br = new BufferedReader ( new FileReader ( new File ( log ) ) );
			String line;
			line = br.readLine ( );
			long last_accessed = Long.parseLong ( line );
			br.close ( );
			return last_accessed;
		}
		catch ( FileNotFoundException e )
		{
			return -1;
		}
		catch ( IOException e )
		{
			return -1;
		}
		
	}
	
	private String buildPath ( String fileName )
	{
		return activity.getFilesDir ( ) + "/" + fileName;
	}
	
	private String buildLogPath ( String fileName )
	{
		return buildPath ( fileName ) + ".log";
	}
	
	private String buildTempPath ( String fileName )
	{
		return buildPath ( fileName ) + ".temp";
	}
	
	private boolean fileRename ( String fileName )
	{
		
		File from = new File ( buildTempPath ( fileName ) );
		File to = new File ( buildPath ( fileName ) );
		if ( !from.exists ( ) )
		{
			return false;
		}
		to.delete ( );
		from.renameTo ( to );
		return true;
	}
	
	private void setCurrentVersion ( DownloadJob dj )
	{
		try
		{
			File log = new File ( buildLogPath ( dj.fileName ) );
			log.createNewFile ( );
			FileOutputStream fos;
			fos = new FileOutputStream ( log );
			fos.write ( ( dj.new_version + "" ).getBytes ( ) );
			fos.close ( );
		}
		catch ( IOException e )
		{
			e.printStackTrace ( );
		}
	}
	
	private boolean buildYesNoDialog ( DownloadJob dj )
	{
		final CountDownLatch wait_answer = new CountDownLatch ( 1 );
		final WaitYesNoDialog dialog = new WaitYesNoDialog ( wait_answer, dj );
		activity.runOnUiThread ( dialog );
		try
		{
			wait_answer.await ( );
		}
		catch ( InterruptedException e )
		{
			e.printStackTrace ( );
		}
		return dialog.getAnswer ( );
	}
	
	private boolean downloadFile ( DownloadJob dj, long current_fileSize, long total_fileSize )
	{
		try
		{
			int count;
			dj.conection.connect ( );
			
			// download the file
			InputStream input = new BufferedInputStream ( dj.conection.getInputStream ( ), 8192 );
			
			// Output stream
			OutputStream output = new FileOutputStream ( buildTempPath ( dj.fileName ) );
			
			byte data[] = new byte [ 1024 ];
			
			long total = current_fileSize;
			
			while ( ( count = input.read ( data ) ) != -1 )
			{
				total += count;
				// publishing the progress....
				// After this onProgressUpdate will be called
				publishProgress ( "" + (int) ( ( total * 100 ) / total_fileSize ) );
				
				// writing data to file
				output.write ( data, 0, count );
				if ( isCancelled ( ) )
				{
					output.flush ( );
					output.close ( );
					input.close ( );
					throw new Exception ( "The download has been cancelled." );
				}
			}
			
			// flushing output
			output.flush ( );
			
			// closing streams
			output.close ( );
			input.close ( );
			return fileRename ( dj.fileName );
			
		}
		catch ( Exception e )
		{
			( new File ( buildTempPath ( dj.fileName ) ) ).delete ( );
			return false;
		}
	}
	
	private class DownloadJob
	{
		final public URLConnection conection;
		final public String fileName;
		final public long new_version;
		final public long old_version;
		final public long fileSize;
		
		public DownloadJob ( String url_str, String fileName ) throws IOException
		{
			if ( isWifiEnabled ( ) || Build.FINGERPRINT.startsWith ( "generic" ) )
			{
				URL url = new URL ( url_str );
				this.conection = url.openConnection ( );
				this.conection.setConnectTimeout ( 1000 );
				this.new_version = conection.getHeaderFieldDate ( "Last-Modified", -1 );
				this.fileSize = conection.getContentLength ( );
			}
			else
			{
				this.conection = null;
				this.new_version = -1;
				this.fileSize = -1;
			}
			this.fileName = fileName;
			this.old_version = getCurrentVersion ( fileName );
		}
		
		public boolean willDownload ( )
		{
			if ( old_version == -1 )
			{
				return true;
			}
			if ( new_version != -1 && old_version < new_version )
			{
				return buildYesNoDialog ( this );
			}
			return false;
		}
		
		public boolean mustDownload ( )
		{
			return old_version == -1;
		}
		
		private boolean isWifiEnabled ( )
		{
			ConnectivityManager connManager = (ConnectivityManager) activity
					.getSystemService ( Context.CONNECTIVITY_SERVICE );
			NetworkInfo mWifi = connManager.getNetworkInfo ( ConnectivityManager.TYPE_WIFI );
			
			if ( mWifi != null && mWifi.isConnected ( ) )
				return true;
			return false;
		}
	}
	
	private class WaitYesNoDialog implements Runnable
	{
		private CountDownLatch wait_answer;
		private boolean answer;
		private String curr_version;
		private String latest_version;
		private String fileSizeMB;
		
		public WaitYesNoDialog ( CountDownLatch wait_answer, DownloadJob dj )
		{
			this.wait_answer = wait_answer;
			DateFormat df = DateFormat.getDateTimeInstance ( );
			this.curr_version = df.format ( new Date ( dj.old_version ) );
			this.latest_version = df.format ( new Date ( dj.new_version ) );
			if ( dj.fileSize != -1 )
			{
				double fileSizeMBDouble = dj.fileSize * 100 / ( 1024 * 1024 );
				fileSizeMBDouble /= 100;
				fileSizeMB = " approximately " + fileSizeMBDouble + " MB";
			}
			else
			{
				fileSizeMB = "unknown";
			}
		}
		
		public boolean getAnswer ( )
		{
			return answer;
		}
		
		@Override
		public void run ( )
		{
			AlertDialog.Builder builder = new AlertDialog.Builder ( activity );
			
			builder.setTitle ( "Do you want to download update data?" );
			builder.setMessage ( "Do you want to download the update file? Your current version is from "
					+ curr_version
					+ ". The latest version available is from "
					+ latest_version
					+ ". The update data size is " + fileSizeMB + "." );
			
			builder.setPositiveButton ( "YES", new DialogInterface.OnClickListener ( ) {
				
				public void onClick ( DialogInterface dialog, int which )
				{
					wait_answer.countDown ( );
					answer = true;
					dialog.dismiss ( );
				}
				
			} );
			
			builder.setNegativeButton ( "NO", new DialogInterface.OnClickListener ( ) {
				
				@Override
				public void onClick ( DialogInterface dialog, int which )
				{
					wait_answer.countDown ( );
					answer = false;
					dialog.dismiss ( );
				}
			} );
			
			builder.setOnCancelListener ( new DialogInterface.OnCancelListener ( ) {
				
				@Override
				public void onCancel ( DialogInterface dialog )
				{
					wait_answer.countDown ( );
					answer = false;
					dialog.dismiss ( );
				}
			} );
			builder.show ( );
			
		}
		
	};
	
	private static class NoFileAvailable extends Exception
	{
		private static final long serialVersionUID = 1L;
		private String file;
		
		public NoFileAvailable ( String file )
		{
			this.file = file;
		}
		
		@Override
		public String toString ( )
		{
			return "The following file is deployed on the device and hasn't yet been downloaded : "
					+ file;
		}
	}
	
	private class TimeoutThread extends Thread
	{
		private final long TIMEOUT_TIME = 90000;
		private long start = System.currentTimeMillis();
		private int progress = -1;
		private boolean stopped = false;
		@Override
		public void run(){
			while( System.currentTimeMillis() - start < TIMEOUT_TIME && ! stopped )
			{
				try {
					sleep(100);
				} catch (InterruptedException e) {
					
				}
			}
			if( !stopped )
				ServerConnector.this.cancel(false);
		}
		public void restart( int progress ){
			if (progress != this.progress)
			{
				this.progress = progress;
				start = System.currentTimeMillis();
			}
		}
		public void onDownloadFinished()
		{
			stopped = true;
		}
	}
	
}
