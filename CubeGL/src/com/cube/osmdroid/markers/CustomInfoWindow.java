
package com.cube.osmdroid.markers;

import java.util.ArrayList;

import org.osmdroid.bonuspack.overlays.DefaultInfoWindow;
import org.osmdroid.views.MapView;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.TextView;

import com.cube.AndroidUI.R;

public class CustomInfoWindow extends DefaultInfoWindow 
{
	private TextView moreInfo = null;
	private ArrayList<BubbleListener> listeners = new ArrayList<BubbleListener>();
	private Object item = null;
	public CustomInfoWindow ( MapView mapView )
	{
		super ( R.layout.itinerary_bubble, mapView );
		Button btn = (Button) ( mView.findViewById ( R.id.bubble_delete ) );
		btn.setOnClickListener ( new Button.OnClickListener ( ) {
			
			@Override
			public void onClick ( View arg0 )
			{
				CustomInfoWindow.this.close ( );
			}
		} );
		getView().setOnTouchListener(new View.OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				return false;
			}
		});
		getView().setClickable(true);
		getView().setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if ( event.getAction() == MotionEvent.ACTION_DOWN ){
					if ( moreInfo != null )
					{
						moreInfo.setPaintFlags( moreInfo.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG );
						moreInfo.setTextColor(0xff33b5e5);
					}
				}
				if ( event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL ){
					if ( moreInfo != null )
					{
						moreInfo.setPaintFlags( moreInfo.getPaintFlags() & ~Paint.UNDERLINE_TEXT_FLAG );
						moreInfo.setTextColor(0xffffffff);
					}
				}
				return false;
			}
		});
		getView().setOnClickListener(new View.OnClickListener ( ){

			@Override
			public void onClick(View v) {
				for (BubbleListener listener : listeners)
				{
					listener.onMoreInfoClicked(CustomInfoWindow.this,item);
				}
				CustomInfoWindow.this.close();
			}
		});
		moreInfo = ((TextView)mView.findViewById ( R.id.bubble_more_info ));
	}
	
	@Override
	public void onOpen(Object item)
	{
		super.onOpen(item);
		this.item = item;
		for (BubbleListener listener : listeners)
		{
			listener.onOpened(this, item);
		}
		if ( moreInfo != null )
		{
			moreInfo.setPaintFlags( moreInfo.getPaintFlags() & ~Paint.UNDERLINE_TEXT_FLAG );
			moreInfo.setTextColor(0xffffffff);
		}
	}
	
	@Override
	public void onClose() 
	{ 
		super.onClose();
		for (BubbleListener listener : listeners)
		{
			listener.onClosed(this,item);
		}
		item = null;
	};
	

	public void addListener( BubbleListener listener ){
		if ( !listeners.contains(listener) )
			listeners.add(listener);
	}

	public void removeListener( BubbleListener listener ){
		listeners.remove(listener);
	}
	
	public static interface BubbleListener{
		public void onMoreInfoClicked( CustomInfoWindow info, Object item );
		public void onClosed(CustomInfoWindow info, Object item );
		public void onOpened(CustomInfoWindow info, Object item);
	}
}
