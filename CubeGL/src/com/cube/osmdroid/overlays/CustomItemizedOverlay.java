
package com.cube.osmdroid.overlays;

import java.util.List;

import org.osmdroid.bonuspack.overlays.InfoWindow;
import org.osmdroid.bonuspack.overlays.ItemizedOverlayWithBubble;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.OverlayItem;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

public class CustomItemizedOverlay < Item extends OverlayItem > extends
		ItemizedOverlayWithBubble < Item >
{
	
	public CustomItemizedOverlay ( Context context, List < Item > aList,
 			MapView mapView, InfoWindow bubble )
	{
		super ( context, aList, mapView, bubble );
	}
	
	public CustomItemizedOverlay ( Context context, List < Item > aList,
			MapView mapView )
	{
		super ( context, aList, mapView );
	}
	
	@Override
	protected boolean hitTest ( final Item item, final android.graphics.drawable.Drawable marker,
			final int hitX, final int hitY )
	{
		Drawable d = item.getDrawable();
		if ( d == null )
			return false;
		Rect r = d.getBounds();
		int dy = r.height() / 2;
		return     r.contains ( hitX, hitY )
				|| r.contains ( hitX, hitY - dy )
				|| r.contains ( hitX, hitY + dy );
	}
}
