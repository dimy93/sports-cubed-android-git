package com.cube.osmdroid.overlays;

import org.osmdroid.bonuspack.overlays.ExtendedOverlayItem;
import org.osmdroid.bonuspack.overlays.InfoWindow;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.text.Spanned;

import com.cube.AndroidUI.R;
import com.cube.UI.DisplayPlaceActivity;
import com.cube.osmdroid.markers.CustomInfoWindow;
import com.cube.osmdroid.markers.CustomInfoWindow.BubbleListener;

public class CustomMarker extends ExtendedOverlayItem implements BubbleListener {
	
	private static int colorInformation = 0xff66cc00;
	private static int colorNoInformation = 0xffffaf0a;
	private static Drawable fromResource = null;
	private Context context;
	private String activities;
	private String address;
	private Spanned comment;
	private String postcode;
	private Spanned email;
	private String telephone;
	private Spanned website;

	public CustomMarker( Spanned name, Spanned address, String postcode, String telephone, 
			             Spanned email, Spanned website, Spanned comment, String activities, 
			            boolean information, GeoPoint aGeoPoint, Context context ) 
	{
		super(name.toString(), "", aGeoPoint, context);
		
		this.address = address == null ? null : address.toString().replace('\n', ' ');
		this.address = this.address == null || postcode == null ? this.address : this.address.replace(postcode.toString(), "");

		// Set up picture
		if ( fromResource == null )
		{
			fromResource = context.getResources().getDrawable(R.drawable.marker);
		}
		Drawable d = fromResource.getConstantState().newDrawable().mutate();
		if ( information )
		{
			d.setColorFilter( colorInformation, Mode.MULTIPLY );
		}
		else
		{
			d.setColorFilter( colorNoInformation, Mode.MULTIPLY );
		}
		super.setMarkerHotspot ( ExtendedOverlayItem.HotspotPlace.BOTTOM_CENTER );
		super.setMarker(d);
		
		//Set up address
		String description = "Address : " + ( this.address == null ? "" : this.address ) + "\nPostcode : " + ( postcode == null ? "" : postcode );
		super.setDescription(description);
		
		this.context = context;
		this.activities = activities;
		this.comment = comment;
		this.postcode = postcode;
		this.email = email;
		this.telephone = telephone;
		this.website = website;
	}	

	@Override
	public void showBubble(InfoWindow bubble, MapView mapView,
			boolean panIntoView) {
		super.showBubble(bubble, mapView, panIntoView);
		if ( bubble instanceof CustomInfoWindow )
		{
			((CustomInfoWindow) bubble).addListener(this);
		}
	}

	@Override
	public void onMoreInfoClicked(CustomInfoWindow info, Object item) {
		
		Intent intent = new Intent( context, DisplayPlaceActivity.class );
		
		intent.putExtra("name", getTitle());
		intent.putExtra("email", email);
		intent.putExtra("phone", telephone);
		intent.putExtra("description", comment);
		intent.putExtra("address", address);
		intent.putExtra("postcode", postcode);
		intent.putExtra("activities", activities);
		intent.putExtra("website", website);
		
		context.startActivity(intent);
		if ( context instanceof Activity ){
			((Activity)context).overridePendingTransition(R.anim.bounce, R.anim.nothing);	
		}
		
	}

	@Override
	public void onClosed(CustomInfoWindow info, Object item) {
		info.removeListener(this);
	}

	@Override
	public void onOpened(CustomInfoWindow info, Object item) {
		// TODO Auto-generated method stub
		
	}
}
