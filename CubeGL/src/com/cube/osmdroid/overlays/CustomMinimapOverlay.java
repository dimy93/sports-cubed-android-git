package com.cube.osmdroid.overlays;

import org.osmdroid.api.IGeoPoint;
import org.osmdroid.tileprovider.MapTileProviderBase;
import org.osmdroid.util.TileSystem;
import org.osmdroid.views.MapView;
import org.osmdroid.views.MapView.Projection;
import org.osmdroid.views.overlay.ItemizedOverlay;
import org.osmdroid.views.overlay.MinimapOverlay;
import org.osmdroid.views.safecanvas.ISafeCanvas;
import org.osmdroid.views.safecanvas.SafePaint;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint.Style;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region.Op;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.view.MotionEvent;

import com.cube.AndroidUI.R;

public class CustomMinimapOverlay extends MinimapOverlay{
	
	private final Context mContext;
	protected static Drawable frame;
	private final Rect minimapZoomed = new Rect();

	public CustomMinimapOverlay(Context pContext,
			Handler pTileRequestCompleteHandler,
			MapTileProviderBase pTileProvider,
			ItemizedOverlay<?> pItemizedOverlay, int pZoomDifference) {
		super(pContext, pTileRequestCompleteHandler, pTileProvider, pItemizedOverlay,
				pZoomDifference);
		mContext = pContext;
		createRectangleFromXML();
	}

	public CustomMinimapOverlay(Context pContext,
			Handler pTileRequestCompleteHandler,
			MapTileProviderBase pTileProvider,
			ItemizedOverlay<?> pItemizedOverlay) {
		super(pContext, pTileRequestCompleteHandler, pTileProvider, pItemizedOverlay);
		mContext = pContext;
		createRectangleFromXML();
	}
	
	public CustomMinimapOverlay(Context pContext,
			Handler pTileRequestCompleteHandler,
			MapTileProviderBase pTileProvider) {
		super(pContext, pTileRequestCompleteHandler, pTileProvider);
		mContext = pContext;
		createRectangleFromXML();
	}

	public CustomMinimapOverlay(Context pContext,
			Handler pTileRequestCompleteHandler,
			ItemizedOverlay<?> pItemizedOverlay) {
		super(pContext, pTileRequestCompleteHandler, pItemizedOverlay);
		mContext = pContext;
		createRectangleFromXML();
	}

	public CustomMinimapOverlay(Context pContext,
			Handler pTileRequestCompleteHandler) {
		super(pContext, pTileRequestCompleteHandler);
		mContext = pContext;
		createRectangleFromXML();
	}
	
	private boolean createRectangleFromXML()
	{
		if ( frame != null )
			return false;
		
		//Save the result
		frame = mContext.getResources().getDrawable(R.drawable.rectangle);
		return true;
	}
	
	private Bitmap createRectangleFromXML(Rect r)
	{
		//Create canvas
		int h = r.bottom-r.top;
		int w = r.right-r.left; 
		Bitmap.Config config = Bitmap.Config.ARGB_8888;
		Bitmap bitmap = Bitmap.createBitmap(w, h, config);
		Canvas canvas = new Canvas(bitmap);
		
		//Creating image
		frame.setBounds(0,0,w,h);
		frame.draw(canvas);
		
		//Save the result
		return bitmap;
	}
	
	@Override
	protected void drawSafe(final ISafeCanvas canvas, final MapView pOsmv, final boolean shadow) 
	{
		if (pOsmv.isAnimating()) 
		{
			return;
		}
		
		if (shadow) {
			return;
		}
		
		super.drawSafe(canvas, pOsmv, shadow);

		final Point NE = getPixelCoordinates(pOsmv.getProjection().getNorthEast(),pOsmv);
		final Point SW = getPixelCoordinates(pOsmv.getProjection().getSouthWest(),pOsmv);
		
		minimapZoomed.top = NE.y - 2;
		minimapZoomed.bottom = SW.y + 2;
		minimapZoomed.left = SW.x - 2;
		minimapZoomed.right = NE.x + 2;
		
		Bitmap image = createRectangleFromXML(minimapZoomed);
		
		canvas.getSafeCanvas().drawBitmap(image, null, minimapZoomed, null);
		drawRoundedEdge( canvas, pOsmv );
	}
	
	protected Point getPixelCoordinates( final IGeoPoint point, final MapView pOsmv )
	{
		final Projection pj = pOsmv.getProjection();
		final Rect screenRect = pj.getScreenRect();
		final Point mCurScreenCoords = new Point();
		
		pj.toMapPixels( point, mCurScreenCoords );
		final int centerMapX = screenRect.centerX();
		final int centerMapY = screenRect.centerY();
		final int centerMinimapX = mMiniMapCanvasRect.centerX();
		final int centerMinimapY = mMiniMapCanvasRect.centerY();
		final int offsetX = mCurScreenCoords.x - centerMapX;
		final int x = ( offsetX >> getZoomDifference(pOsmv.getZoomLevel()) ) + centerMinimapX;
		final int offsetY = mCurScreenCoords.y - centerMapY;
		final int y = ( offsetY >> getZoomDifference(pOsmv.getZoomLevel()) ) + centerMinimapY;
		return new Point(x,y);
	}
	
	protected Rect getMinimapBounds( MapView mapView ){
		Rect bounds = new Rect();
		bounds.right = mapView.getWidth() - getPadding();
		bounds.bottom = mapView.getHeight() - getPadding();
		bounds.left = bounds.right - getWidth();
		bounds.top = bounds.bottom - getHeight();
		return bounds;
	}
	
	protected void drawRoundedEdge( final ISafeCanvas pC, MapView mapView ){
		SafePaint p = new SafePaint();
		p.setAntiAlias(true);
		p.setColor(0xff000000);
		p.setStyle(Style.STROKE);
		p.setStrokeWidth(6f);
		// Draw a solid background where the minimap will be drawn with a 2
		// pixel inset
		pC.save();
		Rect b = pC.getClipBounds();
		int outer = 10;
		b.set(b.left-outer, b.top-outer, b.right+outer, b.bottom+outer);
		pC.clipRect(b, Op.REPLACE);
		mMiniMapCanvasRect.set(mMiniMapCanvasRect.left-2,mMiniMapCanvasRect.top-2,mMiniMapCanvasRect.right+2,mMiniMapCanvasRect.bottom+2);
		pC.drawRoundRect(mMiniMapCanvasRect,10,10, p);
		RectF rf = new RectF();
		rf.set(mMiniMapCanvasRect.left+0.05f,mMiniMapCanvasRect.top+0.05f,mMiniMapCanvasRect.right-0.05f,mMiniMapCanvasRect.bottom-0.05f);
		p.setColor(0xffffffff);
		p.setStrokeWidth(4f);
		pC.drawRoundRect(rf,10,10, p);
		pC.restore();
	}
	
	@Override
	public boolean onSingleTapConfirmed(MotionEvent ev, MapView mapView) {
		Rect bounds = getMinimapBounds(mapView);
		
		if ( bounds.contains((int)ev.getX(), (int)ev.getY()) )
		{
			IGeoPoint coord = getTouchPoint(ev.getX()-bounds.left, ev.getY()-bounds.top,mapView);
			mapView.getController().setCenter(coord);
			return true;
		}
		return false;
	}
	
	private IGeoPoint getTouchPoint ( float x, float y, MapView pOsmv ){
		int map_zoom = pOsmv.getZoomLevel();
		int zoom_dif = getZoomDifference(map_zoom);
		int x_real = ((int)x);
		int y_real = ((int)y);
		int minimap_zoom = map_zoom - zoom_dif;
		
		final Projection projection = pOsmv.getProjection();
		int map_worldSize_2 = TileSystem.MapSize(map_zoom) / 2;
		Rect mapRect = new Rect();
		mapRect.set(projection.getScreenRect());
		mapRect.offset(map_worldSize_2, map_worldSize_2);
		Point mapCenter = new Point( mapRect.centerX(),mapRect.centerY() );
		Point minimapCenter = new Point ( mapCenter.x >> zoom_dif, mapCenter.y >> zoom_dif );
		Rect miniMapScreenRect = new Rect();
		miniMapScreenRect.left = minimapCenter.x - ( getWidth()/2 );
		miniMapScreenRect.right = minimapCenter.x + ( getWidth()/2 );
		miniMapScreenRect.top = minimapCenter.y - ( getHeight()/2 );
		miniMapScreenRect.bottom = minimapCenter.y + ( getHeight()/2 );
		return TileSystem.PixelXYToLatLong(miniMapScreenRect.left +x_real,
				miniMapScreenRect.top +y_real ,minimap_zoom, null);
	}
}
