/**
 * Created on August 12, 2012
 * 
 * @author Melle Sieswerda
 */

package com.cube.osmdroid.tileprovider.tilesource;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;

import org.osmdroid.ResourceProxy;
import org.osmdroid.ResourceProxy.string;
import org.osmdroid.tileprovider.ExpirableBitmapDrawable;
import org.osmdroid.tileprovider.MapTile;
import org.osmdroid.tileprovider.tilesource.BitmapTileSourceBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;

public class MBTileSource extends BitmapTileSourceBase
{
	
	// Log log log log ...
	private static final Logger logger = LoggerFactory.getLogger ( MBTileSource.class );
	
	// Database related fields
	public final static String TABLE_TILES = "tiles";
	public final static String COL_TILES_ZOOM_LEVEL = "zoom_level";
	public final static String COL_TILES_TILE_COLUMN = "tile_column";
	public final static String COL_TILES_TILE_ROW = "tile_row";
	public final static String COL_TILES_TILE_DATA = "tile_data";
	
	protected SQLiteDatabase database;
	protected File archive;
	
	// Reasonable defaults ..
	public static final int minZoom = 8;
	public static final int maxZoom = 17;
	public static final int tileSizePixels = 256;
	
	// Required for the superclass
	public static final string resourceId = ResourceProxy.string.offline_mode;
	
	/**
	 * The reason this constructor is protected is because all parameters,
	 * except file should be determined from the archive file. Therefore a
	 * factory method is necessary.
	 * 
	 * @param minZoom
	 * @param maxZoom
	 * @param tileSizePixels
	 * @param file
	 */
	protected MBTileSource ( int minZoom, int maxZoom, int tileSizePixels, File file,
			SQLiteDatabase db )
	{
		super ( "MBTiles", resourceId, minZoom, maxZoom, tileSizePixels, ".png" );
		
		archive = file;
		database = db;
	}
	
	private static SQLiteDatabase dbOpen( File file )
	{
		int flags = SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READONLY;
		return SQLiteDatabase.openDatabase ( file.getAbsolutePath ( ), null, flags );
	}
	
	private static int getMinZoom( SQLiteDatabase db )
	{
		int value = getInt ( db, "SELECT MIN(zoom_level) FROM tiles;" );
		return value > -1 ? value : minZoom;
	}
	
	private static int getMaxZoom( SQLiteDatabase db )
	{
		int value = getInt ( db, "SELECT MAX(zoom_level) FROM tiles;" );
		return value > -1 ? value : maxZoom;
	}
	
	private static int getTileSizeFromDb( SQLiteDatabase db )
	{
		InputStream is = null;
		int tileSize = tileSizePixels;
        Cursor cursor = db.rawQuery("SELECT tile_data FROM tiles LIMIT 0,1",
                                    new String[] {});
 
        if (cursor.getCount() != 0) {
            cursor.moveToFirst();
            is = new ByteArrayInputStream(cursor.getBlob(0));
 
            Bitmap bitmap = BitmapFactory.decodeStream(is);
            tileSize = bitmap.getHeight();
            logger.debug(String.format("Found a tile size of %d", tileSize));
        }
 
        cursor.close();
        return tileSize;
	}
	
	
	/**
	 * Creates a new MBTileSource from file.
	 * 
	 * Parameters minZoom, maxZoom en tileSizePixels are obtained from the
	 * database. If they cannot be obtained from the DB, the default values as
	 * defined by this class are used.
	 * 
	 * @param file
	 * @return
	 */
	public static MBTileSource createFromFile ( int tileSize, File file )
	{
		SQLiteDatabase db = dbOpen( file );
		int minZoomLevel = getMinZoom(db);
		int maxZoomLevel = getMaxZoom(db);
		if ( tileSize == -1 )
		{
			tileSize = getTileSizeFromDb(db);
		}
		System.gc ( );
		return new MBTileSource ( minZoomLevel, maxZoomLevel, tileSize, file, db );
	}
	
	protected static int getInt ( SQLiteDatabase db, String sql )
	{
		Cursor cursor = db.rawQuery ( sql, new String [ ] { } );
		int value = -1;
		
		if ( cursor.getCount ( ) != 0 )
		{
			cursor.moveToFirst ( );
			value = cursor.getInt ( 0 );
			logger.debug ( String.format ( "Found a minimum zoomlevel of %d", value ) );
		}
		
		cursor.close ( );
		return value;
	}
	
	public Drawable getDrawable ( MapTile pTile )
	{
		
		try
		{
			Drawable ret = null;
			final String [ ] tile = { COL_TILES_TILE_DATA };
			final String [ ] xyz = {
					Integer.toString ( pTile.getX ( ) ),
					Double.toString ( Math.pow ( 2, pTile.getZoomLevel ( ) ) - pTile.getY ( ) - 1 ),
					Integer.toString ( pTile.getZoomLevel ( ) ) };
			
			final Cursor cur = database.query ( TABLE_TILES, tile,
					"tile_column=? and tile_row=? and zoom_level=?", xyz, null, null, null );
			
			if ( cur.getCount ( ) != 0 )
			{
				cur.moveToFirst ( );
				try
				{
					byte [ ] blob = cur.getBlob ( 0 );
					BitmapFactory.Options opt = new BitmapFactory.Options ( );
					opt.inPreferredConfig = Bitmap.Config.RGB_565;
					Bitmap b = BitmapFactory.decodeByteArray ( blob, 0, blob.length, opt );
					ret = new ExpirableBitmapDrawable ( b );
				}
				catch ( final OutOfMemoryError e )
				{
					logger.error ( "OutOfMemoryError loading bitmap: " + pTile );
					System.gc ( );
				}
			}
			
			cur.close ( );
			
			if ( ret != null )
			{
				return ret;
			}
			
		}
		catch ( final Throwable e )
		{
			logger.warn ( "Error getting db stream: " + pTile, e );
		}
		
		return null;
		
	}
	
}
