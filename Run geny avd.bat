@ECHO Off
REM "Samsung Galaxy S3 - 4.3 - API 18 - 720x1280" {553609f7-bf71-4d36-a77a-6c2d30c6087d}
REM "Samsung Galaxy Note - 2.3.7 - API 10 - 800x1280" {12419b07-6128-41f8-8dcc-8a87bff8882d}
REM "Google Nexus 7 - 4.3 - API 18 - 800x1280" {c8e6db56-50c1-407e-837b-9aa8431ab73c}
REM "Google Nexus One - 2.3.7 - API 10 - 480x800" {b5df4472-26d8-41ff-8a38-84c830c03e8e}

setlocal ENABLEDELAYEDEXPANSION
call adb start-server

set /a devices = -1 
for /f "delims=" %%i in ('adb devices') do set /a devices=!devices!+1

@ECHO !devices!
if !devices!==0 (
   start "" "C:\Program Files\Genymobile\Genymotion\player" --vm-name 553609f7-bf71-4d36-a77a-6c2d30c6087d
   :while1
   if !devices!==0 (
     set /a devices = -1
     for /f "delims=" %%i in ('adb devices') do set /a devices=!devices!+1
     goto :while1
   )
   call :waitForBootFunction "getprop dev.bootcomplete" 1
   call :waitForBootFunction "getprop sys.boot_completed" 1
   call :waitForBootFunction "pm path android" package
)
echo here
exit
pause
goto:eof


:waitForBootFunction
echo [emulator-$CONSOLE_PORT] Checking %~1...
:waitFor1
for /f "delims=" %%b in ('start /b cmd /c adb shell %~1 2') do set result=%%b
set result|FINDSTR /b "result="|FINDSTR /i %~2 >nul
IF ERRORLEVEL 1 (
  goto :waitFor1
)
echo [emulator-$CONSOLE_PORT] All boot properties succesful
goto:eof