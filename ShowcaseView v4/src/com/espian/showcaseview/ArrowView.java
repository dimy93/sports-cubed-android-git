package com.espian.showcaseview;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class ArrowView extends LinearLayout {

	public ArrowView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}
	
	public void setColor( int color ){
		for( int i = 0; i < getChildCount(); i++ )
		{
			View child = getChildAt(i);
			Drawable background = child.getBackground();
			if ( child instanceof ImageView )
			{
				ImageView image = (ImageView) child;
				image.clearColorFilter();
				image.setColorFilter(color, PorterDuff.Mode.MULTIPLY);
			}
			else if ( background != null )
			{
				background.clearColorFilter();
				background.setColorFilter(color, PorterDuff.Mode.MULTIPLY);
			}
		}
	}
}
