package com.espian.showcaseview;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParserException;

import com.github.espiandev.showcaseview.R;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.Animator.AnimatorListener;
import com.nineoldandroids.view.ViewHelper;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.XmlResourceParser;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.widget.ImageView;


public class HandyView extends ImageView
{
	
	protected static final int[] HANDY_NONE_STATE_SET = { R.attr.handy_none };
	protected static final int[] HANDY_UP_STATE_SET = { R.attr.handy_up };
	protected static final int[] HANDY_DOWN_STATE_SET = { R.attr.handy_down };
	protected static final int[] HANDY_LEFT_STATE_SET = { R.attr.handy_left };
	protected static final int[] HANDY_RIGHT_STATE_SET = { R.attr.handy_right };
	protected static final int[] HANDY_ROTATE_LEFT_STATE_SET = { R.attr.handy_rotate_left };
	protected static final int[] HANDY_ROTATE_RIGHT_STATE_SET = { R.attr.handy_rotate_right };
	protected static final int[] HANDY_PINCH_IN_STATE_SET = { R.attr.handy_pinch_in };
	protected static final int[] HANDY_PINCH_OUT_STATE_SET = { R.attr.handy_pinch_out };
	
	protected static final int HANDY_NONE = -1;
	protected static final int HANDY_UP = 0;
	protected static final int HANDY_DOWN = 1;
	protected static final int HANDY_LEFT = 2;
	protected static final int HANDY_RIGHT = 3;
	protected static final int HANDY_ROTATE_LEFT = 4;
	protected static final int HANDY_ROTATE_RIGHT = 5;
	protected static final int HANDY_PINCH_IN = 6;
	protected static final int HANDY_PINCH_OUT = 7;
	
	protected SparseArray<Float[]> stateAnims = new SparseArray<Float[]>();
	
	private ArrayList<Animator> anims = new ArrayList<Animator>();
	
	protected int mHandyState = HANDY_NONE;

	public HandyView(Context context, AttributeSet attrs) {
		super(context, attrs);
		XmlResourceParser parser = getResources().getXml(R.xml.handy_centers);
		parser.getAttributeCount();
		try {
			while ( parser.next() != XmlResourceParser.END_DOCUMENT)
			{
				
				if (parser.getEventType() == XmlResourceParser.START_TAG && parser.getName().equals("item"))
				{
					Integer state = getState( parser.getAttributeValue(null, "state") );
					if ( state == null )
						continue;
					Float x = parseFraction(parser,"x");
					Float y = parseFraction(parser,"y");
					Float rotation = parseRotation(parser,"rotation");
					stateAnims.append(state, new Float[]{x,y,rotation});
				}
			}
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private Integer getState( String state )
	{
		if ( state == null )
			return null;
		for ( Field f : HandyView.class.getDeclaredFields() )
		{
			try 
			{
				if ( !f.getType().equals(int.class) )
					continue;
				f.setAccessible(true);
				String name = f.getName();
				if ( name.indexOf("HANDY_") == -1 )
					continue;
				name = f.getName().replace("HANDY_", "");
				if ( name.equals( state.toUpperCase() ) )
				{
					return f.getInt(this);
				}
			} 
			catch (Exception e) {
				//Do nothing
			}	
		}
		return null;
	}
	
	private Float parseFraction(XmlResourceParser parser, String property) {
		try {
			String x = parser.getAttributeValue(null, property);
			if (x.indexOf("%") == -1)
				x = null;
			else
				x = x.replace("%", "");
			return Float.parseFloat(x) / 100f;
		} catch (Exception e) {
			return null;
		}
	}
	
	private Float parseRotation(XmlResourceParser parser, String property) {
		try {
			String x = parser.getAttributeValue(null, property);
			if (x.indexOf("deg") == -1)
				x = null;
			else
				x = x.replace("deg", "");
			return Float.parseFloat(x);
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
    public int[] onCreateDrawableState(int extraSpace) {
        // Ask the parent to add its default states.
        final int[] drawableState = super.onCreateDrawableState(extraSpace + 1);
        
        //Add animation state
        switch(mHandyState)
        {
        case HANDY_UP:
        	mergeDrawableStates(drawableState, HANDY_UP_STATE_SET);
			break;
		case HANDY_DOWN:
			mergeDrawableStates(drawableState, HANDY_DOWN_STATE_SET);
			break;
		case HANDY_LEFT:
			mergeDrawableStates(drawableState, HANDY_LEFT_STATE_SET);
			break;
		case HANDY_RIGHT:
			mergeDrawableStates(drawableState, HANDY_RIGHT_STATE_SET);
			break;
		case HANDY_ROTATE_LEFT:
			mergeDrawableStates(drawableState, HANDY_ROTATE_LEFT_STATE_SET);
			break;
		case HANDY_ROTATE_RIGHT:
			mergeDrawableStates(drawableState, HANDY_ROTATE_RIGHT_STATE_SET);
			break;
		case HANDY_PINCH_IN:
			mergeDrawableStates(drawableState, HANDY_PINCH_IN_STATE_SET);
			break;
		case HANDY_PINCH_OUT:
			mergeDrawableStates(drawableState, HANDY_PINCH_OUT_STATE_SET);
			break;
		case HANDY_NONE:
			mergeDrawableStates(drawableState, HANDY_NONE_STATE_SET);
			break;
		default:
			break;
        }
 
        // Return the new drawable state.
        return drawableState;
    }
	
	protected void setHandy(int aHandy) {
		setHandy(aHandy,true);
	}
	
	protected void setHandy(int aHandy, boolean reset) {
        // If we flip the current state of private mode, record the value
        // and inform Android to refresh the drawable state.
        // This will in turn invalidate() the view.
        if (reset)
        {
        	try
        	{
        		for (Animator a : anims)
        		{
        			a.cancel();
        		}
        		anims.clear();
        	}
        	catch(Exception e)
        	{
        	}
        }
        resetStateAnim();
        setStateAnim(aHandy);
        if (aHandy != mHandyState) {
        	mHandyState = aHandy;
            refreshDrawableState();
        }
   }
	
	public void addAnimation(final Animator a)
	{
		a.addListener(new AnimatorListener() {
			
			@Override
			public void onAnimationStart(Animator arg0) {
			}
			
			@Override
			public void onAnimationRepeat(Animator arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onAnimationEnd(Animator arg0) {
				anims.remove(arg0);
			}
			
			@Override
			public void onAnimationCancel(Animator arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		anims.add(a);
	}
	
	private void resetStateAnim()
	{
		ViewHelper.setPivotX(this, 0);
		ViewHelper.setPivotY(this, 0);
		ViewHelper.setRotation(this, 0);
		ViewHelper.setTranslationX(this, 0);
		ViewHelper.setTranslationY(this, 0);
		ViewHelper.setScaleX(this, 1f);
		ViewHelper.setScaleY(this, 1f);
		ViewHelper.setX(this, 0f);
		ViewHelper.setY(this, 0f);
	}
	
	private void setStateAnim( int state )
	{
		
        Float[] values = stateAnims.get(state);
		if (values == null)
			return;
        this.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
        if (values[0] != null)
        {
        	ViewHelper.setTranslationX(this, -values[0]*getMeasuredWidth());
        	ViewHelper.setPivotX(this, values[0]*getMeasuredWidth());
        }
        if (values[1] != null)
        {
        	ViewHelper.setTranslationY(this, -values[1]*getMeasuredHeight());
        	ViewHelper.setPivotY(this, values[1]*getMeasuredHeight());
        }
        if (values[2] != null)
        {
        	ViewHelper.setRotation(this, values[2]);
        }
	}
	
	public int getTransformedX( int x )
	{
		Float[] values = stateAnims.get(mHandyState);
		if (values[0] == null)
        {
			return x;
        }
		float x_new = x-values[0]*getMeasuredWidth();
		return (int) x_new;
	}
	
	public int getTransformedY( int y )
	{
		Float[] values = stateAnims.get(mHandyState);
		if (values[1] == null)
        {
			return y;
        }
		float y_new = y-values[0]*getMeasuredHeight();
		return (int) y_new;
	}
	
	public float getTransformedRotation( float degrees )
	{
		Float[] values = stateAnims.get(mHandyState);
		if (values[2] == null)
        {
			return degrees;
        }
		float rotation = degrees + values[2];
		return rotation;
	}
	
	public void setColor( int color )
	{
		clearColorFilter();
		setColorFilter(color, PorterDuff.Mode.MULTIPLY);
	}
}