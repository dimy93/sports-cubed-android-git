package com.espian.showcaseview;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import com.espian.showcaseview.drawing.ClingDrawerImpl;
import com.espian.showcaseview.drawing.TextDrawerImpl;
import com.espian.showcaseview.targets.ViewTarget;
import com.github.espiandev.showcaseview.R;
import com.nineoldandroids.view.ViewHelper;

public class ShowcaseFromXML extends ShowcaseView {

	protected String mDescription;
	protected String mTitle;
	protected int mColor;
	protected ViewTarget vt = null;
	
	public static final int ANIMATION_NONE = -1;
	public static final int ANIMATION_UP = 0;
	public static final int ANIMATION_DOWN = 1;
	public static final int ANIMATION_LEFT = 2;
	public static final int ANIMATION_RIGHT = 3;
	public static final int ANIMATION_ROTATE_LEFT = 4;
	public static final int ANIMATION_ROTATE_RIGHT = 5;
	public static final int ANIMATION_ZOOM_IN = 6;
	public static final int ANIMATION_ZOOM_OUT = 7;
	public static final int TAP = 8;
	
	protected int mAnimationFlag;

	public float ANIMATION_DISTANCE = 150;
	public float ANIMATION_ROTATION_ANGLE = 360;

	protected ShowcaseFromXML(Context context) {
		super(context);
	}

	public ShowcaseFromXML(Context context, AttributeSet attrs) {
		super(context, attrs, R.styleable.CustomTheme_showcaseViewStyle);
		parseAttr(context, attrs);
	}

	public ShowcaseFromXML(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		parseAttr(context, attrs);
	}

	public void setColor(int color) {
		((ClingDrawerImpl) this.mShowcaseDrawer).setColor(color);
		mHandy.setColor(color|0xff000000);
		
		mArrowLeft.setColor(color|0xff000000);
		mArrowRight.setColor(color|0xff000000);

		int alpha = color & 0xFF000000;
		alpha = alpha >>> 24;
		int halfAlpha = alpha / 2;
		halfAlpha = halfAlpha << 24;

		int halfAlphaColor = color & 0x00FFFFFF;
		halfAlphaColor |= halfAlpha;
		Drawable bgDrawable = mEndButton.getBackground();
		
		mEndButton.getBackground().clearColorFilter();
		float r = Color.red(color)/256f;
		float b = Color.blue(color)/256f;
		b = 0.6f*b;
		float g = Color.green(color)/256f;
		ColorMatrix cmDesat = new ColorMatrix();
		cmDesat.setSaturation( 0 );
		ColorMatrix cm = new ColorMatrix();
		cm.set( new float[]{
		  r, 0, 0, 0, 0, 
		  0, g, 0, 0, 0, 
		  0, 0, b, 0, 0, 
		  0, 0, 0, 1, 0
		} );
		cmDesat.postConcat( cm );
		bgDrawable.setColorFilter( new ColorMatrixColorFilter( cmDesat ) );

		mBackgroundColor = halfAlphaColor;
		((TextDrawerImpl) mTextDrawer).setTitleColor(getContext(),color|0xff000000);
		mColor = color;
	}

	public void setTitle(String title) {
		this.mTextDrawer.setTitle(title);
		mAlteredText = true;
		mTitle = title;
		invalidate();
	}

	public void setDescription(String desc) {
		this.mTextDrawer.setDetails(desc);
		mAlteredText = true;
		mDescription = desc;
		invalidate();
	}

	public void setAnimation(int type) {
		mAnimationFlag = type;
	}

	@Override
	public void show() {
		setColor(mColor);
		if ( vt != null )
			setShowcase(vt);
		this.invalidate();
		super.show();
	}
	
	@Override
	protected void onTargetRedrawn( )
	{
		showAnim();
    }
	
	@Override
	public void hide() {
		setShowcaseNoView();
		super.hide();
	}

	private void parseAttr(Context c, AttributeSet attrs) {
		TypedArray arr = getContext().obtainStyledAttributes(attrs,
				R.styleable.ShowcaseFromXML);

		int defColor = c.getResources().getColor(
				R.color.honeycomb_blue_transparent);
		int color = arr.getColor(R.styleable.ShowcaseFromXML_background,
				defColor);
		setColor(color);

		String title = arr.getString(R.styleable.ShowcaseFromXML_title);
		title = title == null ? "Title" : title;

		String desc = arr.getString(R.styleable.ShowcaseFromXML_description);
		desc = desc == null ? "Description" : desc;

		int none = 0;
		int ref = arr.getResourceId(R.styleable.ShowcaseFromXML_target, none);

		int target_x = arr.getDimensionPixelOffset(
				R.styleable.ShowcaseFromXML_target_x, Integer.MIN_VALUE);
		int target_y = arr.getDimensionPixelOffset(
				R.styleable.ShowcaseFromXML_target_y, Integer.MIN_VALUE);

		int anim = arr.getInt(R.styleable.ShowcaseFromXML_animation,
				ANIMATION_NONE);

		setColor(color);
		setTitle(title);
		setDescription(desc);
		setAnimation(anim);

		if (ref != none) {
			vt = new ViewTarget(ref, (Activity) c);
		} else {
			if (target_x != Integer.MIN_VALUE && target_y != Integer.MIN_VALUE) {
				setShowcasePosition(target_x, target_y);
			} else {
				setShowcaseNoView();
			}
		}

		arr.recycle();
	}

	private void showAnim() {
		switch (mAnimationFlag) {
		case ANIMATION_UP:
			animateMoveGesture(0, 0, 0, -ANIMATION_DISTANCE);
			break;
		case ANIMATION_DOWN:
			animateMoveGesture(0, 0, 0, ANIMATION_DISTANCE);
			break;
		case ANIMATION_LEFT:
			animateMoveGesture(0, 0, -ANIMATION_DISTANCE, 0);
			break;
		case ANIMATION_RIGHT:
			animateMoveGesture(0, 0, ANIMATION_DISTANCE, 0);
			break;
		case ANIMATION_ROTATE_LEFT:
			animateRotationGesture(0, -ANIMATION_ROTATION_ANGLE);
			break;
		case ANIMATION_ROTATE_RIGHT:
			animateRotationGesture(0, ANIMATION_ROTATION_ANGLE);
			break;
		case ANIMATION_ZOOM_OUT:
			animateZoomOutGesture(ANIMATION_DISTANCE);
			break;
		case ANIMATION_ZOOM_IN:
			animateZoomInGesture(ANIMATION_DISTANCE);
			break;
		case TAP:
			pointTo(0,0);
			mHandy.invalidate();
			break;
		default:
			break;
		}

	}

}
