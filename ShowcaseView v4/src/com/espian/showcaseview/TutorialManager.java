package com.espian.showcaseview;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.espiandev.showcaseview.R;

public final class TutorialManager extends View implements OnShowcaseEventListener {
	private ArrayList<Integer> mShowcases = new ArrayList<Integer>();
	private int mInflatedId;
	private WeakReference<ShowcaseView> mInflatedViewRef;
	private ArrayList<OnShowcaseInflated> mInflateListeners = new ArrayList<OnShowcaseInflated>();

	public TutorialManager(Context context) {
		super(context);
		initialize(context);
	}

	public TutorialManager(Context context, int showcasesResource) {
		super(context);
		extractShowcaseResources(showcasesResource);
		initialize(context);
	}

	public TutorialManager(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public TutorialManager(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.TutorialManager, defStyle, 0);
		try {
			mInflatedId = a.getResourceId(
					R.styleable.TutorialManager_inflated_id, NO_ID);
			int showcases = a.getResourceId(
					R.styleable.TutorialManager_showcases, 0);
			extractShowcaseResources(showcases);
		} finally {
			a.recycle();
			initialize(context);
		}
	}

	private void initialize(Context context) {
		setVisibility(View.INVISIBLE);
		setWillNotDraw(true);
	}

	private void extractShowcaseResources(int showcasesResource) {
		TypedArray ar = getResources().obtainTypedArray(showcasesResource);

		int len = ar.length();
		for (int i = 0; i < len; i++) {
			int id = ar.getResourceId(i, 0);
			mShowcases.add(id);
		}
		ar.recycle();
	}

	private ViewGroup getHierarchyTop() {
		final Context context = getContext();
		if (!(context instanceof Activity)) {
			throw new IllegalArgumentException(
					"TutorialManager's Context must be Activity");
		}

		final Activity activity = (Activity) context;
		final View viewParent = activity.getWindow().getDecorView()
				.findViewById(android.R.id.content);
		if (viewParent == null || !(viewParent instanceof ViewGroup)) {
			throw new IllegalStateException(
					"The View's activity's root must have a non-null ViewGroup viewParent");
		}

		return (ViewGroup) viewParent;
	}

	/**
	 * Returns the id taken by the inflated view. If the inflated id is
	 * {@link View#NO_ID}, the inflated view keeps its original id.
	 * 
	 * @return A positive integer used to identify the inflated view or
	 *         {@link #NO_ID} if the inflated view should keep its id.
	 * 
	 * @see #setInflatedId(int)
	 * @attr ref android.R.styleable#ViewStub_inflatedId
	 */
	public int getInflatedId() {
		return mInflatedId;
	}
	
	public void addView( int id )
	{
		mShowcases.add(id);
	}
	
	public void addView( int index, int res )
	{
		mShowcases.add(index,res);
	}
	
	public void removeView( int res )
	{
		mShowcases.remove((Object)res);
	}

	/**
	 * Defines the id taken by the inflated view. If the inflated id is
	 * {@link View#NO_ID}, the inflated view keeps its original id.
	 * 
	 * @param inflatedId
	 *            A positive integer used to identify the inflated view or
	 *            {@link #NO_ID} if the inflated view should keep its id.
	 * 
	 * @see #getInflatedId()
	 * @attr ref android.R.styleable#ViewStub_inflatedId
	 */
	public void setInflatedId(int inflatedId) {
		mInflatedId = inflatedId;
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		setMeasuredDimension(0, 0);
	}

	@Override
	public void draw(Canvas canvas) {
	}

	@Override
	protected void dispatchDraw(Canvas canvas) {
	}

	/**
	 * When visibility is set to {@link #VISIBLE} or {@link #INVISIBLE},
	 * {@link #inflate()} is invoked and this StubbedView is replaced in its
	 * parent by the inflated layout resource.
	 * 
	 * @param visibility
	 *            One of {@link #VISIBLE}, {@link #INVISIBLE}, or {@link #GONE}.
	 * 
	 * @see #inflate()
	 */
	@Override
	public void setVisibility(int visibility) {
		ShowcaseView v = null;
		if (mInflatedViewRef != null) {
			v = mInflatedViewRef.get();
			if (v == null || v.getVisibility() == View.GONE) {
				v = null;
			}
		}

		switch (visibility) {
		case (VISIBLE):
			if (v == null) {
				inflate();
			} else {
				v.show();
			}
			break;
		case (INVISIBLE):
			if (v != null) {
				v.hide();
			}
			break;
		case (GONE):
			if (v != null) {
				v.hide();
				v.setVisibility(View.GONE);
			}
			break;
		default:

		}
		super.setVisibility(visibility);
	}

	private void inflate() {
		if( mShowcases.isEmpty() )
		{
			this.setVisibility(View.INVISIBLE);
			return;
		}
		final ViewGroup parent = (ViewGroup) getHierarchyTop();
		final int showcaseId = mShowcases.get(0);
		if (showcaseId == 0) {
			throw new IllegalArgumentException(
					"Tutorial Manager tried to display Showcase id that does not exists");
		}
		final LayoutInflater factory = LayoutInflater.from(getContext());
		View view=null;
		try
		{
		 view = factory.inflate(showcaseId, parent, false);
		}
		catch (Exception e)
		{
			e.getCause();
		}
		if (!(view instanceof ShowcaseView)) {
			throw new IllegalArgumentException(
					"Tutorial Manager tried to display id that is not refering to a ShowcaseView");
		}
		final ShowcaseView showcase = (ShowcaseView) view;
		if (mInflatedId != NO_ID) {
			showcase.setId(mInflatedId);
		}
		
		parent.addView(showcase);
		showcase.show();
		showcase.setOnShowcaseEventListener(this);

		mInflatedViewRef = new WeakReference<ShowcaseView>(showcase);
	}

	/**
	 * Specifies the inflate listener to be notified after this ViewStub
	 * successfully inflated its layout resource.
	 * 
	 * @param inflateListener
	 *            The OnInflateListener to notify of successful inflation.
	 * 
	 * @see android.view.ViewStub.OnInflateListener
	 */
	public void addOnInflateListener(OnShowcaseInflated inflateListener) {
		mInflateListeners.add(inflateListener);
	}
	
	public void removeOnInflateListener(OnShowcaseInflated inflateListener) {
		mInflateListeners.remove(inflateListener);
	}

	/**
	 * Listener used to receive a notification after a ViewStub has successfully
	 * inflated its layout resource.
	 * 
	 * @see android.view.ViewStub#setOnInflateListener(android.view.ViewStub.OnInflateListener)
	 */
	public static interface OnShowcaseInflated {
		/**
		 * Invoked after a ViewStub successfully inflated its layout resource.
		 * This method is invoked after the inflated view was added to the
		 * hierarchy but before the layout pass.
		 * 
		 * @param tutorial
		 *            The ViewStub that initiated the inflation.
		 * @param inflated
		 *            The inflated View.
		 */
		void onInflate(TutorialManager tutorial, ShowcaseView inflated);
		void onDeflate(TutorialManager tutorial, ShowcaseView deflated);
	}

	@Override
	public void onShowcaseViewHide(ShowcaseView showcaseView) {

	}

	@Override
	public void onShowcaseViewDidHide(ShowcaseView showcaseView) {	
		if( getVisibility() != View.VISIBLE )
			return;
		mShowcases.remove(0);
		showcaseView.setVisibility(View.GONE);
		
		for (OnShowcaseInflated inflater : mInflateListeners)
		{
			inflater.onDeflate(this, showcaseView);
		}
		
		if( mShowcases.size() == 0 )
		{
			this.setVisibility(View.INVISIBLE);
			return;
		}
		inflate();
	}

	@Override
	public void onShowcaseViewShow(ShowcaseView showcaseView) {
		for (OnShowcaseInflated inflater : mInflateListeners)
		{
			inflater.onInflate(this, showcaseView);
		}
	}
}