package com.espian.showcaseview.anim;
import android.os.Handler;
import android.view.View;
import android.view.View.MeasureSpec;

import com.espian.showcaseview.HandyView;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.Animator.AnimatorListener;
import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.animation.ObjectAnimator;
import com.nineoldandroids.animation.ValueAnimator;
import com.nineoldandroids.view.ViewHelper;

public class AnimationUtils {

	public static final int DEFAULT_DURATION = 300;

	private static final String ALPHA = "alpha";
	private static final float INVISIBLE = 0f;
	private static final float VISIBLE = 1f;
	private static final String COORD_X = "x";
	private static final String COORD_Y = "y";
	private static final String SCALE_X = "scaleX";
	private static final String SCALE_Y = "scaleY";
	private static final String ROTATION = "rotation";
	private static final int INSTANT = 0;

	public interface AnimationStartListener {
		void onAnimationStart();
	}

	public interface AnimationEndListener {
		void onAnimationEnd();
	}

	public static float getX(View view) {
		return ViewHelper.getX(view);
	}

	public static float getY(View view) {
		return ViewHelper.getY(view);
	}

	public static void hide(View view) {
		ViewHelper.setAlpha(view, INVISIBLE);
	}

	public static ObjectAnimator createFadeInAnimation(Object target,
			final AnimationStartListener listener) {
		return createFadeInAnimation(target, DEFAULT_DURATION, listener);
	}

	public static ObjectAnimator createFadeInAnimation(Object target,
			int duration, final AnimationStartListener listener) {
		ObjectAnimator oa = ObjectAnimator.ofFloat(target, ALPHA, INVISIBLE,
				VISIBLE);
		oa.setDuration(duration).addListener(new Animator.AnimatorListener() {
			@Override
			public void onAnimationStart(Animator animator) {
				listener.onAnimationStart();
			}

			@Override
			public void onAnimationEnd(Animator animator) {
			}

			@Override
			public void onAnimationCancel(Animator animator) {
			}

			@Override
			public void onAnimationRepeat(Animator animator) {
			}
		});
		return oa;
	}

	public static ObjectAnimator createFadeOutAnimation(Object target,
			final AnimationEndListener listener) {
		return createFadeOutAnimation(target, DEFAULT_DURATION, listener);
	}

	public static ObjectAnimator createFadeOutAnimation(Object target,
			int duration, final AnimationEndListener listener) {
		ObjectAnimator oa = ObjectAnimator.ofFloat(target, ALPHA, INVISIBLE);
		oa.setDuration(duration).addListener(new Animator.AnimatorListener() {
			@Override
			public void onAnimationStart(Animator animator) {
			}

			@Override
			public void onAnimationEnd(Animator animator) {
				listener.onAnimationEnd();
			}

			@Override
			public void onAnimationCancel(Animator animator) {
			}

			@Override
			public void onAnimationRepeat(Animator animator) {
			}
		});
		return oa;
	}

	public static AnimatorSet createMovementAnimation(View view, float canvasX,
			float canvasY, float offsetStartX, float offsetStartY,
			float offsetEndX, float offsetEndY,
			final AnimationEndListener listener) {
		ViewHelper.setAlpha(view, INVISIBLE);
		
		canvasX += ViewHelper.getTranslationX(view);
		canvasY += ViewHelper.getTranslationY(view);

		ObjectAnimator alphaIn = ObjectAnimator.ofFloat(view, ALPHA, INVISIBLE,
				VISIBLE).setDuration(500);

		ObjectAnimator setUpX = ObjectAnimator.ofFloat(view, COORD_X,
				canvasX + offsetStartX).setDuration(INSTANT);
		ObjectAnimator setUpY = ObjectAnimator.ofFloat(view, COORD_Y,
				canvasY + offsetStartY).setDuration(INSTANT);

		ObjectAnimator moveX = ObjectAnimator.ofFloat(view, COORD_X,
				canvasX + offsetEndX).setDuration(1000);
		ObjectAnimator moveY = ObjectAnimator.ofFloat(view, COORD_Y,
				canvasY + offsetEndY).setDuration(1000);
		moveX.setStartDelay(1000);
		moveY.setStartDelay(1000);

		ObjectAnimator alphaOut = ObjectAnimator
				.ofFloat(view, ALPHA, INVISIBLE).setDuration(500);
		alphaOut.setStartDelay(2500);

		AnimatorSet as = new AnimatorSet();
		as.play(setUpX).with(setUpY).before(alphaIn).before(moveX).with(moveY)
				.before(alphaOut);

		Handler handler = new Handler();
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				listener.onAnimationEnd();
			}
		};
		handler.postDelayed(runnable, 3000);
		if ( view instanceof HandyView )
			((HandyView) view).addAnimation(as);
		return as;
	}

	public static AnimatorSet createRotationAnimation(View view, float x,
			float y, float radius, float start, float end,
			final AnimationEndListener listener) {
		ViewHelper.setAlpha(view, INVISIBLE);

		ObjectAnimator alphaIn = ObjectAnimator.ofFloat(view, ALPHA, INVISIBLE,
				VISIBLE).setDuration(500);
		
		float scale = (3.7f*radius) / view.getMeasuredWidth() ;
		
		x += ViewHelper.getTranslationX(view);
		y += ViewHelper.getTranslationY(view);
		
		start += ViewHelper.getRotation(view);
		end += ViewHelper.getRotation(view);
		
		ObjectAnimator setUpX = ObjectAnimator.ofFloat(view, COORD_X, x)
				.setDuration(INSTANT);
		ObjectAnimator setUpY = ObjectAnimator.ofFloat(view, COORD_Y, y)
				.setDuration(INSTANT);
		ObjectAnimator setUpScaleX = ObjectAnimator.ofFloat(view, SCALE_X, scale)
				.setDuration(INSTANT);
		ObjectAnimator setUpScaleY = ObjectAnimator.ofFloat(view, SCALE_Y, scale)
				.setDuration(INSTANT);
		ObjectAnimator setUpRotation = ObjectAnimator.ofFloat(view, ROTATION,
				start).setDuration(INSTANT);

		ObjectAnimator rot = ObjectAnimator.ofFloat(view, ROTATION, end)
				.setDuration(1000);

		rot.setStartDelay(1000);

		ObjectAnimator alphaOut = ObjectAnimator
				.ofFloat(view, ALPHA, INVISIBLE).setDuration(500);
		alphaOut.setStartDelay(2500);

		AnimatorSet as = new AnimatorSet();
		as.play(setUpX).with(setUpY).with(setUpScaleX).with(setUpScaleY).with(setUpRotation).before(alphaIn)
				.before(rot).before(alphaOut);

		Handler handler = new Handler();
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				listener.onAnimationEnd();
			}
		};
		handler.postDelayed(runnable, 3000);
		if ( view instanceof HandyView )
			((HandyView) view).addAnimation(as);
		return as;
	}

	public static AnimatorSet createArrowAnimation(View view, float startX,
			float startY, float endX, float endY,
			final AnimationEndListener listener) {
		ViewHelper.setAlpha(view, INVISIBLE);

		ObjectAnimator alphaIn = ObjectAnimator.ofFloat(view, ALPHA, INVISIBLE,
				VISIBLE).setDuration(500);
		int h = view.getLayoutParams().height;
		int w = view.getLayoutParams().width;
		ViewHelper.setPivotX(view, w/2);
		ViewHelper.setPivotY(view, w/2);
		
		ObjectAnimator setUpRotation = null;
		if ( startX < endX ){
			setUpRotation = ObjectAnimator.ofFloat(view, ROTATION,
				45).setDuration(INSTANT);
		}
		else{
			setUpRotation = ObjectAnimator.ofFloat(view, ROTATION,
					-135).setDuration(INSTANT);
		}

		ObjectAnimator setUpX = ObjectAnimator.ofFloat(view, COORD_X, startX-w/2)
				.setDuration(INSTANT);
		ObjectAnimator setUpY = ObjectAnimator.ofFloat(view, COORD_Y, startY-w/2 )
				.setDuration(INSTANT);
		
		ObjectAnimator moveX = ObjectAnimator.ofFloat(view, COORD_X,
				endX-w/2).setDuration(1000);
		ObjectAnimator moveY = ObjectAnimator.ofFloat(view, COORD_Y,
				endY-w/2).setDuration(1000);
		ValueAnimator scaleY = ValueAnimator.ofObject(new HeightEvaluator(view), h, 
				(int)(h+Math.abs(endX-startX))).setDuration(1000);
		moveX.setStartDelay(1000);
		moveY.setStartDelay(1000);
		scaleY.setStartDelay(1000);

		ObjectAnimator alphaOut = ObjectAnimator
				.ofFloat(view, ALPHA, INVISIBLE).setDuration(500);
		alphaOut.setStartDelay(2500);

		AnimatorSet as = new AnimatorSet();
		as.play(setUpX).with(setUpY).before(setUpRotation).before(alphaIn)
				.before(scaleY).with(moveX).with(moveY).before(alphaOut);

		as.addListener(new AnimatorListener() {
			
			@Override
			public void onAnimationStart(Animator arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onAnimationRepeat(Animator arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onAnimationEnd(Animator arg0) {
				listener.onAnimationEnd();
			}
			
			@Override
			public void onAnimationCancel(Animator arg0) {
				listener.onAnimationEnd();
			}
		});


		return as;
	}
	
	public static AnimatorSet createArrowAnimationReverse(View view, float startX,
			float startY, float endX, float endY,
			final AnimationEndListener listener) {
		ViewHelper.setAlpha(view, INVISIBLE);

		ObjectAnimator alphaIn = ObjectAnimator.ofFloat(view, ALPHA, INVISIBLE,
				VISIBLE).setDuration(500);
		int h = view.getLayoutParams().height;
		int w = view.getLayoutParams().width;

		ObjectAnimator.ofFloat(view, COORD_X, startX-w/2)
				.setDuration(INSTANT).start();
		ObjectAnimator.ofFloat(view, COORD_Y, startY-h)
				.setDuration(INSTANT).start();
		
		ViewHelper.setPivotX(view, w/2);
		ViewHelper.setPivotY(view, h);
		
		if ( startX < endX ){
			ObjectAnimator.ofFloat(view, ROTATION,
				45).setDuration(INSTANT).start();
		}
		else{
			ObjectAnimator.ofFloat(view, ROTATION,
					-135).setDuration(INSTANT).start();
		}
		
		ObjectAnimator moveX = ObjectAnimator.ofFloat(view, COORD_X,
				endX-w/2).setDuration(1000);
		ObjectAnimator moveY = ObjectAnimator.ofFloat(view, COORD_Y,
				endY-h).setDuration(1000);
		ValueAnimator scaleY = ValueAnimator.ofObject(new HeightEvaluator(view), h, 
				(int)(h+Math.abs(endX-startX))).setDuration(1000);
		moveX.setStartDelay(1000);
		moveY.setStartDelay(1000);
		scaleY.setStartDelay(1000);

		ObjectAnimator alphaOut = ObjectAnimator
				.ofFloat(view, ALPHA, INVISIBLE).setDuration(500);
		alphaOut.setStartDelay(2500);

		AnimatorSet as = new AnimatorSet();
		as.play(alphaIn)
				.before(scaleY).with(moveX).with(moveY).before(alphaOut);

		as.addListener(new AnimatorListener() {
			
			@Override
			public void onAnimationStart(Animator arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onAnimationRepeat(Animator arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onAnimationEnd(Animator arg0) {
				listener.onAnimationEnd();
			}
			
			@Override
			public void onAnimationCancel(Animator arg0) {
				listener.onAnimationEnd();
			}
		});

		return as;
	}
	
	public static AnimatorSet createPintchHandOutZoomOutAnim( HandyView handyOut, float x,
			float y, float radius, final AnimationEndListener listener) {
		ViewHelper.setAlpha(handyOut, INVISIBLE);
		
		ObjectAnimator alphaIn = ObjectAnimator.ofFloat(handyOut, ALPHA, INVISIBLE,
				VISIBLE).setDuration(250);
		handyOut.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		int h = handyOut.getMeasuredHeight();
		int w = handyOut.getMeasuredWidth();
		float scale = 3f*radius/Math.min(w, h);
		
		x += ViewHelper.getTranslationX(handyOut);
		y += ViewHelper.getTranslationY(handyOut);
		
		ObjectAnimator setUpScaleX = ObjectAnimator.ofFloat(handyOut, SCALE_X, scale)
				.setDuration(INSTANT);
		ObjectAnimator setUpScaleY = ObjectAnimator.ofFloat(handyOut, SCALE_Y, scale)
				.setDuration(INSTANT);
		
		ObjectAnimator setUpX = ObjectAnimator.ofFloat(handyOut, COORD_X, x)
				.setDuration(INSTANT);
		ObjectAnimator setUpY = ObjectAnimator.ofFloat(handyOut, COORD_Y, y)
				.setDuration(INSTANT);

		ObjectAnimator alphaOut = ObjectAnimator
				.ofFloat(handyOut, ALPHA, INVISIBLE).setDuration(250);
		alphaOut.setStartDelay(1250);

		AnimatorSet as = new AnimatorSet();
		as.play(setUpX).with(setUpY).with(setUpScaleX).with(setUpScaleY).before(alphaIn).before(alphaOut);

		Handler handler = new Handler();
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				//listener.onAnimationEnd();
			}
		};
		handler.postDelayed(runnable, 3000);

		handyOut.addAnimation(as);
		return as;
	}
	
	public static AnimatorSet createPintchHandInZoomOutAnim( HandyView handyIn, float x,
			float y, float radius, final AnimationEndListener listener) {
		ViewHelper.setAlpha(handyIn, INVISIBLE);
		
		ObjectAnimator alphaIn = ObjectAnimator.ofFloat(handyIn, ALPHA, INVISIBLE,
				VISIBLE).setDuration(250);
		
		int h = handyIn.getMeasuredHeight();
		int w = handyIn.getMeasuredWidth();
		
		float scale = 3f*radius/Math.min(w, h);
		
		x += ViewHelper.getTranslationX(handyIn);
		y += ViewHelper.getTranslationY(handyIn);
		
		ObjectAnimator setUpX = ObjectAnimator.ofFloat(handyIn, COORD_X, x)
				.setDuration(INSTANT);
		ObjectAnimator setUpY = ObjectAnimator.ofFloat(handyIn, COORD_Y, y)
				.setDuration(INSTANT);
		ObjectAnimator setUpScaleX = ObjectAnimator.ofFloat(handyIn, SCALE_X, scale)
				.setDuration(INSTANT);
		ObjectAnimator setUpScaleY = ObjectAnimator.ofFloat(handyIn, SCALE_Y, scale)
				.setDuration(INSTANT);
		
		ObjectAnimator alphaOut = ObjectAnimator
				.ofFloat(handyIn, ALPHA, INVISIBLE).setDuration(250);
		alphaOut.setStartDelay(1250);

		AnimatorSet as = new AnimatorSet();
		as.play(setUpX).with(setUpY).with(setUpScaleX).with(setUpScaleY).before(alphaIn).before(alphaOut);

		Handler handler = new Handler();
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				//listener.onAnimationEnd();
			}
		};
		handler.postDelayed(runnable, 3000);
		handyIn.addAnimation(as);
		return as;
	}

	public static AnimatorSet createMovementAnimation(View view, float x,
			float y) {
		ObjectAnimator alphaIn = ObjectAnimator.ofFloat(view, ALPHA, INVISIBLE,
				VISIBLE).setDuration(500);
		
		x += ViewHelper.getTranslationX(view);
		y += ViewHelper.getTranslationY(view);
		
		ObjectAnimator setUpX = ObjectAnimator.ofFloat(view, COORD_X, x)
				.setDuration(INSTANT);
		ObjectAnimator setUpY = ObjectAnimator.ofFloat(view, COORD_Y, y)
				.setDuration(INSTANT);

		AnimatorSet as = new AnimatorSet();
		as.play(setUpX).with(setUpY).before(alphaIn);
		if ( view instanceof HandyView )
			((HandyView) view).addAnimation(as);
		return as;
	}
}
