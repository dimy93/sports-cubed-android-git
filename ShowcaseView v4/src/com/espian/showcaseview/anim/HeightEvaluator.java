package com.espian.showcaseview.anim;
import com.nineoldandroids.animation.IntEvaluator;

import android.view.View;
import android.view.ViewGroup;
public class HeightEvaluator extends IntEvaluator {

    private View v;
    public HeightEvaluator(View v) {
        this.v = v;
    }

    @Override
    public Integer evaluate(float fraction, Integer startValue, Integer endValue) {
        int num = (Integer)super.evaluate(fraction, startValue, endValue);
        ViewGroup.LayoutParams params = v.getLayoutParams();
        params.height = num;
        v.requestLayout ( );
        return num;
    }
}
